package cz.potass.learn.refactoringexamples.showcase.step09

import cz.potass.learn.refactoringexamples.R
import cz.potass.learn.refactoringexamples.showcase.shared.BaseAsyncTask
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.VERTICAL

internal class VerticalWordsDetectionController(task: BaseAsyncTask) :
        WordsDetectionController(task) {
    override val onStartStringRes = R.string.progress_starting_vertical_words_detection
    override val onWordsComponentsDetectionStringRes = R.string.progress_detecting_columns
    override val onWordsDetectionStringRes = R.string.progress_detecting_vertical_words
    override val mode = VERTICAL
}