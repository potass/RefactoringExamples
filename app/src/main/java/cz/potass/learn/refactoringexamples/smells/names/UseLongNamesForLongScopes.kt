package cz.potass.learn.refactoringexamples.smells.names

internal class UseLongNamesForLongScopes {
    private val listeners = listOf<Listener>()

    fun shortScopeExample() {
        listeners.forEach { it.onSuccess() }
        listeners.forEach { l -> l.onSuccess() }
    }

    fun longScopeExample() {
        listeners.forEach { l ->
            doSomething(l)
            // ...
            // More code.
            // ...
            l.onSuccess()
        }
    }

    fun refactoredLongScopeExample() {
        listeners.forEach { listener ->
            doSomething(listener)
            // ...
            // More code.
            // ...
            listener.onSuccess()
        }
    }

    private fun doSomething(listener: Listener) {
        // Some code.
    }

    interface Listener {
        fun onSuccess()
    }
}