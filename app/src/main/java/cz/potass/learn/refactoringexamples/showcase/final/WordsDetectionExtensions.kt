package cz.potass.learn.refactoringexamples.showcase.final

internal fun List<Int>.doesSegmentContainObjects(currentDivider: Int) = currentDivider + 1 !in this