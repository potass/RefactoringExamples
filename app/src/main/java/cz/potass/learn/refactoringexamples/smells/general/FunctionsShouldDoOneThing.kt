package cz.potass.learn.refactoringexamples.smells.general

internal class FunctionsShouldDoOneThing {
    fun pay(employees: List<Employee>) {
        employees.forEach {
            if (it.isPayDay()) {
                val pay = it.calculatePay()
                it.deliverPay(pay)
            }
        }
    }

    fun refactoredPay(employees: List<Employee>) {
        employees.forEach { payIfNecessary(it) }
    }

    private fun payIfNecessary(employee: Employee) {
        if (employee.isPayDay()) pay(employee)
    }

    private fun pay(employee: Employee) {
        val pay = employee.calculatePay()
        employee.deliverPay(pay)
    }

    interface Employee {
        fun isPayDay(): Boolean
        fun calculatePay(): Double
        fun deliverPay(money: Double)
    }
}