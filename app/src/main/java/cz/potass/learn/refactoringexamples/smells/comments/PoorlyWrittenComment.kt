package cz.potass.learn.refactoringexamples.smells.comments

internal class PoorlyWrittenComment {
    fun dolphinExample() {
        // Just moved from TravelInsurranceController where it was no more useful. It is much more
        // useful here. Just look how awesom it is here. Do not move it back or something bad could
        // happen. Trust me I am a dolphin.
        run {}
    }

    fun refactoredDolphinExample() {
        run {}
    }
}