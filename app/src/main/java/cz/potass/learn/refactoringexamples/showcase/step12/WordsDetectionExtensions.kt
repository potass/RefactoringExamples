package cz.potass.learn.refactoringexamples.showcase.step12

internal fun List<Int>.doesSegmentContainObjects(currentDivider: Int) = currentDivider + 1 !in this