package cz.potass.learn.refactoringexamples.smells.functions

internal class DeadFunction {
    fun example() {
        usedMethod()
    }

    private fun usedMethod() {
        // Some code.
    }

    private fun unusedMethod() {
        // Some code.
    }

    fun refactoredExample() {
        refactoredUsedMethod()
    }

    private fun refactoredUsedMethod() {
        // Some code.
    }
}