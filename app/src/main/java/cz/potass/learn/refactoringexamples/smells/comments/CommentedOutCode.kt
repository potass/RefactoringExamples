package cz.potass.learn.refactoringexamples.smells.comments

internal class CommentedOutCode {
    private val newValue = 42.0

    fun computationExample(): Double {
//        return compute(ratio)
        return Math.sqrt(newValue)
    }

//    private fun compute(ratio: Float): Float {
//        return oldValue * getPercentage() / ratio
//    }

    fun refactoredComputationExample(): Double {
        return Math.sqrt(newValue)
    }
}