package cz.potass.learn.refactoringexamples.showcase.step03

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.GapType.AVERAGE_OBJECT_SIZE
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener
import cz.potass.learn.refactoringexamples.showcase.step03.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Avoid passing allowRecursion and firstRun params as they are always passed as true from
 *  outside.
 *  1. Remove allowRecursion as it is redundant.
 *  1. Remove log param as it depends on debug variant and firstRun.
 *  1. Remove isCacheAvailable param as it can be determined right here.
 *  1. Make left method params instance variables to avoid their passing among methods.
 *  1. Remove hungary notation of settings and move them to instance variables as they are provided
 *  by caller.
 *  1. Create result object to simplify method signature.
 *  1. Create input params object to simplify class signature (mocking simplification).
 *  1. Move basePath value to Params.
 *  1. Avoid null result by using sealed class approach (one timber log removal).
 */
internal class WordsDetectionProcessor(private val params: Params) {
    private companion object {
        const val PATH_SEPARATOR = "/"
    }

    private val cache = Cache.get()
    private val settings = Settings.get()
    private var isFirstRun = true

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    fun detect(): Result {
        params.onStart()

        val blackWhiteImage = loadBlackWhiteImage()
        val descriptors = loadDescriptors()

        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            // Creating horizontal histogram.
            val numberOfObjectPixelsOnRows = FloatArray(blackWhiteImage.rows())
            val imageData = ByteArray(blackWhiteImage.rows() * blackWhiteImage.cols() * blackWhiteImage.channels())
            blackWhiteImage.get(0, 0, imageData)
            for (i in imageData.indices) {
                if (imageData[i] == 255.toByte()) {
                    numberOfObjectPixelsOnRows[i / blackWhiteImage.cols()]++
                }
            }

            val rowsDividers = getRowsDividers(numberOfObjectPixelsOnRows)
            Timber.d("Rows dividers are ${if (rowsDividers == null) "NULL" else "NOT NULL"}.")

            if (rowsDividers != null) {
                val showFoundGapsBetweenWords = settings.shouldShowGapsBetweenWords()

                var heightSum = 0.0f
                var widthSum = 0.0f
                for (d in descriptors) {
                    heightSum += d.straightRectangleHeight
                    widthSum += d.straightRectangleWidth
                }

                if (params.isRowAverageObjectSizeGapType()) {
                    val heightAvg = heightSum / descriptors.size.toFloat()
                    params.setRowGapTypeValue(heightAvg)
                }

                if (params.isWordAverageObjectSizeGapType()) {
                    val widthAvg = widthSum / descriptors.size.toFloat()
                    params.setWordGapTypeValue(widthAvg)
                }

                val mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
                val shortenRowsDividers = shortenDividers(mergedRowsDividers)

                var rows: Mat
                // Drawing rows boundaries.
                if (shouldCreateDebugInfo()) {
                    rows = blackWhiteImage.clone()

                    for (rowDivider in shortenRowsDividers) {
                        line(
                                rows,
                                Point(1, rowDivider),
                                Point(blackWhiteImage.cols(), rowDivider),
                                Scalar(255), 1, 8, 0)
                    }

                    conditionalSave(true, params.image, rows,
                            if (params.isHorizontal())
                                HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
                            else
                                VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME)
                    rows.release()
                }

                params.onWordsDetection()

                rows = blackWhiteImage.clone()
                val bgrImageWithWordsBoundaries =
                        if (showFoundGapsBetweenWords) loadBgrImageWithWordsBoundaries() else null

                // This stores pairs of word with its objects.
                val wordsWithObjects = LinkedHashMap<Descriptor, List<Descriptor>>()
                // This stores pairs of image's segment (row or column) and number of words in this segment.
                val segmentsWithNumberOfWords = LinkedHashMap<Descriptor, Int>()

                for (i in 0 until shortenRowsDividers.size - 1) {
                    val currentRowDivider = shortenRowsDividers[i]
                    // When this row contains objects, process it.
                    if (!mergedRowsDividers.contains(currentRowDivider + 1)) {
                        if (params.isCancelled()) return Cancelled

                        val nextRowDivider = shortenRowsDividers[i + 1]

                        val height = nextRowDivider - currentRowDivider

                        val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)

                        // Creating vertical histogram of row.
                        val numberOfObjectPixelsOnCols = FloatArray(row.cols())
                        val rowData = ByteArray(row.rows() * row.cols() * row.channels())
                        row.get(0, 0, rowData)
                        for (j in rowData.indices) {
                            if (rowData[j] == 255.toByte()) {
                                numberOfObjectPixelsOnCols[j % row.cols()]++
                            }
                        }

                        val colsDividers = getColumnsDividers(numberOfObjectPixelsOnCols)
                        if (colsDividers != null) {
                            val mergedColsDividers = mergeColumnsDividers(colsDividers, params.wordGapType, row.cols())
                            val shortenColsDividers = shortenDividers(mergedColsDividers)

                            if (shortenColsDividers.isNotEmpty()) {
                                // Drawing words' boundaries only in the first run. It would need
                                // more complicated handling to support also drawing of second run...
                                if (showFoundGapsBetweenWords && isFirstRun) {
                                    for (colDivider in mergedColsDividers) {
                                        line(
                                                bgrImageWithWordsBoundaries!!,
                                                Point(colDivider, currentRowDivider + 1),
                                                Point(colDivider, nextRowDivider),
                                                Scalar(54, 67, 244), 1, 8, 0)
                                    }
                                }

                                if (shouldCreateDebugInfo()) {
                                    for (colDivider in shortenColsDividers) {
                                        line(
                                                rows,
                                                Point(colDivider, currentRowDivider + 1),
                                                Point(colDivider, nextRowDivider),
                                                Scalar(255), 1, 8, 0)
                                    }
                                }

                                val segment = mutableListOf<Point>()
                                var numberOfWordsInSegment = 0

                                // Extracting of words on current row (with a little variance).
                                for (k in 0 until shortenColsDividers.size - 1) {
                                    val currentColDivider = shortenColsDividers[k]

                                    if (!mergedColsDividers.contains(currentColDivider + 1)) {
                                        val nextColDivider = shortenColsDividers[k + 1]

                                        val width = nextColDivider - currentColDivider

                                        val wordObjects = mutableListOf<Descriptor>()
                                        val word = mutableListOf<Point>()

                                        // Collection of descriptors to be removed in this iteration to
                                        // speed up processing.
                                        val toRemove = mutableListOf<Descriptor>()

                                        // Because of using threshold when detecting rows, some rows with
                                        // objects can be considered as gaps. These objects can cause problems
                                        // here, so I must be really careful when defining rectangle of word.
                                        var wordRectY = currentRowDivider + 1
                                        var wordRectHeight = height
                                        val maxExtraHeight = (height * 0.5).toInt()
                                        if (i - 1 >= 0) {
                                            val previousRowDivider = shortenRowsDividers[i - 1]
                                            var topExtraHeight = ((currentRowDivider - previousRowDivider) * 0.5).toInt()
                                            if (topExtraHeight > maxExtraHeight)
                                                topExtraHeight = maxExtraHeight
                                            wordRectY -= topExtraHeight
                                            wordRectHeight += topExtraHeight
                                        }
                                        if (i + 2 < shortenRowsDividers.size) {
                                            val nextNextRowDivider = shortenRowsDividers[i + 2]
                                            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * 0.5).toInt()
                                            if (bottomExtraHeight > maxExtraHeight)
                                                bottomExtraHeight = maxExtraHeight
                                            wordRectHeight += bottomExtraHeight
                                        }

                                        // Assumed rectangle of word dependent on extracted dividers.
                                        val wordRect = Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)

                                        for (d in descriptors) {
                                            // Checking if center point of descriptor lies in word rectangle.
                                            if (wordRect.contains(d.center)) {
                                                wordObjects.add(d)
                                                word.addAll(d.`object`.toList())
                                                toRemove.add(d)
                                            }
                                        }
                                        descriptors.removeAll(toRemove)
                                        toRemove.clear()

                                        // Adding word with its objects to its result.
                                        if (wordObjects.size > 0 && word.size > 0) {
                                            segment.addAll(word)
                                            numberOfWordsInSegment++

                                            val wordMatOfPoint = MatOfPoint()
                                            wordMatOfPoint.fromList(word)
                                            wordsWithObjects[Descriptor(wordMatOfPoint)] = wordObjects
                                        }
                                    }
                                }

                                // Adding segment with its number of words to its result.
                                if (segment.size > 0) {
                                    val segmentMatOfPoint = MatOfPoint()
                                    segmentMatOfPoint.fromList(segment)
                                    segmentsWithNumberOfWords[Descriptor(segmentMatOfPoint)] = numberOfWordsInSegment
                                }
                            }
                        }
                    }
                }
                if (showFoundGapsBetweenWords && isFirstRun) {
                    // During second run will nothing change so cache image only in the first run.
                    cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries!!)
                    conditionalSave(shouldCreateDebugInfo(), params.image, bgrImageWithWordsBoundaries,
                            WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME)
                }
                // This is only saved during first call!
                conditionalSave(shouldCreateDebugInfo(), params.image, rows,
                        if (params.isHorizontal())
                            HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
                        else
                            VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME)

                // Coloring of words.
                conditionalColorAndSave(shouldCreateDebugInfo(), params.image, blackWhiteImage.size(),
                        wordsWithObjects.values.toList(),
                        if (params.isHorizontal())
                            HORIZONTAL_WORDS_IMAGE_NAME
                        else
                            VERTICAL_WORDS_IMAGE_NAME)

                if (descriptors.isNotEmpty()) {
                    // Ok, there are objects which were probably considered as gaps, then call the whole
                    // algorithm again recursively and disable another recursion.
                    if (isRecursionAllowed()) {
                        Timber.w("${descriptors.size} objects have not been assigned to any word. Processing unassigned objects once again.")

                        forbidRecursion()

                        // Prepare new image with black background and white foreground which contains
                        // only unassigned objects.
                        val recursionBlackWhiteImage = zeros(blackWhiteImage.size(), blackWhiteImage.type())
                        val dataReadable = ByteArray(recursionBlackWhiteImage.rows() *
                                recursionBlackWhiteImage.cols() * recursionBlackWhiteImage.channels())
                        recursionBlackWhiteImage.get(0, 0, dataReadable)
                        for (d in descriptors) {
                            for (p in d.`object`.toList()) {
                                // Descriptors starts at zero indexes! See more in object detection.
                                val index = p.x.toInt() % recursionBlackWhiteImage.cols() + p.y.toInt() * recursionBlackWhiteImage.cols()
                                dataReadable[index] = 255.toByte()
                            }
                        }
                        recursionBlackWhiteImage.put(0, 0, dataReadable)
                        // Update cache for recursive call.
                        cache.cacheBinaryBlackWhiteImage(recursionBlackWhiteImage)
                        cache.cacheDescriptors(descriptors)

                        // Add newly found words.
                        val recursionResult = detect()
                        if (recursionResult is Success) {
                            wordsWithObjects.putAll(recursionResult.wordsWithObjects)
                            // Update descriptors.
                            for (values in recursionResult.wordsWithObjects.values)
                                descriptors.removeAll(values)
                            // Update segments.
                            segmentsWithNumberOfWords.putAll(recursionResult.segmentsWithNumberOfWords)

                            if (descriptors.size > 0)
                                Timber.w("${descriptors.size} objects have not been assigned to any word during recursive call.")
                            else
                                Timber.i("All objects have been successfully used while detecting words.")

                            // Coloring of words after recursive call.
                            conditionalColorAndSave(shouldCreateDebugInfo(), params.image, recursionBlackWhiteImage.size(),
                                    wordsWithObjects.values.toList(),
                                    if (params.isHorizontal())
                                        RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
                                    else
                                        RECURSIVE_VERTICAL_WORDS_IMAGE_NAME)
                        }

                        allowRecursion()
                    }
                }

                return Success(wordsWithObjects, descriptors, segmentsWithNumberOfWords)
            }
        }

        return NoObjectsFound
    }

    private fun loadBlackWhiteImage(): Mat {
        var result: Mat
        if (isFirstRun && cache.getBinaryBlackWhiteImage() == null) {
            result = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
            cache.cacheBinaryBlackWhiteImage(result)
        } else {
            result = cache.getBinaryBlackWhiteImage()!!
        }
        return result
    }

    private fun loadDescriptors(): MutableList<Descriptor> {
        return if (isFirstRun && cache.getDescriptors() == null) {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        } else {
            cache.getDescriptors()!!
        }
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        var result: Mat
        if (isFirstRun) {
            result =
                    if (cache.getBgrImage() == null) {
                        imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                                CV_LOAD_IMAGE_UNCHANGED)
                    } else {
                        cache.getBgrImage()!!.clone()
                    }

            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
        } else {
            result = cache.getBgrImageWithWordsBoundaries()!!.clone()
        }
        return result
    }

    private fun shouldCreateDebugInfo() = DEBUG && isFirstRun

    private fun isRecursionAllowed() = isFirstRun

    private fun forbidRecursion() {
        isFirstRun = false
    }

    private fun allowRecursion() {
        isFirstRun = true
    }

    data class Params(
            private val listener: WordsDetectionProgressListener,
            val rowGapType: GapType,
            val wordGapType: GapType,
            val image: Image,
            val mode: Mode,
            val isCancelled: () -> Boolean
    ) {
        val basePath = image.getBasePath()

        fun isVertical() = mode.isVertical()
        fun isHorizontal() = mode.isHorizontal()

        fun onStart() = listener.onStart()
        fun onWordsComponentsDetection() = listener.onWordsComponentsDetection()
        fun onWordsDetection() = listener.onWordsDetection()

        fun isRowAverageObjectSizeGapType() = rowGapType == AVERAGE_OBJECT_SIZE
        fun isWordAverageObjectSizeGapType() = wordGapType == AVERAGE_OBJECT_SIZE
        fun setRowGapTypeValue(value: Float) = rowGapType.setValue(value)
        fun setWordGapTypeValue(value: Float) = wordGapType.setValue(value)
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val segmentsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result()
    }

    enum class Mode {
        HORIZONTAL,
        VERTICAL;

        fun isHorizontal(): Boolean {
            return this == HORIZONTAL
        }

        fun isVertical(): Boolean {
            return this == VERTICAL
        }
    }
}