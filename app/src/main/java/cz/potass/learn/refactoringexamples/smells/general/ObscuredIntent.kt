package cz.potass.learn.refactoringexamples.smells.general

import java.lang.Math.PI

internal class ObscuredIntent {
    fun vCalc(rds: Double, h: Double): Double {
        return 3.14 * Math.pow(rds, 2.0) * h
    }

    fun cylinderVolume(radius: Double, height: Double): Double {
        return PI * Math.pow(radius, 2.0) * height
    }
}