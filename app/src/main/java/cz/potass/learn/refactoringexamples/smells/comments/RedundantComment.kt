package cz.potass.learn.refactoringexamples.smells.comments

internal class RedundantComment {
    fun counterExample() {
        var counter = 0
        // Increment counter.
        counter++
    }

    fun refactoredCounterExample() {
        var counter = 0
        counter++
    }

    /**
     * @param param
     * @return Unit
     * @throws NullPointerException
     */
    @Throws(NullPointerException::class)
    fun docExample(param: Any) = Unit

    @Throws(NullPointerException::class)
    fun refactoredDocExample(param: Any) = Unit
}