package cz.potass.learn.refactoringexamples.smells.general

internal class AvoidTransitiveNavigation {
    fun example() {
        A().getB().getC().doSomething()
    }

    class A {
        fun getB(): B {
            return B()
        }
    }

    class B {
        fun getC(): C {
            return C()
        }
    }

    class C {
        fun doSomething() {}
    }

    fun refactoredExample() {
        RefactoredA().doSomething()
    }

    class RefactoredA {
        fun doSomething() {
            RefactoredB().doSomething()
        }
    }

    class RefactoredB {
        fun doSomething() {
            RefactoredC().doSomething()
        }
    }

    class RefactoredC {
        fun doSomething() {}
    }
}