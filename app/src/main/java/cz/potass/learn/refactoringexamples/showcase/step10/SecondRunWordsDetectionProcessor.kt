package cz.potass.learn.refactoringexamples.showcase.step10

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.Size
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionDrawer.Companion.WHITE_AS_BYTE
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionProcessor.Result.Success

internal class SecondRunWordsDetectionProcessor(params: WordsDetectionParams)
    : WordsDetectionProcessor(params) {
    private val imageWithUnassignedObjects: Mat

    init {
        val blackWhiteImage = checkNotNull(cache.getBinaryBlackWhiteImage())
        val descriptors = checkNotNull(cache.getDescriptors())
        imageWithUnassignedObjects = prepareImageWithUnassignedObjects(blackWhiteImage, descriptors)
    }

    private fun prepareImageWithUnassignedObjects(
            blackWhiteImage: Mat, descriptors: List<Descriptor>
    ): Mat {
        val bwImageWithUnassignedObjects = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val dataReadable = ByteArray(
                bwImageWithUnassignedObjects.rows() *
                        bwImageWithUnassignedObjects.cols() *
                        bwImageWithUnassignedObjects.channels()
        )
        bwImageWithUnassignedObjects.get(0, 0, dataReadable)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % bwImageWithUnassignedObjects.cols() +
                        p.y.toInt() * bwImageWithUnassignedObjects.cols()
                dataReadable[index] = WHITE_AS_BYTE
            }
        }
        bwImageWithUnassignedObjects.put(0, 0, dataReadable)
        cache.cacheBinaryBlackWhiteImage(bwImageWithUnassignedObjects)
        return bwImageWithUnassignedObjects
    }

    override fun detect(): Result {
        logger.logPreRecursionInfo(descriptors.size)
        val result = super.detect()
        if (result is Success) {
            logger.logRecursionInfo(descriptors)
            persistRecursiveImageWithColoredWords(imageWithUnassignedObjects.size())
        }
        return result
    }

    private fun persistRecursiveImageWithColoredWords(blackWhiteImageSize: Size) {
        persistImageWithColoredWords { arg1, arg2 ->
            saveRecursiveImageWithColoredWordsIfPossible(arg1, blackWhiteImageSize, arg2)
        }
    }

    override fun shouldShowGapsBetweenWords() = false

    override fun shouldCreateDebugInfo() = false
}