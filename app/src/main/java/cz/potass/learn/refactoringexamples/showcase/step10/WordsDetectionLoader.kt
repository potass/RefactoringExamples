package cz.potass.learn.refactoringexamples.showcase.step10

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects

internal class WordsDetectionLoader(private val params: WordsDetectionParams) {
    private companion object {
        const val PATH_SEPARATOR = "/"
    }

    private val cache = Cache.get()
    private val settings = Settings.get()

    fun loadBlackWhiteImage(): Mat {
        return cache.getBinaryBlackWhiteImage() ?: run {
            var bwImage = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) bwImage = rotateBy90Multiple(bwImage, 1)
            bwImage.apply { cache.cacheBinaryBlackWhiteImage(this) }
        }
    }

    fun loadDescriptors(): MutableList<Descriptor> {
        return cache.getDescriptors() ?: run {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        }
    }

    fun loadBgrImageWithWordsBoundaries(): Mat {
        return cache.getBgrImageWithWordsBoundaries()?.clone() ?: run {
            val result = cache.getBgrImage()?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                            CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) rotateBy90Multiple(result, 1) else result
        }
    }
}