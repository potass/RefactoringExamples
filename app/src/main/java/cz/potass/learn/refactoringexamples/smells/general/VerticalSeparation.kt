package cz.potass.learn.refactoringexamples.smells.general

internal class VerticalSeparation {
    class Example {
        private fun privateFunction3(param: Int) {
            // Some code.
        }

        private val value1 = 1

        fun function1() {
            val localValue = privateFunction2()
            privateFunction1()
            privateFunction3(localValue)
        }

        private fun privateFunction2(): Int {
            return 42 * value1 * value2
        }

        fun function2() {
            privateFunction4()
        }

        private fun privateFunction1() {
            privateFunction4()
        }

        private fun privateFunction4() {
            // Some code.
        }

        private val value2 = 2
    }

    class RefactoredExample {
        private val value1 = 1
        private val value2 = 2

        fun function1() {
            privateFunction1()
            val localValue = privateFunction2()
            privateFunction3(localValue)
        }

        private fun privateFunction1() {
            privateFunction4()
        }

        private fun privateFunction2(): Int {
            return 42 * value1 * value2
        }

        private fun privateFunction3(param: Int) {
            // Some code.
        }

        fun function2() {
            privateFunction4()
        }

        private fun privateFunction4() {
            // Some code.
        }
    }
}