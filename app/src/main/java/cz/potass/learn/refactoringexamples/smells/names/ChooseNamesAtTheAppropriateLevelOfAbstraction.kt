package cz.potass.learn.refactoringexamples.smells.names

internal class ChooseNamesAtTheAppropriateLevelOfAbstraction {
    interface Modem {
        fun dial(phoneNumber: String): Boolean
        fun disconnect(): Boolean
        fun send(c: Char): Boolean
        fun receive(): Char
        fun getConnectedPhoneNumber(): String
    }

    interface RefactoredModem {
        fun connect(connectionLocator: String): Boolean
        fun disconnect(): Boolean
        fun send(c: Char): Boolean
        fun receive(): Char
        fun getConnectedLocator(): String
    }
}