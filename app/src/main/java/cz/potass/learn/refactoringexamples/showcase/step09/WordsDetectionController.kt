package cz.potass.learn.refactoringexamples.showcase.step09

import cz.potass.learn.refactoringexamples.showcase.shared.BaseAsyncTask
import cz.potass.learn.refactoringexamples.showcase.shared.Cache
import cz.potass.learn.refactoringexamples.showcase.shared.GapType
import cz.potass.learn.refactoringexamples.showcase.shared.Image
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionParams
import cz.potass.learn.refactoringexamples.showcase.step09.WordsDetectionProcessor.Result
import cz.potass.learn.refactoringexamples.showcase.step09.WordsDetectionProcessor.Result.*

internal abstract class WordsDetectionController(private val task: BaseAsyncTask) :
        WordsDetectionProgressListener {
    protected abstract val onStartStringRes: Int
    protected abstract val onWordsComponentsDetectionStringRes: Int
    protected abstract val onWordsDetectionStringRes: Int
    protected abstract val mode: WordsDetectionMode

    private val cache = Cache.get()

    fun detect(rowGapType: GapType, wordGapType: GapType, image: Image) {
        prepareForWordsDetection()

        val params = WordsDetectionParams(
                this,
                rowGapType,
                wordGapType,
                image,
                mode
        ) { task.isCancelled() }

        FirstRunWordsDetectionProcessor(params).detect()
                .run { handleFirstRunResult(this, params) }
    }

    private fun prepareForWordsDetection() {
        cache.releaseBinaryBlackWhiteImage()
        cache.releaseDescriptors()
        cache.releaseBgrImageWithWordsBoundaries()
    }

    private fun handleFirstRunResult(result: Result, params: WordsDetectionParams) {
        handleResult(result) {
            if (isAnyObjectUnprocessed()) {
                cache.cacheDescriptors(descriptors)
                SecondRunWordsDetectionProcessor(params).detect()
                        .let { handleSecondRunResult(it, this) }
            } else onSuccess(this)
        }
    }

    private fun handleSecondRunResult(result: Result, firstRunResult: Success) {
        handleResult(result) {
            val finalResult = firstRunResult.update(this)
            onSuccess(finalResult)
        }
    }

    private fun handleResult(result: Result, onSuccess: Success.() -> Unit) {
        when (result) {
            is Cancelled -> onCancelled()
            is NoObjectsFound -> onNoObjectsFound()
            is Success -> onSuccess(result)
        }
    }

    private fun Success.update(secondRunResult: Success): Success {
        val mutableDescriptors = descriptors.toMutableList()
        secondRunResult.wordsWithObjects.values.forEach { mutableDescriptors.removeAll(it) }
        return Success(
                wordsWithObjects + secondRunResult.wordsWithObjects,
                mutableDescriptors,
                rowsWithNumberOfWords + secondRunResult.rowsWithNumberOfWords
        )
    }

    private fun onCancelled() {}
    private fun onNoObjectsFound() {}
    private fun onSuccess(result: Success) {}

    override fun onStart() = onStartStringRes.doProgress()
    override fun onWordsComponentsDetection() = onWordsComponentsDetectionStringRes.doProgress()
    override fun onWordsDetection() = onWordsDetectionStringRes.doProgress()

    private fun Int.doProgress() = task.doProgress(this)
}