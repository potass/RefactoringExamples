package cz.potass.learn.refactoringexamples.smells.general

internal class CodeAtWrongLevelOfAbstraction {
    // QUEUE EXAMPLE

    interface Queue {
        fun insert()
        fun remove()
        fun isEmpty(): Boolean
        fun isCircular(): Boolean
    }

    interface RefactoredQueue {
        fun insert()
        fun remove()
        fun isEmpty(): Boolean
    }

    interface CircularQueue : RefactoredQueue

    // ACTION EXAMPLE

    interface Action {
        fun execute()
        fun undo()
    }

    interface RefactoredAction {
        fun execute()
    }

    interface UndoableAction : RefactoredAction {
        fun undo()
    }
}