package cz.potass.learn.refactoringexamples.smells.general

internal class EncapsulateBoundaryConditions {
    fun positionVsIndexExample(position: Int, items: List<Any>): Any? {
        if (position - 1 in items.indices) {
            return items[position - 1]
        }
        return null
    }

    fun refactoredPositionVsIndexExample(position: Int, items: List<Any>): Any? {
        val index = position - 1
        if (index in items.indices) {
            return items[index]
        }
        return null
    }
}