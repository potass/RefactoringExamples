package cz.potass.learn.refactoringexamples.showcase.step06

import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.Point
import cz.potass.learn.refactoringexamples.showcase.shared.Scalar

class WordsDetectionDrawer {
    companion object {
        const val WHITE = 255
        private val BGR_WHITE = Scalar(WHITE)
        private val BGR_RED = Scalar(54, 67, 244)
    }

    fun drawHorizontalBoundaries(dividers: List<Int>, rows: Mat, numberOfCols: Int) {
        dividers.forEach {
            drawLine(
                    rows,
                    Point(1, it),
                    Point(numberOfCols, it),
                    Scalar(WHITE)
            )
        }
    }

    fun drawWordsBoundaries(
            currentRowDivider: Int, nextRowDivider: Int, mergedColsDividers: List<Int>, image: Mat
    ) {
        drawVerticalBoundaries(currentRowDivider, nextRowDivider, mergedColsDividers, image,
                BGR_RED)
    }

    fun drawColumnsBoundaries(
            currentRowDivider: Int, nextRowDivider: Int, shortenColsDividers: List<Int>, image: Mat
    ) {
        drawVerticalBoundaries(currentRowDivider, nextRowDivider, shortenColsDividers, image,
                BGR_WHITE)
    }

    private fun drawVerticalBoundaries(
            currentHorizontalDivider: Int,
            nextHorizontalDivider: Int,
            dividers: List<Int>,
            image: Mat,
            color: Scalar
    ) {
        dividers.forEach {
            drawLine(
                    image,
                    Point(it, currentHorizontalDivider + 1),
                    Point(it, nextHorizontalDivider),
                    color
            )
        }
    }

    private fun drawLine(image: Mat, start: Point, end: Point, color: Scalar) =
            line(image, start, end, color, 1, 8, 0)
}