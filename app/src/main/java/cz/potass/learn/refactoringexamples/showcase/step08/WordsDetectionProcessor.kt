package cz.potass.learn.refactoringexamples.showcase.step08

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step06.*
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.VERTICAL
import cz.potass.learn.refactoringexamples.showcase.step08.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Do some general refactoring (renaming, simplification, return value type, ...).
 *  1. Eliminate long params method list by extracting params objects (or classes).
 *  1. Extract WordBoundariesDetector.
 *  1. Extract WordInfoFactory.
 */
internal class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    private companion object {
        const val PATH_SEPARATOR = "/"
        const val WHITE = WordsDetectionDrawer.WHITE.toByte()
    }

    private val cache = Cache.get()
    private val settings = Settings.get()
    private var isFirstRun = true
    private val logger = WordsDetectionLogger()
    private val storage = WordsDetectionStorage(params)
    private val drawer = WordsDetectionDrawer()

    private lateinit var blackWhiteImage: Mat
    private lateinit var descriptors: MutableList<Descriptor>
    private lateinit var mergedRowsDividers: List<Int>
    private lateinit var shortenRowsDividers: List<Int>
    private lateinit var mergedColsDividers: List<Int>
    private lateinit var shortenColsDividers: List<Int>
    private lateinit var wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
    private lateinit var rowsWithNumberOfWords: MutableMap<Descriptor, Int>

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     * TODO create 3 classes, one for each logical procedure?
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    fun detect(): Result {
        params.onStart()

        loadBlackWhiteImage()
        loadDescriptors()

        return detectWordsIfAnyObjectIsPresent()
    }

    private fun loadBlackWhiteImage() {
        if (isFirstRun && cache.getBinaryBlackWhiteImage() == null) {
            blackWhiteImage = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) blackWhiteImage = rotateBy90Multiple(blackWhiteImage, 1)
            cache.cacheBinaryBlackWhiteImage(blackWhiteImage)
        } else {
            blackWhiteImage = cache.getBinaryBlackWhiteImage()!!
        }
    }

    private fun loadDescriptors() {
        descriptors = if (isFirstRun && cache.getDescriptors() == null) {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        } else {
            cache.getDescriptors()!!
        }
    }

    private fun detectWordsIfAnyObjectIsPresent(): Result {
        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            detectWordsOnRowsIfPossible()?.run { return this }
        }
        return NoObjectsFound
    }

    private fun detectWordsOnRowsIfPossible(): Result? {
        val numberOfObjectPixelsOnRows = createHorizontalHistogram()

        getRowsDividers(numberOfObjectPixelsOnRows)?.run {
            prepareForWordsDetectionOnEachRow(this)

            val rows = blackWhiteImage.clone()
            val bgrImageWithWordsBoundaries =
                    if (settings.shouldShowGapsBetweenWords()) loadBgrImageWithWordsBoundaries()
                    else null
            if (detectWordsOnEachRow(rows, bgrImageWithWordsBoundaries)) return Cancelled
            persistWordsImages(bgrImageWithWordsBoundaries, rows)
            tryToDetectWordsInGapsIfPossible()

            return Success(wordsWithObjects, descriptors, rowsWithNumberOfWords)
        } ?: logger.logNoRowDividers()
        return null
    }

    private fun prepareForWordsDetectionOnEachRow(rowsDividers: List<Int>) {
        updateGapTypeAveragesIfNecessary()
        mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
        shortenRowsDividers = shortenDividers(mergedRowsDividers)
        drawRowsBoundariesIfNecessary()
        params.onWordsDetection()
        wordsWithObjects = LinkedHashMap()
        rowsWithNumberOfWords = LinkedHashMap()
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        return if (isFirstRun) {
            var result = cache.getBgrImage()
                    ?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                            CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
            result
        } else {
            cache.getBgrImageWithWordsBoundaries()!!.clone()
        }
    }

    private fun createHorizontalHistogram() = createHistogram(blackWhiteImage, HORIZONTAL)

    private fun detectWordsOnEachRow(rows: Mat, bgrImageWithWordsBoundaries: Mat?): Boolean {
        for (i in 0 until shortenRowsDividers.size - 1) {
            val currentRowDivider = shortenRowsDividers[i]
            if (doesRowContainObjects(currentRowDivider)) {
                if (params.isCancelled()) return true

                val nextRowDivider = shortenRowsDividers[i + 1]
                val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                val numberOfObjectPixelsOnCols = createVerticalHistogram(row)

                getColumnsDividers(numberOfObjectPixelsOnCols)?.run {
                    mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                    shortenColsDividers = shortenDividers(mergedColsDividers)

                    extractWordsOnRowIfPossible(WordsOnRowParams(
                            shortenColsDividers, bgrImageWithWordsBoundaries, i, rows))
                }
            }
        }
        return false
    }

    private fun extractWordsOnRowIfPossible(params: WordsOnRowParams) {
        with(params) {
            if (canExtractWordsOnRow()) {
                bgrImageWithWordsBoundaries
                        ?.run { drawWordsBoundariesIfNecessary(this, currentRowDividerIndex) }
                drawColumnsBoundariesIfNecessary(rows, currentRowDividerIndex)

                val rowWords = extractWordsOnRow(currentRowDividerIndex)
                addRowWordsToResultIfPossible(rowWords)
            }
        }
    }

    private fun persistWordsImages(bgrImageWithWordsBoundaries: Mat?, rows: Mat) {
        bgrImageWithWordsBoundaries?.let(::persistBgrImageWithWordsBoundariesIfPossible)
        persistImageWithWordsBoundaries(rows)
        persistImageWithColoredWords()
    }

    private fun createVerticalHistogram(image: Mat) = createHistogram(image, VERTICAL)

    private fun createHistogram(image: Mat, mode: WordsDetectionMode): FloatArray {
        return image.run {
            val numberOfObjectPixels = FloatArray(if (mode.isHorizontal()) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE) {
                    if (mode.isHorizontal()) {
                        numberOfObjectPixels[i / cols()]++
                    } else {
                        numberOfObjectPixels[i % cols()]++
                    }
                }
            }
            numberOfObjectPixels
        }
    }

    private fun updateGapTypeAveragesIfNecessary() {
        updateRowGapTypeAverageIfNecessary()
        updateWordGapTypeAverageIfNecessary()
    }

    private fun updateRowGapTypeAverageIfNecessary() {
        if (params.isRowAverageObjectSizeGapType()) {
            val heightAverage = descriptors.map { it.straightRectangleHeight }.average()
            params.setRowGapTypeValue(heightAverage.toFloat())
        }
    }

    private fun updateWordGapTypeAverageIfNecessary() {
        if (params.isWordAverageObjectSizeGapType()) {
            val widthAverage = descriptors.map { it.straightRectangleWidth }.average()
            params.setWordGapTypeValue(widthAverage.toFloat())
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) {
            val rows = blackWhiteImage.clone()
            drawer.drawHorizontalBoundaries(shortenRowsDividers, rows, blackWhiteImage.cols())
            storage.saveRowsBoundariesImage(rows)
            rows.release()
        }
    }

    private fun drawWordsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        drawVerticalBoundariesIfNecessary(DrawParams(
                shouldDrawWordsBoundaries(),
                drawer,
                shortenRowsDividers,
                image,
                currentRowDividerIndex,
                mergedColsDividers
        ) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawWordsBoundaries(arg1, arg2, arg3, arg4)
        })
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        drawVerticalBoundariesIfNecessary(DrawParams(
                shouldCreateDebugInfo(),
                drawer,
                shortenRowsDividers,
                image,
                currentRowDividerIndex,
                shortenColsDividers
        ) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawColumnsBoundaries(arg1, arg2, arg3, arg4)
        })
    }

    private fun drawVerticalBoundariesIfNecessary(params: DrawParams) {
        with(params) {
            if (condition) {
                drawAction(
                        drawer,
                        shortenRowsDividers[currentRowDividerIndex],
                        shortenRowsDividers[currentRowDividerIndex + 1],
                        dividers,
                        image
                )
            }
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    private fun extractWordsOnRow(currentRowDividerIndex: Int): List<WordInfo> {
        val rowWords = mutableListOf<WordInfo>()
        for (i in 0 until shortenColsDividers.size - 1) {
            extractWordOnRowUsingColumnsRangeIfPossible(RowParams(
                    descriptors, shortenRowsDividers, shortenColsDividers, i, currentRowDividerIndex
            ))?.let(rowWords::add)
        }
        return rowWords
    }

    private fun extractWordOnRowUsingColumnsRangeIfPossible(params: RowParams): WordInfo? {
        with(params) {
            if (doesColumnContainObjects(currentColDivider)) {
                val wordInfo = WordInfoFactory(WordInfoFactory.Params(
                        descriptors,
                        shortenRowsDividers,
                        currentRowDividerIndex,
                        currentColDivider,
                        nextColDivider
                )).create()

                descriptors.removeAll(wordInfo.wordObjects)

                if (addWordToResultIfPossible(wordInfo)) return wordInfo
            }
            return null
        }
    }

    private fun doesColumnContainObjects(currentColDivider: Int) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun addWordToResultIfPossible(wordInfo: WordInfo): Boolean {
        with(wordInfo) {
            if (isWordPresent()) {
                wordsWithObjects[Descriptor(createWordMatOfPoint())] = wordObjects
                return true
            }
            return false
        }
    }

    private fun addRowWordsToResultIfPossible(rowWords: List<WordInfo>) {
        if (rowWords.isNotEmpty()) {
            val rowPoints = rowWords.flatMap { it.word }
            val rowMatOfPoint = MatOfPoint().apply { fromList(rowPoints) }
            rowsWithNumberOfWords[Descriptor(rowMatOfPoint)] = rowWords.size
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords() && isFirstRun

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(shouldCreateDebugInfo(), rows)
    }

    private fun persistImageWithColoredWords() {
        storage.saveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(), blackWhiteImage.size(),
                wordsWithObjects.values.toList())
    }

    private fun tryToDetectWordsInGapsIfPossible() {
        if (shouldTryToDetectWordsInGaps()) {
            logger.logPreRecursionInfo(descriptors.size)

            doWithRecursionLock {
                val imageWithUnassignedObjects = prepareImageWithUnassignedObjects()
                val recursionResult = doWithFieldsRestore { detect() }

                if (recursionResult is Success) {
                    updateOriginalResult(recursionResult)
                    logger.logRecursionInfo(descriptors)
                    persistRecursiveImageWithColoredWords(imageWithUnassignedObjects)
                }
            }
        }
    }

    private fun shouldTryToDetectWordsInGaps() = descriptors.isNotEmpty() && isRecursionAllowed()

    private fun doWithRecursionLock(action: () -> Unit) {
        forbidRecursion()
        action()
        allowRecursion()
    }

    private fun prepareImageWithUnassignedObjects(): Mat {
        val blackWhiteImageWithUnassignedObjects = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val dataReadable = ByteArray(blackWhiteImageWithUnassignedObjects.rows() *
                blackWhiteImageWithUnassignedObjects.cols() *
                blackWhiteImageWithUnassignedObjects.channels())
        blackWhiteImageWithUnassignedObjects.get(0, 0, dataReadable)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % blackWhiteImageWithUnassignedObjects.cols() +
                        p.y.toInt() * blackWhiteImageWithUnassignedObjects.cols()
                dataReadable[index] = WHITE
            }
        }
        blackWhiteImageWithUnassignedObjects.put(0, 0, dataReadable)
        cache.cacheBinaryBlackWhiteImage(blackWhiteImageWithUnassignedObjects)
        cache.cacheDescriptors(descriptors)
        return blackWhiteImageWithUnassignedObjects
    }

    private inline fun <T> doWithFieldsRestore(action: () -> T): T {
        val blackWhiteImageCopy = blackWhiteImage
        val descriptorsCopy = descriptors.toMutableList()
        val shortenRowsDividersCopy = shortenRowsDividers.toList()
        val mergedRowsDividersCopy = mergedRowsDividers.toList()
        val shortenColsDividersCopy = shortenColsDividers.toList()
        val mergedColsDividersCopy = mergedColsDividers.toList()
        val wordsWithObjectsCopy = wordsWithObjects.toMutableMap()
        val rowsWithNumberOfWordsCopy = rowsWithNumberOfWords.toMutableMap()

        val result = action()

        blackWhiteImage = blackWhiteImageCopy
        descriptors = descriptorsCopy.toMutableList()
        shortenRowsDividers = shortenRowsDividersCopy.toList()
        mergedRowsDividers = mergedRowsDividersCopy.toList()
        shortenColsDividers = shortenColsDividersCopy.toList()
        mergedColsDividers = mergedColsDividersCopy.toList()
        wordsWithObjects = wordsWithObjectsCopy.toMutableMap()
        rowsWithNumberOfWords = rowsWithNumberOfWordsCopy.toMutableMap()

        return result
    }

    private fun updateOriginalResult(recursionResult: Success) {
        wordsWithObjects.putAll(recursionResult.wordsWithObjects)
        recursionResult.wordsWithObjects.values.forEach { descriptors.removeAll(it) }
        rowsWithNumberOfWords.putAll(recursionResult.rowsWithNumberOfWords)
    }

    private fun persistRecursiveImageWithColoredWords(blackWhiteImage: Mat) {
        storage.saveRecursiveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(),
                blackWhiteImage.size(), wordsWithObjects.values.toList())
    }

    private fun shouldCreateDebugInfo() = DEBUG && isFirstRun

    private fun isRecursionAllowed() = isFirstRun

    private fun forbidRecursion() {
        isFirstRun = false
    }

    private fun allowRecursion() {
        isFirstRun = true
    }

    private data class WordsOnRowParams(
            val shortenColsDividers: List<Int>,
            val bgrImageWithWordsBoundaries: Mat?,
            val currentRowDividerIndex: Int,
            val rows: Mat
    ) {
        fun canExtractWordsOnRow() = shortenColsDividers.isNotEmpty()
    }

    private data class DrawParams(
            val condition: Boolean,
            val drawer: WordsDetectionDrawer,
            val shortenRowsDividers: List<Int>,
            val image: Mat,
            val currentRowDividerIndex: Int,
            val dividers: List<Int>,
            val drawAction: WordsDetectionDrawer.(Int, Int, List<Int>, Mat) -> Unit
    )

    private data class RowParams(
            val descriptors: MutableList<Descriptor>,
            val shortenRowsDividers: List<Int>,
            val shortenColsDividers: List<Int>,
            val currentShortenColsDivider: Int,
            val currentRowDividerIndex: Int
    ) {
        val currentColDivider = shortenColsDividers[currentShortenColsDivider]
        val nextColDivider = shortenColsDividers[currentShortenColsDivider + 1]
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val rowsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result()
    }
}