package cz.potass.learn.refactoringexamples.smells.general

internal class EncapsulateConditionals {
    fun example(result: String) {
        if (result.startsWith("#") && !result.contains("404")) {
            // Some code.
        }
    }

    fun refactoredExample(result: String) {
        if (canProcessResult(result)) {
            // Some code.
        }
    }

    private fun canProcessResult(result: String): Boolean {
        return result.startsWith("#") && !result.contains("404")
    }
}