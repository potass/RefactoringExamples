package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.R
import cz.potass.learn.refactoringexamples.showcase.final.WordsDetectionMode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.shared.BaseAsyncTask

internal class HorizontalWordsDetectionController(task: BaseAsyncTask) :
        WordsDetectionController(task) {
    override val onStartStringRes = R.string.progress_starting_horizontal_words_detection
    override val onWordsComponentsDetectionStringRes = R.string.progress_detecting_rows
    override val onWordsDetectionStringRes = R.string.progress_detecting_horizontal_words
    override val mode = HORIZONTAL
}