@file:Suppress("UNUSED_PARAMETER", "unused")

package cz.potass.learn.refactoringexamples.showcase.shared

class Rect(x: Int, y: Int, width: Int, height: Int) {
    fun contains(center: Point): Boolean {
        leftUnimplemented()
    }
}

class Scalar {
    constructor(value: Int)
    constructor(value1: Int, value2: Int, value3: Int)
}

object Imgproc {
    fun line(rows: Mat, point1: Point, point2: Point, scalar: Scalar, i1: Int, i2: Int, i3: Int) {
        leftUnimplemented()
    }
}

object Timber {
    fun d(msg: String) {
        leftUnimplemented()
    }

    fun w(msg: String) {
        leftUnimplemented()
    }

    fun e(msg: String) {
        leftUnimplemented()
    }

    fun i(msg: String) {
        leftUnimplemented()
    }
}

object ObjectDetection {
    fun findObjects(
            task: BaseAsyncTask,
            settings: Settings,
            binarizedImage: Mat,
            mergeObjects: Boolean
    ): MutableList<Descriptor> {
        leftUnimplemented()
    }

    fun findObjects(
            settings: Settings,
            binarizedImage: Mat,
            mergeObjects: Boolean,
            isCancelled: () -> Boolean
    ): MutableList<Descriptor> {
        leftUnimplemented()
    }
}

object Core {
    fun flip(mat: Mat, rotatedImage: Mat, rotation: Int) {
        leftUnimplemented()
    }
}

object ImageConstants {
    const val BINARIZED = ""
    const val BINARY_IMAGE_NAME = BINARIZED
    const val BLACK_WHITE = ""
    const val BLACK_WHITE_IMAGE_NAME = BLACK_WHITE
    const val ROWS_BOUNDARIES_HORIZONTALLY = ""
    const val HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME = ROWS_BOUNDARIES_HORIZONTALLY
    const val ROWS_BOUNDARIES_VERTICALLY = ""
    const val VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME = ROWS_BOUNDARIES_VERTICALLY
    const val WHITE_BLACK_BGR = ""
    const val WHITE_BLACK_BGR_IMAGE_NAME = WHITE_BLACK_BGR
    const val WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES = ""
    const val WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME = WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES
    const val WORDS_BOUNDARIES_HORIZONTALLY = ""
    const val HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME = WORDS_BOUNDARIES_HORIZONTALLY
    const val WORDS_BOUNDARIES_VERTICALLY = ""
    const val VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME = WORDS_BOUNDARIES_VERTICALLY
    const val WORDS_HORIZONTALLY = ""
    const val HORIZONTAL_WORDS_IMAGE_NAME = WORDS_HORIZONTALLY
    const val WORDS_VERTICALLY = ""
    const val VERTICAL_WORDS_IMAGE_NAME = WORDS_VERTICALLY
    const val WORDS_RECURSIVE_HORIZONTALLY = ""
    const val RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME = WORDS_RECURSIVE_HORIZONTALLY
    const val WORDS_RECURSIVE_VERTICALLY = ""
    const val RECURSIVE_VERTICAL_WORDS_IMAGE_NAME = WORDS_RECURSIVE_VERTICALLY
}

object Imgcodecs {
    const val CV_LOAD_IMAGE_UNCHANGED = 1

    fun imread(path: String, loadType: Int): Mat {
        leftUnimplemented()
    }
}

interface BaseAsyncTask {
    fun doProgress(res: Int)
    fun isCancelled(): Boolean
}

enum class GapType {
    AVERAGE_OBJECT_SIZE,
    USER_DEFINED,
    AVERAGE_GAP_SIZE;

    fun setValue(value: Float) {
        leftUnimplemented()
    }

    fun getValue(): Float {
        leftUnimplemented()
    }
}

interface Image {
    fun getBasePath(): String
}

class Descriptor(points: MatOfPoint) {
    val straightRectangleHeight = 1f
    val straightRectangleWidth = 1f
    val center = Point()
    val `object` = MatOfPoint()
    val matrix = MatOfPoint()
}

interface Triplet<out T, out U, out V> {
    companion object {
        fun <T, U, V> create(first: T, second: U, third: V): Triplet<T, U, V> {
            leftUnimplemented()
        }
    }

    val first: T
    val second: U
    val third: V
}

enum class Task {
    PROCESS_IMAGE_TASK,
    RESET_WORDS_TASK,
    PROCESS_COLUMNS_TASK
}

interface ProcessImageTask : BaseAsyncTask

interface ResetWordsTask : BaseAsyncTask

interface ProcessColumnsTask : BaseAsyncTask

class Mat(size: Size, type: Any) {
    companion object {
        fun zeros(size: Size, type: Any): Mat {
            leftUnimplemented()
        }
    }

    fun size(): Size {
        leftUnimplemented()
    }

    fun type(): Any {
        leftUnimplemented()
    }

    fun t(): Mat {
        leftUnimplemented()
    }

    fun rows(): Int {
        leftUnimplemented()
    }

    fun cols(): Int {
        leftUnimplemented()
    }

    fun channels(): Int {
        leftUnimplemented()
    }

    fun get(x: Int, y: Int, imageData: ByteArray) {
        leftUnimplemented()
    }

    fun clone(): Mat {
        leftUnimplemented()
    }

    fun release() {
        leftUnimplemented()
    }

    fun rowRange(from: Int, to: Int): Mat {
        leftUnimplemented()
    }

    fun put(i: Int, j: Int, data: ByteArray) {
        leftUnimplemented()
    }
}

interface Size

class MatOfPoint {
    fun toList(): List<Point> {
        leftUnimplemented()
    }

    fun fromList(points: List<Point>) {
        leftUnimplemented()
    }
}

class Point(val x: Float, val y: Float) {
    constructor() : this(0f, 0f)
    constructor(x: Int, y: Int) : this(x.toFloat(), y.toFloat())
}

class Cache {
    companion object {
        fun get(): Cache {
            leftUnimplemented()
        }
    }

    fun getBinaryBlackWhiteImage(): Mat? {
        leftUnimplemented()
    }

    fun cacheBinaryBlackWhiteImage(blackWhiteImage: Mat) {
        leftUnimplemented()
    }

    fun releaseBinaryBlackWhiteImage() {
        leftUnimplemented()
    }

    fun getDescriptors(): MutableList<Descriptor>? {
        leftUnimplemented()
    }

    fun cacheDescriptors(descriptors: List<Descriptor>) {
        leftUnimplemented()
    }

    fun releaseDescriptors() {
        leftUnimplemented()
    }

    fun getBgrImage(): Mat? {
        leftUnimplemented()
    }

    fun getBgrImageWithWordsBoundaries(): Mat? {//
        leftUnimplemented()
    }

    fun cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries: Mat) {
        leftUnimplemented()
    }

    fun releaseBgrImageWithWordsBoundaries() {
        leftUnimplemented()
    }
}

class Settings {
    companion object {
        fun get(): Settings {
            leftUnimplemented()
        }
    }

    fun shouldMergeObjects(): Boolean {
        leftUnimplemented()
    }

    fun shouldShowGapsBetweenWords(): Boolean {
        leftUnimplemented()
    }
}

private fun leftUnimplemented(): Nothing = throw Exception()

/**
 * Rotates `image` by `multiple` of 90 degrees (1 means 90 degrees, 2 means 180
 * degrees, ...).
 *
 * @param image Image to be rotated
 * @param multiple Multiple of 90 degrees which defines the rotation angle
 * @return Rotated image.
 */
fun rotateBy90Multiple(image: Mat, multiple: Int): Mat {
    var rotatedImage = Mat(image.size(), image.type())
    when (multiple % 4) {
        0 ->
            // No rotation needed.
            rotatedImage = image
        1 ->
            // Rotation by 90 degrees.
            Core.flip(image.t(), rotatedImage, 1)
        2 ->
            // Rotation by 180 degrees.
            Core.flip(image.t(), rotatedImage, -1)
        3 ->
            // Rotation by 270 degrees.
            Core.flip(image.t(), rotatedImage, 0)
    }
    return rotatedImage
}

/**
 * Retrieves indexes of rows which are likely gaps between rows with objects.
 *
 * @param histogram Horizontal histogram
 * @return Indexes of rows divider or `null` if there are none.
 */
fun getRowsDividers(histogram: FloatArray): List<Int>? {
    val minimum = histogram.min()!!
    var sum = 0f
    var divider = histogram.size.toFloat()
    for (i in histogram) {
        if (i == 0f) divider--
        sum += i
    }
    // Average of non-zero values.
    val avg = sum / divider

    // Because it is likely that user will not perfectly balance the image, I define a thresh
    // which permits a variance. I prefer less indexes than more.
    val thresh = avg * 0.33f
    Timber.d("minimum = $minimum, sum = $sum, divider = $divider, avg = $avg, thresh = $thresh")
    if (minimum > thresh) return null

    val indexes = ArrayList<Int>()
    for (i in histogram.indices) {
        // If value is not lower than this thresh I will not take it into account. Exception is
        // the first and the last row (necessary to be able to detect cut rows).
        if (histogram[i] < thresh || i == 0 || i == histogram.size - 1) {
            indexes.add(i)
        }
    }

    return if (indexes.size > 0) indexes else null
}

/**
 * Finds continuous sequences of rows dividers and ads another rows if they are not considered
 * to be rows with objects.
 * <br></br><br></br>
 * There are two options how to determine if two sequences should be merged:
 *
 *  1. Using average height of objects
 *  1. Using user-defined value
 *
 * Use `rowGapType` parameter to specify this behaviour.<br></br>
 * NOTE: Using average height of gaps is not supported.
 *
 * @param rowsDividers Indexes of rows dividers
 * @param rowGapType [GapType] which specifies how to merge two sequences.
 * @return Indexes of merged rows divider.
 */
fun mergeRowsDividers(rowsDividers: List<Int>, rowGapType: GapType): List<Int> {
    val indexes = ArrayList<Int>()

    // Creating sequences of rows dividers which are continuous.
    val clusteredDividers = clusterDividers(rowsDividers)

    // Thresh value indicates minimal height of row with objects.
    val thresh = when (rowGapType) {
        GapType.USER_DEFINED -> rowGapType.getValue()
        GapType.AVERAGE_OBJECT_SIZE -> rowGapType.getValue() * 0.5f
        GapType.AVERAGE_GAP_SIZE -> throw IllegalArgumentException("AVERAGE_GAP_SIZE is not supported.")
    }

    // Checking if space between 2 sequences should be considered as another gap.
    for (i in 0 until clusteredDividers.size - 1) {
        val current = clusteredDividers[i]
        val next = clusteredDividers[i + 1]

        indexes.addAll(current)

        val firstNextValue = next[0]
        val lastCurrentValue = current[current.size - 1]

        if (firstNextValue - lastCurrentValue < thresh) {
            for (j in lastCurrentValue + 1 until firstNextValue) {
                indexes.add(j)
            }
        }

        // Adding last sequence.
        if (i == clusteredDividers.size - 2) {
            indexes.addAll(next)
        }
    }

    return indexes
}

/**
 * Creates sequences of dividers which are continuous.
 *
 * @param dividers Indexes of dividers
 * @return Continuous sequences of dividers.
 */
fun clusterDividers(dividers: List<Int>): List<List<Int>> {
    val clusteredDividers = ArrayList<List<Int>>()
    var start = 0
    var end = 0
    for (i in 1 until dividers.size) {
        if (dividers[i - 1] == dividers[i] - 1) {
            end = i
            if (i == dividers.size - 1) {
                clusteredDividers.add(dividers.subList(start, end + 1))
            }
        } else {
            clusteredDividers.add(dividers.subList(start, end + 1))
            end = i
            start = end
            // When the last index is the only one in 'cluster', add it, otherwise it would be
            // ignored.
            if (i == dividers.size - 1) {
                clusteredDividers.add(listOf(dividers[i]))
            }
        }
    }
    return clusteredDividers
}

/**
 * Shortens sequences of dividers and preserves only first and last divider from each sequence.
 *
 * @param dividers Indexes of dividers (rows or columns)
 * @return Shortened dividers.
 */
fun shortenDividers(dividers: List<Int>): List<Int> {
    val indexes = ArrayList<Int>()
    var start = 0
    var end = 0
    for (i in 1 until dividers.size) {
        if (dividers[i - 1] == dividers[i] - 1) {
            end = i
            if (i == dividers.size - 1) {
                indexes.add(dividers[start])
                indexes.add(dividers[end])
            }
        } else {
            indexes.add(dividers[start])
            if (start != end) indexes.add(dividers[end])
            end = i
            start = end
            // When the last index is the only one in 'cluster', add it, otherwise it would be
            // ignored.
            if (i == dividers.size - 1) {
                indexes.add(dividers[i])
            }
        }
    }
    return indexes
}

/**
 * Retrieves indexes of columns which are likely gaps between words. This method is more strict
 * than [.getRowsDividers] because it does not allow any threshold value, only
 * zero is accepted as gap (or borders).
 *
 * @param histogram Vertical histogram
 * @return Indexes of columns divider or `null` if there are none.
 */
fun getColumnsDividers(histogram: FloatArray): List<Int>? {
    val indexes = ArrayList<Int>()
    for (i in histogram.indices) {
        // Last two conditions assure that there will always be indexes of columns representing
        // row borders, which is necessary to be able to detect cut (border) words.
        if (histogram[i] == 0f || i == 0 || i == histogram.size - 1) {
            indexes.add(i)
        }
    }
    return if (indexes.size > 0) indexes else null
}

/**
 * Finds continuous sequences of columns dividers and ads another columns if they are not
 * considered to be columns with objects.
 * <br></br><br></br>
 * There are three options how to determine if two sequences should be merged:
 *
 *  1. Using average height of objects
 *  1. Using average height of gaps
 *  1. Using user-defined value
 *
 * Use `wordGapType` parameter to specify this behaviour.
 *
 * @param columnsDividers Indexes of columns dividers
 * @param wordGapType [GapType] which specifies how to merge two sequences.
 * @param cols Number of columns
 * @return Indexes of merged columns divider.
 */
fun mergeColumnsDividers(columnsDividers: List<Int>, wordGapType: GapType, cols: Int): List<Int> {
    val indexes = ArrayList<Int>()

    // Creating sequences of columns dividers which are continuous.
    val clusteredDividers = clusterDividers(columnsDividers)

    // Thresh value indicates minimal gap width between words.
    var thresh = 0.0f
    when (wordGapType) {
        GapType.USER_DEFINED -> thresh = wordGapType.getValue()
        GapType.AVERAGE_OBJECT_SIZE -> thresh = wordGapType.getValue() * 0.5f
        GapType.AVERAGE_GAP_SIZE -> {
            // In this case thresh value must be computed.
            // Firstly, I eliminate big gaps which can be at the beginning and at the end of row.
            // Secondly, I search for the biggest and the smallest gap and determine the threshold.

            val startIndex = if (clusteredDividers[0][0] == 0) 1 else 0
            val size = clusteredDividers.size
            val lastSize = clusteredDividers[size - 1].size
            val endIndex = if (clusteredDividers[size - 1][lastSize - 1] == cols - 1) size - 1 else size
            var biggestGap = 0
            var smallestGap = Integer.MAX_VALUE

            Timber.d("number of columns = $cols, start index = $startIndex, end index = $endIndex")

            val clusteredFilteredDividers = ArrayList(clusteredDividers.subList(startIndex, endIndex))
            for (i in clusteredFilteredDividers.indices) {
                val current = clusteredFilteredDividers[i]
                val currentSize = current.size
                if (currentSize > biggestGap) {
                    biggestGap = currentSize
                }
                if (currentSize < smallestGap) {
                    smallestGap = currentSize
                }
            }

            thresh = (smallestGap + 0.5 * (biggestGap - smallestGap)).toFloat()

            Timber.d("smallest gap = $smallestGap, biggest gap = $biggestGap, thresh = $thresh")
        }
    }

    // Simply check size of each index cluster and compare it with thresh.
    for (i in clusteredDividers.indices) {
        val current = clusteredDividers[i]
        // Last two conditions assure that there will always be indexes of columns representing
        // row borders (this is dependent on getColumnsDividers() which must handle this scenario
        // the same way), which is necessary to be able to detect cut (border) words and words
        // which are simply too near to borders (in this case divider between word and border
        // would be ignored).
        if (current.size > thresh || i == 0 || i == clusteredDividers.size - 1) {
            indexes.addAll(current)
        }
    }

    return indexes
}

fun conditionalSave(debug: Boolean, image: Image, imageMat: Mat, suffix: String) {
    // Not important logic for this show-case.
}

fun conditionalColorAndSave(debug: Boolean, image: Image, size: Size, objects: List<List<Descriptor>>, suffix: String) {
    // Not important logic for this show-case.
}