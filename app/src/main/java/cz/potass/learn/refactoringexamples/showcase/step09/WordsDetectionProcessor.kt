package cz.potass.learn.refactoringexamples.showcase.step09

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step06.*
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.VERTICAL
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfoFactory
import cz.potass.learn.refactoringexamples.showcase.step09.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Some small minor refactorings.
 *  1. Extract recursion logic, make caller responsible for that.
 */
internal abstract class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    protected companion object {
        const val PATH_SEPARATOR = "/"
        const val WHITE = WordsDetectionDrawer.WHITE.toByte()
    }

    protected val cache = Cache.get()
    protected val settings = Settings.get()
    protected val logger = WordsDetectionLogger()
    private val storage = WordsDetectionStorage(params)
    private val drawer = WordsDetectionDrawer()

    private lateinit var blackWhiteImage: Mat
    protected lateinit var descriptors: MutableList<Descriptor> private set
    private lateinit var mergedRowsDividers: List<Int>
    private lateinit var shortenRowsDividers: List<Int>
    private lateinit var mergedColsDividers: List<Int>
    private lateinit var shortenColsDividers: List<Int>
    private lateinit var wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
    private lateinit var rowsWithNumberOfWords: MutableMap<Descriptor, Int>

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     * TODO create 3 classes, one for each logical procedure?
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    open fun detect(): Result {
        params.onStart()

        loadBlackWhiteImage()
        loadDescriptors()

        return detectWordsIfAnyObjectIsPresent()
    }

    private fun loadBlackWhiteImage() {
        blackWhiteImage = cache.getBinaryBlackWhiteImage() ?: run {
            var bwImage = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) bwImage = rotateBy90Multiple(bwImage, 1)
            bwImage.apply { cache.cacheBinaryBlackWhiteImage(this) }
        }
    }

    private fun loadDescriptors() {
        descriptors = cache.getDescriptors() ?: run {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        }
    }

    private fun detectWordsIfAnyObjectIsPresent(): Result {
        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            detectWordsOnRowsIfPossible()?.run { return this }
        }
        return NoObjectsFound
    }

    private fun detectWordsOnRowsIfPossible(): Result? {
        val numberOfObjectPixelsOnRows = createHorizontalHistogram()

        getRowsDividers(numberOfObjectPixelsOnRows)?.run {
            prepareForWordsDetectionOnEachRow(this)

            val rows = blackWhiteImage.clone()
            val bgrImageWithWordsBoundaries =
                    if (settings.shouldShowGapsBetweenWords()) loadBgrImageWithWordsBoundaries()
                    else null
            if (detectWordsOnEachRow(rows, bgrImageWithWordsBoundaries)) return Cancelled
            persistWordsImages(bgrImageWithWordsBoundaries, rows)

            return Success(wordsWithObjects, descriptors, rowsWithNumberOfWords)
        } ?: logger.logNoRowDividers()
        return null
    }

    private fun prepareForWordsDetectionOnEachRow(rowsDividers: List<Int>) {
        updateGapTypeAveragesIfNecessary()
        mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
        shortenRowsDividers = shortenDividers(mergedRowsDividers)
        drawRowsBoundariesIfNecessary()
        params.onWordsDetection()
        wordsWithObjects = LinkedHashMap()
        rowsWithNumberOfWords = LinkedHashMap()
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        return cache.getBgrImageWithWordsBoundaries()?.clone() ?: run {
            val result = cache.getBgrImage()?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                            CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) rotateBy90Multiple(result, 1) else result
        }
    }

    private fun createHorizontalHistogram() = createHistogram(blackWhiteImage, HORIZONTAL)

    private fun detectWordsOnEachRow(rows: Mat, bgrImageWithWordsBoundaries: Mat?): Boolean {
        for (i in 0 until shortenRowsDividers.size - 1) {
            val currentRowDivider = shortenRowsDividers[i]
            if (doesRowContainObjects(currentRowDivider)) {
                if (params.isCancelled()) return true

                val nextRowDivider = shortenRowsDividers[i + 1]
                val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                val numberOfObjectPixelsOnCols = createVerticalHistogram(row)

                getColumnsDividers(numberOfObjectPixelsOnCols)?.run {
                    mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                    shortenColsDividers = shortenDividers(mergedColsDividers)

                    extractWordsOnRowIfPossible(WordsOnRowParams(
                            shortenColsDividers, bgrImageWithWordsBoundaries, i, rows))
                }
            }
        }
        return false
    }

    private fun extractWordsOnRowIfPossible(params: WordsOnRowParams) {
        with(params) {
            if (canExtractWordsOnRow()) {
                bgrImageWithWordsBoundaries
                        ?.run { drawWordsBoundariesIfNecessary(this, currentRowDividerIndex) }
                drawColumnsBoundariesIfNecessary(rows, currentRowDividerIndex)

                val rowWords = extractWordsOnRow(currentRowDividerIndex)
                addRowWordsToResultIfPossible(rowWords)
            }
        }
    }

    private fun persistWordsImages(bgrImageWithWordsBoundaries: Mat?, rows: Mat) {
        bgrImageWithWordsBoundaries?.let(::persistBgrImageWithWordsBoundariesIfPossible)
        persistImageWithWordsBoundaries(rows)
        persistImageWithColoredWords(blackWhiteImage.size())
    }

    private fun createVerticalHistogram(image: Mat) = createHistogram(image, VERTICAL)

    private fun createHistogram(image: Mat, mode: WordsDetectionMode): FloatArray {
        image.run {
            val numberOfObjectPixels = FloatArray(if (mode.isHorizontal()) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE) {
                    if (mode.isHorizontal()) {
                        numberOfObjectPixels[i / cols()]++
                    } else {
                        numberOfObjectPixels[i % cols()]++
                    }
                }
            }
            return numberOfObjectPixels
        }
    }

    private fun updateGapTypeAveragesIfNecessary() {
        updateRowGapTypeAverageIfNecessary()
        updateWordGapTypeAverageIfNecessary()
    }

    private fun updateRowGapTypeAverageIfNecessary() {
        if (params.isRowAverageObjectSizeGapType()) {
            val heightAverage = descriptors.map { it.straightRectangleHeight }.average()
            params.setRowGapTypeValue(heightAverage.toFloat())
        }
    }

    private fun updateWordGapTypeAverageIfNecessary() {
        if (params.isWordAverageObjectSizeGapType()) {
            val widthAverage = descriptors.map { it.straightRectangleWidth }.average()
            params.setWordGapTypeValue(widthAverage.toFloat())
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) {
            val rows = blackWhiteImage.clone()
            drawer.drawHorizontalBoundaries(shortenRowsDividers, rows, blackWhiteImage.cols())
            storage.saveRowsBoundariesImage(rows)
            rows.release()
        }
    }

    private fun drawWordsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        drawVerticalBoundariesIfNecessary(DrawParams(
                shouldDrawWordsBoundaries(),
                drawer,
                shortenRowsDividers,
                image,
                currentRowDividerIndex,
                mergedColsDividers
        ) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawWordsBoundaries(arg1, arg2, arg3, arg4)
        })
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        drawVerticalBoundariesIfNecessary(DrawParams(
                shouldCreateDebugInfo(),
                drawer,
                shortenRowsDividers,
                image,
                currentRowDividerIndex,
                shortenColsDividers
        ) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawColumnsBoundaries(arg1, arg2, arg3, arg4)
        })
    }

    private fun drawVerticalBoundariesIfNecessary(params: DrawParams) {
        with(params) {
            if (condition) {
                drawAction(
                        drawer,
                        shortenRowsDividers[currentRowDividerIndex],
                        shortenRowsDividers[currentRowDividerIndex + 1],
                        dividers,
                        image
                )
            }
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    private fun extractWordsOnRow(currentRowDividerIndex: Int): List<WordInfo> {
        val rowWords = mutableListOf<WordInfo>()
        for (i in 0 until shortenColsDividers.size - 1) {
            extractWordOnRowUsingColumnsRangeIfPossible(RowParams(
                    descriptors, shortenRowsDividers, shortenColsDividers, i, currentRowDividerIndex
            ))?.let(rowWords::add)
        }
        return rowWords
    }

    private fun extractWordOnRowUsingColumnsRangeIfPossible(params: RowParams): WordInfo? {
        with(params) {
            if (doesColumnContainObjects(currentColDivider)) {
                val wordInfo = WordInfoFactory(WordInfoFactory.Params(
                        descriptors,
                        shortenRowsDividers,
                        currentRowDividerIndex,
                        currentColDivider,
                        nextColDivider
                )).create()

                descriptors.removeAll(wordInfo.wordObjects)

                if (addWordToResultIfPossible(wordInfo)) return wordInfo
            }
            return null
        }
    }

    private fun doesColumnContainObjects(currentColDivider: Int) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun addWordToResultIfPossible(wordInfo: WordInfo): Boolean {
        with(wordInfo) {
            if (isWordPresent()) {
                wordsWithObjects[Descriptor(createWordMatOfPoint())] = wordObjects
                return true
            }
            return false
        }
    }

    private fun addRowWordsToResultIfPossible(rowWords: List<WordInfo>) {
        if (rowWords.isNotEmpty()) {
            val rowPoints = rowWords.flatMap { it.word }
            val rowMatOfPoint = MatOfPoint().apply { fromList(rowPoints) }
            rowsWithNumberOfWords[Descriptor(rowMatOfPoint)] = rowWords.size
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    protected abstract fun shouldShowGapsBetweenWords(): Boolean

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(shouldCreateDebugInfo(), rows)
    }

    private fun persistImageWithColoredWords(blackWhiteImageSize: Size) {
        persistImageWithColoredWords { arg1, arg2 ->
            saveImageWithColoredWordsIfPossible(arg1, blackWhiteImageSize, arg2)
        }
    }

    protected fun persistImageWithColoredWords(
            saveAction: WordsDetectionStorage.(Boolean, List<List<Descriptor>>) -> Unit
    ) {
        saveAction(storage, shouldCreateDebugInfo(), wordsWithObjects.values.toList())
    }

    protected abstract fun shouldCreateDebugInfo(): Boolean

    private data class WordsOnRowParams(
            val shortenColsDividers: List<Int>,
            val bgrImageWithWordsBoundaries: Mat?,
            val currentRowDividerIndex: Int,
            val rows: Mat
    ) {
        fun canExtractWordsOnRow() = shortenColsDividers.isNotEmpty()
    }

    private data class DrawParams(
            val condition: Boolean,
            val drawer: WordsDetectionDrawer,
            val shortenRowsDividers: List<Int>,
            val image: Mat,
            val currentRowDividerIndex: Int,
            val dividers: List<Int>,
            val drawAction: WordsDetectionDrawer.(Int, Int, List<Int>, Mat) -> Unit
    )

    private data class RowParams(
            val descriptors: MutableList<Descriptor>,
            val shortenRowsDividers: List<Int>,
            val shortenColsDividers: List<Int>,
            val currentShortenColsDivider: Int,
            val currentRowDividerIndex: Int
    ) {
        val currentColDivider = shortenColsDividers[currentShortenColsDivider]
        val nextColDivider = shortenColsDividers[currentShortenColsDivider + 1]
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val rowsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result() {
            fun isAnyObjectUnprocessed() = descriptors.isNotEmpty()
        }
    }
}