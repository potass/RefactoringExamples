package cz.potass.learn.refactoringexamples.showcase.step02

import cz.potass.learn.refactoringexamples.showcase.shared.BaseAsyncTask
import cz.potass.learn.refactoringexamples.showcase.shared.GapType
import cz.potass.learn.refactoringexamples.showcase.shared.Image
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProcessor.Mode

internal abstract class WordsDetectionController(private val task: BaseAsyncTask) :
        WordsDetectionProgressListener {
    protected abstract val onStartStringRes: Int
    protected abstract val onWordsComponentsDetectionStringRes: Int
    protected abstract val onWordsDetectionStringRes: Int
    protected abstract val mode: Mode

    fun detect(
            isCacheAvailable: Boolean,
            rowGapType: GapType,
            wordGapType: GapType,
            image: Image,
            log: Boolean
    ) {
        // Skipping result handling.
        WordsDetectionProcessor(this).detect(
                isCacheAvailable,
                rowGapType,
                wordGapType,
                image,
                mode,
                true,
                true,
                log
        ) { task.isCancelled() }
    }

    override fun onStart() {
        onStartStringRes.doProgress()
    }

    override fun onWordsComponentsDetection() {
        onWordsComponentsDetectionStringRes.doProgress()
    }

    override fun onWordsDetection() {
        onWordsDetectionStringRes.doProgress()
    }

    private fun Int.doProgress() {
        task.doProgress(this)
    }
}