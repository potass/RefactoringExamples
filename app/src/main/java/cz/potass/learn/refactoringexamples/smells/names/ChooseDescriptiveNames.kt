package cz.potass.learn.refactoringexamples.smells.names

// Similar to ObscuredIntent.
internal class ChooseDescriptiveNames {
    private val l = listOf(9, 7, 10, 10, 10, 5, 8, 6, 8, 10)

    fun x(): Int {
        var q = 0
        var z = 0
        repeat(10) {
            when {
                l[z] == 10 -> {
                    q += 10 + (l[z + 1] + l[z + 2])
                    z++
                }
                l[z] + l[z + 1] == 10 -> {
                    q += 10 + l[z + 2]
                    z += 2
                }
                else -> {
                    q += l[z] + l[z + 1]
                    z += 2
                }
            }
        }
        return q
    }

    private val rolls = listOf(9, 7, 10, 10, 10, 5, 8, 6, 8, 10)

    fun score(): Int {
        var score = 0
        var frame = 0
        repeat(10) {
            when {
                isStrike(frame) -> {
                    score += 10 + nextTwoBallsForStrike(frame)
                    frame++
                }
                isSpare(frame) -> {
                    score += 10 + nextBallForSpare(frame)
                    frame += 2
                }
                else -> {
                    score += twoBallsInFrame(frame)
                    frame += 2
                }
            }
        }
        return score
    }

    private fun isStrike(frame: Int) = rolls[frame] == 10

    private fun nextTwoBallsForStrike(frame: Int) = rolls[frame + 1] + rolls[frame + 2]

    private fun isSpare(frame: Int) = twoBallsInFrame(frame) == 10

    private fun nextBallForSpare(frame: Int) = rolls[frame + 2]

    private fun twoBallsInFrame(frame: Int) = rolls[frame] + rolls[frame + 1]
}