package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Timber

internal class WordsDetectionLogger {
    fun logNoRowDividers() = Timber.d("No row dividers detected.")

    fun logPreRecursionInfo(numberOfObjects: Int) {
        Timber.w("$numberOfObjects objects have not been assigned to any word. Processing" +
                " unassigned objects once again.")
    }

    fun logRecursionInfo(descriptors: List<Descriptor>) {
        if (descriptors.isNotEmpty()) {
            Timber.w("${descriptors.size} objects have not been assigned to any word during " +
                    "recursive call.")
        } else Timber.i("All objects have been successfully used while detecting words.")
    }
}