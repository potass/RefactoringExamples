package cz.potass.learn.refactoringexamples.smells.general

import java.time.Month
import java.time.Month.*

internal class ObviousBehaviorIsUnimplemented {
    fun monthExample(month: String): Month? {
        return when (month) {
            "January" -> JANUARY
            "February" -> FEBRUARY
            "March" -> MARCH
            "April" -> APRIL
            "May" -> MAY
            "June" -> JUNE
            "July" -> JULY
            "August" -> AUGUST
            "September" -> SEPTEMBER
            "October" -> OCTOBER
            "November" -> NOVEMBER
            "December" -> DECEMBER
            else -> null
        }
    }

    fun refactoredMonthExample(month: String): Month? {
        return when (month.toLowerCase()) {
            "january", "jan" -> JANUARY
            "february", "feb" -> FEBRUARY
            "march", "mar" -> MARCH
            "april", "apr" -> APRIL
            "may" -> MAY
            "june", "jun" -> JUNE
            "july", "jul" -> JULY
            "august", "aug" -> AUGUST
            "september", "sep" -> SEPTEMBER
            "october", "oct" -> OCTOBER
            "november", "nov" -> NOVEMBER
            "december", "dec" -> DECEMBER
            else -> null
        }
    }
}