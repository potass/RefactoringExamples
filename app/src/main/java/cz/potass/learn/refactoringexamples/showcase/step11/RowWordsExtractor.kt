package cz.potass.learn.refactoringexamples.showcase.step11

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionDrawer

internal class RowWordsExtractor(private val params: Params) {
    fun extract(): List<WordInfo> {
        with(params) {
            bgrImageWithWordsBoundaries?.run { drawWordsBoundariesIfNecessary() }
            drawColumnsBoundariesIfNecessary()
            return extractWordsOnRow()
        }
    }

    private fun Params.drawWordsBoundariesIfNecessary() {
        if (shouldDrawWordsBoundaries) {
            drawer.drawWordsBoundaries(WordsDetectionDrawer.Params(
                    shortenRowsDividers,
                    bgrImageWithWordsBoundaries!!,
                    currentRowDividerIndex,
                    mergedColsDividers
            ))
        }
    }

    private fun Params.drawColumnsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo) {
            drawer.drawColumnsBoundaries(WordsDetectionDrawer.Params(
                    shortenRowsDividers,
                    rows,
                    currentRowDividerIndex,
                    shortenColsDividers
            ))
        }
    }

    private fun Params.extractWordsOnRow(): List<WordInfo> {
        val rowWords = mutableListOf<WordInfo>()
        for (i in 0 until shortenColsDividers.size - 1) {
            extractWordOnRowIfPossible(WordInfoExtractor.Params(
                    descriptors, shortenRowsDividers, shortenColsDividers, i, currentRowDividerIndex
            ))?.let(rowWords::add)
        }
        return rowWords
    }

    private fun Params.extractWordOnRowIfPossible(params: WordInfoExtractor.Params): WordInfo? {
        with(params) {
            return if (doesColumnContainObjects(currentColDivider)) {
                WordInfoExtractor(this).extract().run {
                    if (isWordPresent()) this else null
                }
            } else null
        }
    }

    data class Params(
            val drawer: WordsDetectionDrawer,
            val shortenColsDividers: List<Int>,
            val bgrImageWithWordsBoundaries: Mat?,
            val shouldDrawWordsBoundaries: Boolean,
            val shortenRowsDividers: List<Int>,
            val mergedColsDividers: List<Int>,
            val currentRowDividerIndex: Int,
            val rows: Mat,
            val shouldCreateDebugInfo: Boolean,
            val descriptors: List<Descriptor>,
            val doesColumnContainObjects: (Int) -> Boolean
    ) {
        fun canExtractWordsOnRow() = shortenColsDividers.isNotEmpty()
    }
}