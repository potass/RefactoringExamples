package cz.potass.learn.refactoringexamples.showcase.step01

import cz.potass.learn.refactoringexamples.R
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.GapType.AVERAGE_OBJECT_SIZE
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.shared.Task.*
import java.util.*

/**
 *  1. Rename class to make its meaning clear.
 *  1. Fix typos like binarized -> binary.
 *  1. Shorten imports where meaningful.
 *  1. Rename some imported constants.
 *  1. Remove string concatenation.
 *  1. Extract constant '/'.
 *  1. Simplify when with [Task].
 */
internal object DigitalImageProcessing {
    private const val PATH_SEPARATOR = "/"

    private val CACHE = Cache.get()
    private lateinit var mSettings: Settings

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically sending info about progress to the task.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     *
     * @param task [BaseAsyncTask] which is running current code, only instance of
     * [ProcessImageTask], [ResetWordsTask] or [ProcessColumnsTask]
     * is accepted
     * @param isCacheAvailable Determines whether cache is available or not
     * @param rowGapType [GapType] used to detect rows, [GapType.AVERAGE_GAP_SIZE] is
     * not supported in this case
     * @param wordGapType [GapType] used to detect words
     * @param image [Image] which words are searched
     * @param horizontally Determines whether to search words in rows or in columns
     * @param allowRecursion If `true` then algorithm can recursively call itself
     * @param firstRun Determines whether this method call is the first one or if it is the recursion
     * @param log If `true` then all partial result images are stored and added to gallery,
     * should be `true` when in debug mode except the situation when method is
     * called recursively
     * @return [Triplet] which first item is a [LinkedHashMap], which keys are
     * founded words and values are their objects, second item is a [List] of unused objects
     * and the third item is also a [LinkedHashMap], which keys are founded segments
     * and values are their number of words.
     */
    fun findWords(task: BaseAsyncTask, isCacheAvailable: Boolean, rowGapType: GapType, wordGapType: GapType,
                  image: Image, horizontally: Boolean, allowRecursion: Boolean, firstRun: Boolean, log: Boolean):
            Triplet<Map<Descriptor, List<Descriptor>>, List<Descriptor>, Map<Descriptor, Int>>? {
        val taskType: Task = when (task) {
            is ProcessImageTask -> PROCESS_IMAGE_TASK
            is ResetWordsTask -> RESET_WORDS_TASK
            is ProcessColumnsTask -> PROCESS_COLUMNS_TASK
            else -> throw IllegalArgumentException("Task ${task.javaClass.simpleName} is not supported.")
        }

        when (taskType) {
            PROCESS_IMAGE_TASK, RESET_WORDS_TASK -> task.doProgress(R.string.progress_starting_words_detection)
            PROCESS_COLUMNS_TASK -> task.doProgress(R.string.progress_starting_columns_detection)
        }

        val basePath = image.getBasePath()

        var blackWhiteImage: Mat
        val descriptors: MutableList<Descriptor>
        if (isCacheAvailable) {
            // Cache is not available when searching for words in columns.
            blackWhiteImage = CACHE.getBinaryBlackWhiteImage()!!
            descriptors = CACHE.getDescriptors()!!
        } else {
            var binaryImage = imread(
                    "$basePath$PATH_SEPARATOR$BINARY_IMAGE_NAME", CV_LOAD_IMAGE_UNCHANGED)
            blackWhiteImage = imread(
                    "$basePath$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME", CV_LOAD_IMAGE_UNCHANGED)
            if (!horizontally) {
                binaryImage = rotateBy90Multiple(binaryImage, 1)
                blackWhiteImage = rotateBy90Multiple(blackWhiteImage, 1)
            }
            CACHE.cacheBinaryBlackWhiteImage(blackWhiteImage)
            descriptors = findObjects(task, mSettings, binaryImage, mSettings.shouldMergeObjects())
        }

        if (descriptors.isNotEmpty()) {
            if (task.isCancelled()) return null
            when (taskType) {
                PROCESS_IMAGE_TASK, RESET_WORDS_TASK -> task.doProgress(R.string.progress_detecting_rows)
                PROCESS_COLUMNS_TASK -> task.doProgress(R.string.progress_detecting_columns)
            }

            // Creating horizontal histogram.
            val numberOfObjectPixelsOnRows = FloatArray(blackWhiteImage.rows())
            val imageData = ByteArray(blackWhiteImage.rows() * blackWhiteImage.cols() * blackWhiteImage.channels())
            blackWhiteImage.get(0, 0, imageData)
            for (i in imageData.indices) {
                if (imageData[i] == 255.toByte()) {
                    numberOfObjectPixelsOnRows[i / blackWhiteImage.cols()]++
                }
            }

            val rowsDividers = getRowsDividers(numberOfObjectPixelsOnRows)
            Timber.d("Rows dividers are ${if (rowsDividers == null) "NULL" else "NOT NULL"}.")

            if (rowsDividers != null) {
                val showFoundGapsBetweenWords = mSettings.shouldShowGapsBetweenWords()

                var heightSum = 0.0f
                var widthSum = 0.0f
                for (d in descriptors) {
                    heightSum += d.straightRectangleHeight
                    widthSum += d.straightRectangleWidth
                }

                if (rowGapType == AVERAGE_OBJECT_SIZE) {
                    val heightAvg = heightSum / descriptors.size.toFloat()
                    rowGapType.setValue(heightAvg)
                }

                if (wordGapType == AVERAGE_OBJECT_SIZE) {
                    val widthAvg = widthSum / descriptors.size.toFloat()
                    wordGapType.setValue(widthAvg)
                }

                val mergedRowsDividers = mergeRowsDividers(rowsDividers, rowGapType)
                val shortenRowsDividers = shortenDividers(mergedRowsDividers)

                var rows: Mat
                // Drawing rows boundaries.
                if (log) {
                    rows = blackWhiteImage.clone()

                    for (rowDivider in shortenRowsDividers) {
                        line(
                                rows,
                                Point(1, rowDivider),
                                Point(blackWhiteImage.cols(), rowDivider),
                                Scalar(255), 1, 8, 0)
                    }

                    conditionalSave(true, image, rows,
                            if (horizontally)
                                HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
                            else
                                VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME)
                    rows.release()
                }

                task.doProgress(R.string.progress_detecting_words)

                rows = blackWhiteImage.clone()
                var bgrImageWithWordsBoundaries: Mat? = null
                if (showFoundGapsBetweenWords) {
                    if (firstRun) {
                        bgrImageWithWordsBoundaries =
                                if (isCacheAvailable) {
                                    CACHE.getBgrImage()!!.clone()
                                } else {
                                    imread("$basePath$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                                            CV_LOAD_IMAGE_UNCHANGED)
                                }
                        if (!horizontally) {
                            bgrImageWithWordsBoundaries = rotateBy90Multiple(bgrImageWithWordsBoundaries, 1)
                        }
                    } else {
                        // The image was already cached in first run.
                        bgrImageWithWordsBoundaries = CACHE.getBgrImageWithWordsBoundaries()!!.clone()
                    }
                }

                // This stores pairs of word with its objects.
                val wordsWithObjects = LinkedHashMap<Descriptor, List<Descriptor>>()
                // This stores pairs of image's segment (row or column) and number of words in this segment.
                val segmentWithNumberOfWords = LinkedHashMap<Descriptor, Int>()

                for (i in 0 until shortenRowsDividers.size - 1) {
                    val currentRowDivider = shortenRowsDividers[i]
                    // When this row contains objects, process it.
                    if (!mergedRowsDividers.contains(currentRowDivider + 1)) {
                        if (task.isCancelled()) return null

                        val nextRowDivider = shortenRowsDividers[i + 1]

                        val height = nextRowDivider - currentRowDivider

                        val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)

                        // Creating vertical histogram of row.
                        val numberOfObjectPixelsOnCols = FloatArray(row.cols())
                        val rowData = ByteArray(row.rows() * row.cols() * row.channels())
                        row.get(0, 0, rowData)
                        for (j in rowData.indices) {
                            if (rowData[j] == 255.toByte()) {
                                numberOfObjectPixelsOnCols[j % row.cols()]++
                            }
                        }

                        val colsDividers = getColumnsDividers(numberOfObjectPixelsOnCols)
                        if (colsDividers != null) {
                            val mergedColsDividers = mergeColumnsDividers(colsDividers, wordGapType, row.cols())
                            val shortenColsDividers = shortenDividers(mergedColsDividers)

                            if (shortenColsDividers.isNotEmpty()) {
                                // Drawing words' boundaries only in the first run. It would need
                                // more complicated handling to support also drawing of second run...
                                if (showFoundGapsBetweenWords && firstRun) {
                                    for (colDivider in mergedColsDividers) {
                                        line(
                                                bgrImageWithWordsBoundaries!!,
                                                Point(colDivider, currentRowDivider + 1),
                                                Point(colDivider, nextRowDivider),
                                                Scalar(54, 67, 244), 1, 8, 0)
                                    }
                                }

                                if (log) {
                                    for (colDivider in shortenColsDividers) {
                                        line(
                                                rows,
                                                Point(colDivider, currentRowDivider + 1),
                                                Point(colDivider, nextRowDivider),
                                                Scalar(255), 1, 8, 0)
                                    }
                                }

                                val segment = mutableListOf<Point>()
                                var numberOfWordsInSegment = 0

                                // Extracting of words on current row (with a little variance).
                                for (k in 0 until shortenColsDividers.size - 1) {
                                    val currentColDivider = shortenColsDividers[k]

                                    if (!mergedColsDividers.contains(currentColDivider + 1)) {
                                        val nextColDivider = shortenColsDividers[k + 1]

                                        val width = nextColDivider - currentColDivider

                                        val wordObjects = mutableListOf<Descriptor>()
                                        val word = mutableListOf<Point>()

                                        // Collection of descriptors to be removed in this iteration to
                                        // speed up processing.
                                        val toRemove = mutableListOf<Descriptor>()

                                        // Because of using threshold when detecting rows, some rows with
                                        // objects can be considered as gaps. These objects can cause problems
                                        // here, so I must be really careful when defining rectangle of word.
                                        var wordRectY = currentRowDivider + 1
                                        var wordRectHeight = height
                                        val maxExtraHeight = (height * 0.5).toInt()
                                        if (i - 1 >= 0) {
                                            val previousRowDivider = shortenRowsDividers[i - 1]
                                            var topExtraHeight = ((currentRowDivider - previousRowDivider) * 0.5).toInt()
                                            if (topExtraHeight > maxExtraHeight)
                                                topExtraHeight = maxExtraHeight
                                            wordRectY -= topExtraHeight
                                            wordRectHeight += topExtraHeight
                                        }
                                        if (i + 2 < shortenRowsDividers.size) {
                                            val nextNextRowDivider = shortenRowsDividers[i + 2]
                                            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * 0.5).toInt()
                                            if (bottomExtraHeight > maxExtraHeight)
                                                bottomExtraHeight = maxExtraHeight
                                            wordRectHeight += bottomExtraHeight
                                        }

                                        // Assumed rectangle of word dependent on extracted dividers.
                                        val wordRect = Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)

                                        for (d in descriptors) {
                                            // Checking if center point of descriptor lies in word rectangle.
                                            if (wordRect.contains(d.center)) {
                                                wordObjects.add(d)
                                                word.addAll(d.`object`.toList())
                                                toRemove.add(d)
                                            }
                                        }
                                        descriptors.removeAll(toRemove)
                                        toRemove.clear()

                                        // Adding word with its objects to its result.
                                        if (wordObjects.size > 0 && word.size > 0) {
                                            segment.addAll(word)
                                            numberOfWordsInSegment++

                                            val wordMatOfPoint = MatOfPoint()
                                            wordMatOfPoint.fromList(word)
                                            wordsWithObjects[Descriptor(wordMatOfPoint)] = wordObjects
                                        }
                                    }
                                }

                                // Adding segment with its number of words to its result.
                                if (segment.size > 0) {
                                    val segmentMatOfPoint = MatOfPoint()
                                    segmentMatOfPoint.fromList(segment)
                                    segmentWithNumberOfWords[Descriptor(segmentMatOfPoint)] = numberOfWordsInSegment
                                }
                            }
                        }
                    }
                }
                if (showFoundGapsBetweenWords && firstRun) {
                    // During second run will nothing change so cache image only in the first run.
                    CACHE.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries!!)
                    conditionalSave(log, image, bgrImageWithWordsBoundaries,
                            WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME)
                }
                // This is only saved during first call!
                conditionalSave(log, image, rows,
                        if (horizontally)
                            HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
                        else
                            VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME)

                // Coloring of words.
                conditionalColorAndSave(log, image, blackWhiteImage.size(),
                        wordsWithObjects.values.toList(),
                        if (horizontally)
                            HORIZONTAL_WORDS_IMAGE_NAME
                        else
                            VERTICAL_WORDS_IMAGE_NAME)

                if (descriptors.isNotEmpty()) {
                    // Ok, there are objects which were probably considered as gaps, then call the whole
                    // algorithm again recursively and disable another recursion.
                    if (allowRecursion) {
                        Timber.w("${descriptors.size} objects have not been assigned to any word. Processing unassigned objects once again.")

                        // Prepare new image with black background and white foreground which contains
                        // only unassigned objects.
                        val recursionBlackWhiteImage = zeros(blackWhiteImage.size(), blackWhiteImage.type())
                        val dataReadable = ByteArray(recursionBlackWhiteImage.rows() *
                                recursionBlackWhiteImage.cols() * recursionBlackWhiteImage.channels())
                        recursionBlackWhiteImage.get(0, 0, dataReadable)
                        for (d in descriptors) {
                            for (p in d.`object`.toList()) {
                                // Descriptors starts at zero indexes! See more in object detection.
                                val index = p.x.toInt() % recursionBlackWhiteImage.cols() + p.y.toInt() * recursionBlackWhiteImage.cols()
                                dataReadable[index] = 255.toByte()
                            }
                        }
                        recursionBlackWhiteImage.put(0, 0, dataReadable)
                        // Update cache for recursive call.
                        CACHE.cacheBinaryBlackWhiteImage(recursionBlackWhiteImage)
                        CACHE.cacheDescriptors(descriptors)

                        // Add newly found words.
                        val recursionResult = findWords(task, true, rowGapType, wordGapType, image, horizontally, false, false, false)
                        if (recursionResult != null) {
                            wordsWithObjects.putAll(recursionResult.first)
                            // Update descriptors.
                            for (values in recursionResult.first.values)
                                descriptors.removeAll(values)
                            // Update segments.
                            segmentWithNumberOfWords.putAll(recursionResult.third)

                            if (descriptors.size > 0)
                                Timber.w("${descriptors.size} objects have not been assigned to any word during recursive call.")
                            else
                                Timber.i("All objects have been successfully used while detecting words.")

                            // Coloring of words after recursive call.
                            conditionalColorAndSave(log, image, recursionBlackWhiteImage.size(),
                                    wordsWithObjects.values.toList(),
                                    if (horizontally)
                                        RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
                                    else
                                        RECURSIVE_VERTICAL_WORDS_IMAGE_NAME)
                        }
                    }
                }

                return Triplet.create(wordsWithObjects, descriptors, segmentWithNumberOfWords)
            }
        } else {
            Timber.e("No objects have been found during the first phase of words detection or task was cancelled.")
        }

        return null
    }
}