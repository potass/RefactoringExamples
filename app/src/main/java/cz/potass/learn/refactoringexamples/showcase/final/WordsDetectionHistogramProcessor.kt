package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.showcase.final.WordsDetectionDrawer.Companion.WHITE_AS_BYTE
import cz.potass.learn.refactoringexamples.showcase.shared.Mat

internal class WordsDetectionHistogramProcessor {
    fun createHorizontalHistogram(image: Mat) = createHistogram(image, true)

    fun createVerticalHistogram(image: Mat) = createHistogram(image, false)

    private fun createHistogram(image: Mat, isHorizontal: Boolean): FloatArray {
        image.run {
            val numberOfObjectPixels = FloatArray(if (isHorizontal) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE_AS_BYTE) {
                    if (isHorizontal) numberOfObjectPixels[i / cols()]++
                    else numberOfObjectPixels[i % cols()]++
                }
            }
            return numberOfObjectPixels
        }
    }
}