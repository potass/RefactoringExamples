package cz.potass.learn.refactoringexamples.showcase.step08

import cz.potass.learn.refactoringexamples.showcase.shared.Rect

class WordBoundariesDetector(private val params: Params) {
    private companion object {
        const val BOUNDARY_TOLERANCE = 0.5
    }

    private var wordRectY = params.currentRowDivider + 1
    private var wordRectHeight = params.rowHeight

    fun detect(): Rect {
        with(params) {
            addExtraTopSpaceIfPossible()
            addExtraBottomSpaceIfPossible()
            return Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)
        }
    }

    private fun Params.addExtraTopSpaceIfPossible() {
        if (isNotFirstRowDivider()) {
            val previousRowDivider = shortenRowsDividers[currentRowDividerIndex - 1]
            var topExtraHeight = ((currentRowDivider - previousRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (topExtraHeight > maxExtraHeight) topExtraHeight = maxExtraHeight
            wordRectY -= topExtraHeight
            wordRectHeight += topExtraHeight
        }
    }

    private fun Params.addExtraBottomSpaceIfPossible() {
        if (isNotPenultimateRowDivider()) {
            val nextNextRowDivider = shortenRowsDividers[currentRowDividerIndex + 2]
            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (bottomExtraHeight > maxExtraHeight) bottomExtraHeight = maxExtraHeight
            wordRectHeight += bottomExtraHeight
        }
    }

    data class Params(
            val shortenRowsDividers: List<Int>,
            val currentRowDividerIndex: Int,
            val currentColDivider: Int,
            val nextColDivider: Int
    ) {
        val currentRowDivider = shortenRowsDividers[currentRowDividerIndex]
        val nextRowDivider = shortenRowsDividers[currentRowDividerIndex + 1]
        val rowHeight = nextRowDivider - currentRowDivider
        val maxExtraHeight = (rowHeight * BOUNDARY_TOLERANCE).toInt()
        val width = nextColDivider - currentColDivider

        fun isNotFirstRowDivider() = currentRowDividerIndex > 0

        fun isNotPenultimateRowDivider() = currentRowDividerIndex + 2 in shortenRowsDividers.indices
    }
}