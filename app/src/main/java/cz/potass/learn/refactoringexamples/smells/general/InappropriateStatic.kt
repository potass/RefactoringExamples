package cz.potass.learn.refactoringexamples.smells.general

internal class InappropriateStatic {
    object OvertimeHourlyPayCalculator {
        fun calculatePay(employee: Employee): Double {
            return employee.getHoursToPay() * 42.0 * 2
        }
    }

    object StraightTimeHourlyPayCalculator {
        fun calculatePay(employee: Employee): Double {
            return employee.getHoursToPay() * 42.0
        }
    }

    interface Employee {
        fun getHoursToPay(): Int
    }

    interface RefactoredEmployee {
        fun calculateOvertimePay(): Double
        fun calculateStraightTimePay(): Double
    }
}