package cz.potass.learn.refactoringexamples.showcase.step10

import cz.potass.learn.refactoringexamples.showcase.shared.GapType
import cz.potass.learn.refactoringexamples.showcase.shared.GapType.AVERAGE_OBJECT_SIZE
import cz.potass.learn.refactoringexamples.showcase.shared.Image
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode

internal data class WordsDetectionParams(
        private val listener: WordsDetectionProgressListener,
        val rowGapType: GapType,
        val wordGapType: GapType,
        val image: Image,
        val mode: WordsDetectionMode,
        val isCancelled: () -> Boolean
) {
    val basePath = image.getBasePath()

    fun isVertical() = mode.isVertical()
    fun isHorizontal() = mode.isHorizontal()

    fun onStart() = listener.onStart()
    fun onWordsComponentsDetection() = listener.onWordsComponentsDetection()
    fun onWordsDetection() = listener.onWordsDetection()

    fun updateRowGapTypeAverageIfPossible(average: () -> Float) {
        if (isRowAverageObjectSizeGapType()) rowGapType.setValue(average())
    }

    private fun isRowAverageObjectSizeGapType() = rowGapType == AVERAGE_OBJECT_SIZE

    fun updateWordGapTypeAverageIfPossible(average: () -> Float) {
        if (isWordAverageObjectSizeGapType()) wordGapType.setValue(average())
    }

    private fun isWordAverageObjectSizeGapType() = wordGapType == AVERAGE_OBJECT_SIZE
}