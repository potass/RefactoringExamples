package cz.potass.learn.refactoringexamples.smells.general

internal class Duplication {
    // IDENTICAL CODE EXAMPLE

    class A(private val string: String, private val int: Int) {
        fun print() {
            val stringForPrint =
                    "\$# ${string.trimEnd().run { substring(Math.min(length - 1, 5)..(length - 1)) }}#\$"
            val intForPrint = "int=$int"
            println("$stringForPrint-$intForPrint")
        }
    }

    class B(private val string: String, private val float: Float) {
        fun format(): String {
            val formattedString =
                    "\$# ${string.trimEnd().run { substring(Math.min(length - 1, 5)..(length - 1)) }}#\$"
            val formattedFloat = float.rem(42)
            return "$formattedString\n$formattedFloat"
        }
    }

    inner class RefactoredA(private val string: String, private val int: Int) {
        fun print() {
            val stringForPrint = string.format()
            val intForPrint = "int=$int"
            println("$stringForPrint-$intForPrint")
        }
    }

    inner class RefactoredB(private val string: String, private val float: Float) {
        fun format(): String {
            val formattedString = string.format()
            val formattedFloat = float.rem(42)
            return "$formattedString\n$formattedFloat"
        }
    }

    fun String.format() = "\$# ${trimEnd().run { substring(Math.min(length - 1, 5)..(length - 1)) }}#\$"

    // SIMILAR CODE EXAMPLE

    class C(private val string: String, private val int: Int) {
        fun print() {
            val stringForPrint =
                    "\$# ${string.trimEnd().run { substring(Math.min(length - 1, 5)..(length - 1)) }}#\$"
            val intForPrint = "int=$int"
            println("$stringForPrint-$intForPrint")
        }
    }

    class D(private val string: String, private val float: Float) {
        fun format(): String {
            val formattedString =
                    "#${string.trimEnd().run { substring(Math.min(length - 1, 4)..(length - 1)) }}**"
            val formattedFloat = float.rem(42)
            return "$formattedString\n$formattedFloat"
        }
    }

    inner class RefactoredC(private val string: String, private val int: Int) {
        fun print() {
            val stringForPrint = string.format("\$# ", 5, "#\$")
            val intForPrint = "int=$int"
            println("$stringForPrint-$intForPrint")
        }
    }

    inner class RefactoredD(private val string: String, private val float: Float) {
        fun format(): String {
            val formattedString = string.format("#", 4, "**")
            val formattedFloat = float.rem(42)
            return "$formattedString\n$formattedFloat"
        }
    }

    fun String.format(prefix: String, lowerBoundIndex: Int, suffix: String) =
            "$prefix${trimEnd().run { substring(Math.min(length - 1, lowerBoundIndex)..(length - 1)) }}$suffix"

    // SWITCH (IF/ELSE) EXAMPLE

    class E {
        fun showDetail(phoneNumber: String) {
            when (phoneNumber) {
                "+44 01 2345 6789" -> { /*show international number detail*/ }
                "+420 777 888 999" -> { /*show mobile number detail*/ }
                "+420 555 444 333" -> { /*show landline detail*/ }
                "112" -> { /*show emergency number detail*/ }
            }
        }
    }

    class F {
        fun call(phoneNumber: String) {
            when (phoneNumber) {
                "+44 01 2345 6789" -> { /*call international number*/ }
                "+420 777 888 999" -> { /*call mobile number*/ }
                "+420 555 444 333" -> { /*call landline*/ }
                "112" -> { /*call emergency number*/ }
            }
        }
    }

    class RefactoredE {
        fun showDetail(phoneNumber: PhoneNumber) {
            phoneNumber.showDetail()
        }
    }

    class RefactoredF {
        fun call(phoneNumber: PhoneNumber) {
            phoneNumber.call()
        }
    }

    interface PhoneNumber {
        fun showDetail()
        fun call()
    }

    class InternationalNumber : PhoneNumber {
        override fun showDetail() {
            /*show international number detail*/
        }

        override fun call() {
            /*call international number*/
        }
    }

    class MobileNumber : PhoneNumber {
        override fun showDetail() {
            /*show mobile number detail*/
        }

        override fun call() {
            /*call mobile number*/
        }
    }

    class Landline : PhoneNumber {
        override fun showDetail() {
            /*show landline detail*/
        }

        override fun call() {
            /*call landline number*/
        }
    }

    class EmergencyNumber : PhoneNumber {
        override fun showDetail() {
            /*show emergency number detail*/
        }

        override fun call() {
            /*call emergency number*/
        }
    }
}