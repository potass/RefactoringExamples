package cz.potass.learn.refactoringexamples.smells.general

import java.io.FileOutputStream
import java.io.IOException

internal class DeadCode {
    class IfExample(private val condition: Boolean) {
        fun process() {
            if (condition) {
                // Reachable code.
            } else if (!condition) {
                // Reachable code.
            } else if (condition == null) {
                // Unreachable code.
            } else {
                // Unreachable code.
            }
        }

        fun refactoredProcess() {
            if (condition) {
                // Reachable code.
            } else {
                // Reachable code.
            }
        }
    }

    class CatchExample {
        fun process() {
            try {
                FileOutputStream("file").writer().append('#')
            } catch (e: ArrayIndexOutOfBoundsException) {
                // Unreachable code.
            } catch (e: IOException) {
                // Reachable code.
            }
        }

        fun refactoredProcess() {
            try {
                FileOutputStream("file").writer().append('#')
            } catch (e: IOException) {
                // Reachable code.
            }
        }
    }
}