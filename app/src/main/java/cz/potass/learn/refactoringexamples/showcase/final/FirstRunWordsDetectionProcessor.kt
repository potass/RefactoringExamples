package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG

internal class FirstRunWordsDetectionProcessor(params: WordsDetectionParams)
    : WordsDetectionProcessor(params) {
    override fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords()

    override fun shouldCreateDebugInfo() = DEBUG
}