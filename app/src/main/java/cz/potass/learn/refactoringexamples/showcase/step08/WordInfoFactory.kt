package cz.potass.learn.refactoringexamples.showcase.step08

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Point
import cz.potass.learn.refactoringexamples.showcase.shared.Rect

/** No more used from step 11. */
class WordInfoFactory(private val params: Params) {
    private lateinit var wordRect: Rect
    private val wordObjects = mutableListOf<Descriptor>()
    private val word = mutableListOf<Point>()

    fun create(): WordInfo {
        with(params) {
            detectWordRect()
            retrieveWordContent()
            return WordInfo(wordObjects, word)
        }
    }

    private fun Params.detectWordRect() {
        wordRect = WordBoundariesDetector(WordBoundariesDetector.Params(
                shortenRowsDividers,
                currentRowDividerIndex,
                currentColDivider,
                nextColDivider
        )).detect()
    }

    private fun Params.retrieveWordContent() {
        descriptors.forEach { d ->
            if (isDescriptorCenterInsideRectangle(wordRect, d)) {
                wordObjects.add(d)
                word.addAll(d.matrix.toList())
            }
        }
    }

    private fun isDescriptorCenterInsideRectangle(rect: Rect, descriptor: Descriptor) =
            rect.contains(descriptor.center)

    data class Params(
            val descriptors: List<Descriptor>,
            val shortenRowsDividers: List<Int>,
            val currentRowDividerIndex: Int,
            val currentColDivider: Int,
            val nextColDivider: Int
    )
}