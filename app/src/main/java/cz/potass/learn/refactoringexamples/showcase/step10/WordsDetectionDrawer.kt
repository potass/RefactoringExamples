package cz.potass.learn.refactoringexamples.showcase.step10

import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.Point
import cz.potass.learn.refactoringexamples.showcase.shared.Scalar

internal class WordsDetectionDrawer(private val storage: WordsDetectionStorage) {
    companion object {
        private const val WHITE = 255
        const val WHITE_AS_BYTE = WHITE.toByte()
        private val BGR_WHITE = Scalar(WHITE)
        private val BGR_RED = Scalar(54, 67, 244)
    }

    fun drawRowsBoundaries(blackWhiteImage: Mat, shortenRowsDividers: List<Int>) {
        val rows = blackWhiteImage.clone()
        drawHorizontalBoundaries(shortenRowsDividers, rows, blackWhiteImage.cols())
        storage.saveRowsBoundariesImage(rows)
        rows.release()
    }

    private fun drawHorizontalBoundaries(dividers: List<Int>, rows: Mat, numberOfCols: Int) {
        dividers.forEach {
            drawLine(
                    rows,
                    Point(1, it),
                    Point(numberOfCols, it),
                    Scalar(WHITE)
            )
        }
    }

    fun drawWordsBoundaries(params: Params) {
        drawVerticalBoundaries(params) { arg1, arg2, arg3, arg4 ->
            drawWordsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    fun drawColumnsBoundaries(params: Params) {
        drawVerticalBoundaries(params) { arg1, arg2, arg3, arg4 ->
            drawColumnsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    private fun drawVerticalBoundaries(
            params: Params, drawAction: (Int, Int, List<Int>, Mat) -> Unit
    ) {
        with(params) {
            drawAction(
                    shortenRowsDividers[currentRowDividerIndex],
                    shortenRowsDividers[currentRowDividerIndex + 1],
                    dividers,
                    image
            )
        }
    }

    private fun drawWordsBoundaries(
            currentRowDivider: Int, nextRowDivider: Int, mergedColsDividers: List<Int>, image: Mat
    ) {
        drawVerticalBoundaries(currentRowDivider, nextRowDivider, mergedColsDividers, image,
                BGR_RED)
    }

    private fun drawColumnsBoundaries(
            currentRowDivider: Int, nextRowDivider: Int, shortenColsDividers: List<Int>, image: Mat
    ) {
        drawVerticalBoundaries(currentRowDivider, nextRowDivider, shortenColsDividers, image,
                BGR_WHITE)
    }

    private fun drawVerticalBoundaries(
            currentHorizontalDivider: Int,
            nextHorizontalDivider: Int,
            dividers: List<Int>,
            image: Mat,
            color: Scalar
    ) {
        dividers.forEach {
            drawLine(
                    image,
                    Point(it, currentHorizontalDivider + 1),
                    Point(it, nextHorizontalDivider),
                    color
            )
        }
    }

    private fun drawLine(image: Mat, start: Point, end: Point, color: Scalar) =
            line(image, start, end, color, 1, 8, 0)

    data class Params(
            val shortenRowsDividers: List<Int>,
            val image: Mat,
            val currentRowDividerIndex: Int,
            val dividers: List<Int>
    )
}