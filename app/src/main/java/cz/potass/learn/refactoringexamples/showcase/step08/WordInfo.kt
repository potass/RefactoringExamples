package cz.potass.learn.refactoringexamples.showcase.step08

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.MatOfPoint
import cz.potass.learn.refactoringexamples.showcase.shared.Point

data class WordInfo(
        val wordObjects: MutableList<Descriptor>,
        val word: MutableList<Point>
) {
    fun isWordPresent() = wordObjects.isNotEmpty() && word.isNotEmpty()

    fun createWordMatOfPoint() = MatOfPoint().apply { fromList(word) }
}