package cz.potass.learn.refactoringexamples.showcase.step05

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.GapType.AVERAGE_OBJECT_SIZE
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener
import cz.potass.learn.refactoringexamples.showcase.step05.WordsDetectionProcessor.Mode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.step05.WordsDetectionProcessor.Mode.VERTICAL
import cz.potass.learn.refactoringexamples.showcase.step05.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Rename java params to avoid kotlin `` encapsulation.
 *  1. Extract and refactor method to extract words on row and to call recursion.
 *  1. Some minor refactoring such as isNotEmpty() or !in usage, null check removal and for to forEach.
 *  1. Removal of some comments (redundant or by condition/method extraction).
 */
internal class WordsDetectionProcessor(private val params: Params) {
    private companion object {
        const val PATH_SEPARATOR = "/"
        const val WHITE = 255
        val BGR_RED = Scalar(54, 67, 244)
        const val BOUNDARY_TOLERANCE = 0.5
    }

    private val cache = Cache.get()
    private val settings = Settings.get()
    private var isFirstRun = true

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    fun detect(): Result {
        params.onStart()

        val blackWhiteImage = loadBlackWhiteImage()
        val descriptors = loadDescriptors()

        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            val numberOfObjectPixelsOnRows = createHorizontalHistogram(blackWhiteImage)
            val rowsDividers = getRowsDividers(numberOfObjectPixelsOnRows)

            rowsDividers?.run {
                updateGapTypeAveragesIfNecessary(descriptors)

                val mergedRowsDividers = mergeRowsDividers(this, params.rowGapType)
                val shortenRowsDividers = shortenDividers(mergedRowsDividers)

                drawRowsBoundariesIfNecessary(blackWhiteImage, shortenRowsDividers)

                params.onWordsDetection()

                val rows = blackWhiteImage.clone()
                val bgrImageWithWordsBoundaries =
                        if (settings.shouldShowGapsBetweenWords()) loadBgrImageWithWordsBoundaries()
                        else null

                val wordsWithObjects = LinkedHashMap<Descriptor, List<Descriptor>>()
                val segmentsWithNumberOfWords = LinkedHashMap<Descriptor, Int>()

                for (i in 0 until shortenRowsDividers.size - 1) {
                    val currentRowDivider = shortenRowsDividers[i]
                    if (doesRowContainObjects(currentRowDivider, mergedRowsDividers)) {
                        if (params.isCancelled()) return Cancelled

                        val nextRowDivider = shortenRowsDividers[i + 1]
                        val height = nextRowDivider - currentRowDivider
                        val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                        val numberOfObjectPixelsOnCols = createVerticalHistogram(row)
                        val colsDividers = getColumnsDividers(numberOfObjectPixelsOnCols)

                        colsDividers?.run {
                            val mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                            val shortenColsDividers = shortenDividers(mergedColsDividers)

                            if (shortenColsDividers.isNotEmpty()) {
                                drawWordsBoundariesIfNecessary(
                                        mergedColsDividers,
                                        bgrImageWithWordsBoundaries!!,
                                        currentRowDivider,
                                        nextRowDivider
                                )

                                drawColumnsBoundariesIfNecessary(shortenColsDividers, rows,
                                        currentRowDivider, nextRowDivider)

                                val segment = mutableListOf<Point>()
                                // TODO improve sent params and result (do not modify input params?)
                                val numberOfWordsInSegment = extractWordsOnRow(
                                        shortenColsDividers,
                                        mergedColsDividers,
                                        shortenRowsDividers,
                                        currentRowDivider,
                                        i,
                                        nextRowDivider,
                                        height,
                                        descriptors,
                                        segment,
                                        wordsWithObjects
                                )

                                addSegmentToResultIfPossible(segment, segmentsWithNumberOfWords, numberOfWordsInSegment)
                            }
                        }
                    }
                }

                persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries!!)
                persistImageWithWordsBoundaries(rows)
                persistImageWithColoredWords(blackWhiteImage, wordsWithObjects)

                tryToDetectWordsInGapsIfPossible(descriptors, blackWhiteImage, wordsWithObjects, segmentsWithNumberOfWords)

                return Success(wordsWithObjects, descriptors, segmentsWithNumberOfWords)
            } ?: Timber.d("No row dividers detected.")
        }

        return NoObjectsFound
    }

    // TODO extract methods to separate classes which possible regarding SRP!!!

    private fun loadBlackWhiteImage(): Mat {
        var result: Mat
        if (isFirstRun && cache.getBinaryBlackWhiteImage() == null) {
            result = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
            cache.cacheBinaryBlackWhiteImage(result)
        } else {
            result = cache.getBinaryBlackWhiteImage()!!
        }
        return result
    }

    private fun loadDescriptors(): MutableList<Descriptor> {
        return if (isFirstRun && cache.getDescriptors() == null) {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        } else {
            cache.getDescriptors()!!
        }
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        var result: Mat
        if (isFirstRun) {
            result = cache.getBgrImage()
                    ?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)

            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
        } else {
            result = cache.getBgrImageWithWordsBoundaries()!!.clone()
        }
        return result
    }

    private fun createHorizontalHistogram(image: Mat) = createHistogram(image, HORIZONTAL)

    private fun createVerticalHistogram(image: Mat) = createHistogram(image, VERTICAL)

    private fun createHistogram(image: Mat, mode: Mode): FloatArray {
        return image.run {
            val numberOfObjectPixels = FloatArray(if (mode == HORIZONTAL) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE.toByte()) {
                    if (mode == HORIZONTAL) {
                        numberOfObjectPixels[i / cols()]++
                    } else {
                        numberOfObjectPixels[i % cols()]++
                    }
                }
            }
            numberOfObjectPixels
        }
    }

    private fun updateGapTypeAveragesIfNecessary(descriptors: List<Descriptor>) {
        updateRowGapTypeAverageIfNecessary(descriptors)
        updateWordGapTypeAverageIfNecessary(descriptors)
    }

    private fun updateRowGapTypeAverageIfNecessary(descriptors: List<Descriptor>) {
        if (params.isRowAverageObjectSizeGapType()) {
            val heightAverage = descriptors.map { it.straightRectangleHeight }.average()
            params.setRowGapTypeValue(heightAverage.toFloat())
        }
    }

    private fun updateWordGapTypeAverageIfNecessary(descriptors: List<Descriptor>) {
        if (params.isWordAverageObjectSizeGapType()) {
            val widthAverage = descriptors.map { it.straightRectangleWidth }.average()
            params.setWordGapTypeValue(widthAverage.toFloat())
        }
    }

    private fun drawRowsBoundariesIfNecessary(image: Mat, shortenRowsDividers: List<Int>) {
        if (shouldCreateDebugInfo()) {
            val rows = image.clone()

            shortenRowsDividers.forEach {
                drawLine(
                        rows,
                        Point(1, it),
                        Point(image.cols(), it),
                        Scalar(WHITE)
                )
            }

            conditionalSave(
                    true, params.image, rows,
                    if (params.isHorizontal()) HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
                    else VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
            )

            rows.release()
        }
    }

    private fun drawWordsBoundariesIfNecessary(
            mergedColsDividers: List<Int>,
            image: Mat,
            currentRowDivider: Int,
            nextRowDivider: Int
    ) {
        if (shouldDrawWordsBoundaries()) {
            mergedColsDividers.forEach {
                drawLine(
                        image,
                        Point(it, currentRowDivider + 1),
                        Point(it, nextRowDivider),
                        BGR_RED
                )
            }
        }
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(
            shortenColsDividers: List<Int>,
            image: Mat,
            currentRowDivider: Int,
            nextRowDivider: Int
    ) {
        if (shouldCreateDebugInfo()) {
            shortenColsDividers.forEach {
                drawLine(
                        image,
                        Point(it, currentRowDivider + 1),
                        Point(it, nextRowDivider),
                        Scalar(WHITE)
                )
            }
        }
    }

    private fun drawLine(image: Mat, start: Point, end: Point, color: Scalar) {
        line(image, start, end, color, 1, 8, 0)
    }

    private fun doesRowContainObjects(currentRowDivider: Int, mergedRowsDividers: List<Int>) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    // TODO needs more refactoring!
    private fun extractWordsOnRow(
            shortenColsDividers: List<Int>,
            mergedColsDividers: List<Int>,
            shortenRowsDividers: List<Int>,
            currentRowDivider: Int,
            currentRowDividerIndex: Int,
            nextRowDivider: Int,
            rowHeight: Int,
            descriptors: MutableList<Descriptor>,
            segment: MutableList<Point>,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>
    ): Int {
        var numberOfWordsInSegment = 0
        for (i in 0 until shortenColsDividers.size - 1) {
            val currentColDivider = shortenColsDividers[i]

            if (doesColumnContainObjects(currentColDivider, mergedColsDividers)) {
                val wordRect = determineWordBoundaries(
                        currentRowDivider,
                        currentRowDividerIndex,
                        shortenRowsDividers,
                        nextRowDivider,
                        currentColDivider,
                        shortenColsDividers[i + 1],
                        rowHeight
                )

                val wordObjects = mutableListOf<Descriptor>()
                val word = mutableListOf<Point>()

                val processedDescriptors = mutableListOf<Descriptor>()
                descriptors.forEach { d ->
                    if (isDescriptorCenterInsideRectangle(wordRect, d)) {
                        wordObjects.add(d)
                        word.addAll(d.matrix.toList())
                        processedDescriptors.add(d)
                    }
                }
                descriptors.removeAll(processedDescriptors)

                if (addWordToResultIfPossible(wordObjects, word, segment, wordsWithObjects)) numberOfWordsInSegment++
            }
        }
        return numberOfWordsInSegment
    }

    private fun doesColumnContainObjects(currentColDivider: Int, mergedColsDividers: List<Int>) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun determineWordBoundaries(
            currentRowDivider: Int,
            currentRowDividerIndex: Int,
            shortenRowsDividers: List<Int>,
            nextRowDivider: Int,
            currentColDivider: Int,
            nextColDivider: Int,
            rowHeight: Int
    ): Rect {
        var wordRectY = currentRowDivider + 1
        var wordRectHeight = rowHeight
        val maxExtraHeight = (rowHeight * BOUNDARY_TOLERANCE).toInt()
        if (currentRowDividerIndex - 1 >= 0) {
            val previousRowDivider = shortenRowsDividers[currentRowDividerIndex - 1]
            var topExtraHeight = ((currentRowDivider - previousRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (topExtraHeight > maxExtraHeight) topExtraHeight = maxExtraHeight
            wordRectY -= topExtraHeight
            wordRectHeight += topExtraHeight
        }
        if (currentRowDividerIndex + 2 in shortenRowsDividers.indices) {
            val nextNextRowDivider = shortenRowsDividers[currentRowDividerIndex + 2]
            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (bottomExtraHeight > maxExtraHeight) bottomExtraHeight = maxExtraHeight
            wordRectHeight += bottomExtraHeight
        }
        val width = nextColDivider - currentColDivider
        return Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)
    }

    private fun isDescriptorCenterInsideRectangle(rect: Rect, descriptor: Descriptor) =
            rect.contains(descriptor.center)

    private fun addWordToResultIfPossible(
            wordObjects: MutableList<Descriptor>,
            word: MutableList<Point>,
            segment: MutableList<Point>,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>
    ): Boolean {
        if (wordObjects.isNotEmpty() && word.isNotEmpty()) {
            segment.addAll(word)
            val wordMatOfPoint = MatOfPoint()
            wordMatOfPoint.fromList(word)
            wordsWithObjects[Descriptor(wordMatOfPoint)] = wordObjects
            return true
        }
        return false
    }

    private fun addSegmentToResultIfPossible(
            segment: MutableList<Point>,
            segmentsWithNumberOfWords: LinkedHashMap<Descriptor, Int>,
            numberOfWordsInSegment: Int
    ) {
        if (segment.isNotEmpty()) {
            val segmentMatOfPoint = MatOfPoint()
            segmentMatOfPoint.fromList(segment)
            segmentsWithNumberOfWords[Descriptor(segmentMatOfPoint)] = numberOfWordsInSegment
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            conditionalSave(shouldCreateDebugInfo(), params.image, bgrImageWithWordsBoundaries,
                    WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords() && isFirstRun

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        conditionalSave(shouldCreateDebugInfo(), params.image, rows,
                if (params.isHorizontal()) HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
                else VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME)
    }

    private fun persistImageWithColoredWords(
            blackWhiteImage: Mat,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>
    ) {
        persistImageWithColoredWords(blackWhiteImage, wordsWithObjects,
                if (params.isHorizontal()) HORIZONTAL_WORDS_IMAGE_NAME
                else VERTICAL_WORDS_IMAGE_NAME)
    }

    // TODO needs more refactoring!
    private fun tryToDetectWordsInGapsIfPossible(
            descriptors: MutableList<Descriptor>,
            blackWhiteImage: Mat,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>,
            segmentsWithNumberOfWords: LinkedHashMap<Descriptor, Int>
    ) {
        if (descriptors.isNotEmpty() && isRecursionAllowed()) {
            Timber.w("${descriptors.size} objects have not been assigned to any word. Processing unassigned objects once again.")

            forbidRecursion()

            val imageWithUnassignedObjects = prepareImageWithUnassignedObjects(blackWhiteImage, descriptors)

            val recursionResult = detect()
            if (recursionResult is Success) {
                updateOriginalResult(recursionResult, wordsWithObjects, descriptors, segmentsWithNumberOfWords)

                if (descriptors.isNotEmpty())
                    Timber.w("${descriptors.size} objects have not been assigned to any word during recursive call.")
                else
                    Timber.i("All objects have been successfully used while detecting words.")

                persistRecursiveImageWithColoredWords(imageWithUnassignedObjects, wordsWithObjects)
            }

            allowRecursion()
        }
    }

    private fun prepareImageWithUnassignedObjects(blackWhiteImage: Mat, descriptors: MutableList<Descriptor>): Mat {
        val blackWhiteImageWithUnassignedObjects = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val dataReadable = ByteArray(blackWhiteImageWithUnassignedObjects.rows() *
                blackWhiteImageWithUnassignedObjects.cols() *
                blackWhiteImageWithUnassignedObjects.channels())
        blackWhiteImageWithUnassignedObjects.get(0, 0, dataReadable)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % blackWhiteImageWithUnassignedObjects.cols() +
                        p.y.toInt() * blackWhiteImageWithUnassignedObjects.cols()
                dataReadable[index] = WHITE.toByte()
            }
        }
        blackWhiteImageWithUnassignedObjects.put(0, 0, dataReadable)
        cache.cacheBinaryBlackWhiteImage(blackWhiteImageWithUnassignedObjects)
        cache.cacheDescriptors(descriptors)
        return blackWhiteImageWithUnassignedObjects
    }

    private fun updateOriginalResult(
            recursionResult: Success,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>,
            descriptors: MutableList<Descriptor>,
            segmentsWithNumberOfWords: LinkedHashMap<Descriptor, Int>
    ) {
        wordsWithObjects.putAll(recursionResult.wordsWithObjects)
        recursionResult.wordsWithObjects.values.forEach { descriptors.removeAll(it) }
        segmentsWithNumberOfWords.putAll(recursionResult.segmentsWithNumberOfWords)
    }

    private fun persistRecursiveImageWithColoredWords(
            blackWhiteImage: Mat,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>
    ) {
        persistImageWithColoredWords(blackWhiteImage, wordsWithObjects,
                if (params.isHorizontal()) RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
                else RECURSIVE_VERTICAL_WORDS_IMAGE_NAME)
    }

    private fun persistImageWithColoredWords(
            blackWhiteImage: Mat,
            wordsWithObjects: LinkedHashMap<Descriptor, List<Descriptor>>,
            suffix: String
    ) {
        conditionalColorAndSave(shouldCreateDebugInfo(), params.image, blackWhiteImage.size(),
                wordsWithObjects.values.toList(), suffix)
    }

    private fun shouldCreateDebugInfo() = DEBUG && isFirstRun

    private fun isRecursionAllowed() = isFirstRun

    private fun forbidRecursion() {
        isFirstRun = false
    }

    private fun allowRecursion() {
        isFirstRun = true
    }

    data class Params(
            private val listener: WordsDetectionProgressListener,
            val rowGapType: GapType,
            val wordGapType: GapType,
            val image: Image,
            val mode: Mode,
            val isCancelled: () -> Boolean
    ) {
        val basePath = image.getBasePath()

        fun isVertical() = mode.isVertical()
        fun isHorizontal() = mode.isHorizontal()

        fun onStart() = listener.onStart()
        fun onWordsComponentsDetection() = listener.onWordsComponentsDetection()
        fun onWordsDetection() = listener.onWordsDetection()

        fun isRowAverageObjectSizeGapType() = rowGapType == AVERAGE_OBJECT_SIZE
        fun isWordAverageObjectSizeGapType() = wordGapType == AVERAGE_OBJECT_SIZE
        fun setRowGapTypeValue(value: Float) = rowGapType.setValue(value)
        fun setWordGapTypeValue(value: Float) = wordGapType.setValue(value)
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val segmentsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result()
    }

    enum class Mode {
        HORIZONTAL,
        VERTICAL;

        fun isHorizontal(): Boolean {
            return this == HORIZONTAL
        }

        fun isVertical(): Boolean {
            return this == VERTICAL
        }
    }
}