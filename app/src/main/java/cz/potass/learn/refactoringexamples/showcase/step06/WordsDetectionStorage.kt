package cz.potass.learn.refactoringexamples.showcase.step06

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME

class WordsDetectionStorage(private val params: WordsDetectionParams) {
    fun saveRowsBoundariesImage(rows: Mat) {
        val suffix =
                if (params.isHorizontal()) HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
                else VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
        save(rows, suffix)
    }

    fun saveBgrImageWithWordsBoundariesIfPossible(
            condition: Boolean, bgrImageWithWordsBoundaries: Mat
    ) {
        saveConditional(condition, bgrImageWithWordsBoundaries) {
            WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME
        }
    }

    fun saveImageWithWordsBoundariesIfPossible(condition: Boolean, imageWithWordsBoundaries: Mat) {
        saveConditional(condition, imageWithWordsBoundaries) {
            if (params.isHorizontal()) HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
            else VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
        }
    }

    private fun saveConditional(condition: Boolean, imageMat: Mat, suffix: () -> String) {
        if (condition) save(imageMat, suffix())
    }

    private fun save(imageMat: Mat, suffix: String) =
            conditionalSave(true, params.image, imageMat, suffix)

    fun saveImageWithColoredWordsIfPossible(
            condition: Boolean, size: Size, objects: List<List<Descriptor>>
    ) {
        saveColoredConditional(condition, size, objects) {
            if (params.isHorizontal()) HORIZONTAL_WORDS_IMAGE_NAME
            else VERTICAL_WORDS_IMAGE_NAME
        }
    }

    fun saveRecursiveImageWithColoredWordsIfPossible(
            condition: Boolean, size: Size, objects: List<List<Descriptor>>
    ) {
        saveColoredConditional(condition, size, objects) {
            if (params.isHorizontal()) RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
            else RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
        }
    }

    private fun saveColoredConditional(
            condition: Boolean, size: Size, objects: List<List<Descriptor>>, suffix: () -> String
    ) {
        if (condition) saveColored(size, objects, suffix())
    }

    private fun saveColored(size: Size, objects: List<List<Descriptor>>, suffix: String) {
        conditionalColorAndSave(true, params.image, size, objects, suffix)
    }
}