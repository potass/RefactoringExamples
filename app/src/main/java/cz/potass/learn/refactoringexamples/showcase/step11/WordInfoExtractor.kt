package cz.potass.learn.refactoringexamples.showcase.step11

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Point
import cz.potass.learn.refactoringexamples.showcase.shared.Rect
import cz.potass.learn.refactoringexamples.showcase.step08.WordBoundariesDetector
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo

internal class WordInfoExtractor(private val params: Params) {
    private lateinit var wordRect: Rect
    private val wordObjects = mutableListOf<Descriptor>()
    private val word = mutableListOf<Point>()

    fun extract() = params.run { createWordInfo() }

    private fun Params.createWordInfo(): WordInfo {
        detectWordRect()
        retrieveWordContent()
        return WordInfo(wordObjects, word)
    }

    private fun Params.detectWordRect() {
        wordRect = WordBoundariesDetector(WordBoundariesDetector.Params(
                shortenRowsDividers,
                currentRowDividerIndex,
                currentColDivider,
                nextColDivider
        )).detect()
    }

    private fun Params.retrieveWordContent() {
        descriptors.forEach { d ->
            if (isDescriptorCenterInsideRectangle(wordRect, d)) {
                wordObjects.add(d)
                word.addAll(d.matrix.toList())
            }
        }
    }

    private fun isDescriptorCenterInsideRectangle(rect: Rect, descriptor: Descriptor) =
            rect.contains(descriptor.center)

    data class Params(
            val descriptors: List<Descriptor>,
            val shortenRowsDividers: List<Int>,
            val shortenColsDividers: List<Int>,
            val currentShortenColsDivider: Int,
            val currentRowDividerIndex: Int
    ) {
        val currentColDivider = shortenColsDividers[currentShortenColsDivider]
        val nextColDivider = shortenColsDividers[currentShortenColsDivider + 1]
    }
}