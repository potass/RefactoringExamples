package cz.potass.learn.refactoringexamples.showcase.step07

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step06.*
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.VERTICAL
import cz.potass.learn.refactoringexamples.showcase.step07.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Extract other methods.
 */
internal class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    private companion object {
        const val PATH_SEPARATOR = "/"
        const val WHITE = WordsDetectionDrawer.WHITE.toByte()
        const val BOUNDARY_TOLERANCE = 0.5
    }

    private val cache = Cache.get()
    private val settings = Settings.get()
    private var isFirstRun = true
    private val logger = WordsDetectionLogger()
    private val storage = WordsDetectionStorage(params)
    private val drawer = WordsDetectionDrawer()

    private lateinit var blackWhiteImage: Mat
    private lateinit var descriptors: MutableList<Descriptor>
    private lateinit var mergedRowsDividers: List<Int>
    private lateinit var shortenRowsDividers: List<Int>
    private lateinit var mergedColsDividers: List<Int>
    private lateinit var shortenColsDividers: List<Int>
    private lateinit var wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
    private lateinit var segmentsWithNumberOfWords: MutableMap<Descriptor, Int>

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     * TODO create 3 classes, one for each logical procedure?
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    fun detect(): Result {
        params.onStart()

        loadBlackWhiteImage()
        loadDescriptors()

        return detectWordsIfAnyObjectIsPresent()
    }

    private fun loadBlackWhiteImage() {
        if (isFirstRun && cache.getBinaryBlackWhiteImage() == null) {
            blackWhiteImage = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) blackWhiteImage = rotateBy90Multiple(blackWhiteImage, 1)
            cache.cacheBinaryBlackWhiteImage(blackWhiteImage)
        } else {
            blackWhiteImage = cache.getBinaryBlackWhiteImage()!!
        }
    }

    private fun loadDescriptors() {
        descriptors = if (isFirstRun && cache.getDescriptors() == null) {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        } else {
            cache.getDescriptors()!!
        }
    }

    private fun detectWordsIfAnyObjectIsPresent(): Result {
        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            detectWordsOnRowsIfPossible()?.run { return this }
        }
        return NoObjectsFound
    }

    private fun detectWordsOnRowsIfPossible(): Result? {
        val numberOfObjectPixelsOnRows = createHorizontalHistogram()

        getRowsDividers(numberOfObjectPixelsOnRows)?.run {
            prepareForWordsDetectionOnEachRow(this)

            val rows = blackWhiteImage.clone()
            val bgrImageWithWordsBoundaries =
                    if (settings.shouldShowGapsBetweenWords()) loadBgrImageWithWordsBoundaries()
                    else null
            if (detectWordsOnEachRow(rows, bgrImageWithWordsBoundaries!!)) return Cancelled
            persistWordsImages(bgrImageWithWordsBoundaries, rows)
            tryToDetectWordsInGapsIfPossible()

            return Success(wordsWithObjects, descriptors, segmentsWithNumberOfWords)
        } ?: logger.logNoRowDividers()
        return null
    }

    private fun prepareForWordsDetectionOnEachRow(rowsDividers: List<Int>) {
        updateGapTypeAveragesIfNecessary()
        mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
        shortenRowsDividers = shortenDividers(mergedRowsDividers)
        drawRowsBoundariesIfNecessary()
        params.onWordsDetection()
        wordsWithObjects = LinkedHashMap()
        segmentsWithNumberOfWords = LinkedHashMap()
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        var result: Mat
        if (isFirstRun) {
            result = cache.getBgrImage()
                    ?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)

            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
        } else {
            result = cache.getBgrImageWithWordsBoundaries()!!.clone()
        }
        return result
    }

    private fun createHorizontalHistogram() = createHistogram(blackWhiteImage, HORIZONTAL)

    private fun detectWordsOnEachRow(rows: Mat, bgrImageWithWordsBoundaries: Mat): Boolean {
        for (i in 0 until shortenRowsDividers.size - 1) {
            val currentRowDivider = shortenRowsDividers[i]
            if (doesRowContainObjects(currentRowDivider)) {
                if (params.isCancelled()) return true

                val nextRowDivider = shortenRowsDividers[i + 1]
                val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                val numberOfObjectPixelsOnCols = createVerticalHistogram(row)

                getColumnsDividers(numberOfObjectPixelsOnCols)?.run {
                    mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                    shortenColsDividers = shortenDividers(mergedColsDividers)

                    extractWordsOnRowIfPossible(bgrImageWithWordsBoundaries, i, rows)
                }
            }
        }
        return false
    }

    private fun extractWordsOnRowIfPossible(
            bgrImageWithWordsBoundaries: Mat, currentRowDividerIndex: Int, rows: Mat
    ) {
        if (shortenColsDividers.isNotEmpty()) {
            drawWordsBoundariesIfNecessary(bgrImageWithWordsBoundaries, currentRowDividerIndex)
            drawColumnsBoundariesIfNecessary(rows, currentRowDividerIndex)

            val segment = mutableListOf<Point>()
            val numberOfWordsInSegment = extractWordsOnRow(currentRowDividerIndex, segment)
            addSegmentToResultIfPossible(segment, numberOfWordsInSegment)
        }
    }

    private fun persistWordsImages(bgrImageWithWordsBoundaries: Mat, rows: Mat) {
        persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries)
        persistImageWithWordsBoundaries(rows)
        persistImageWithColoredWords()
    }

    private fun createVerticalHistogram(image: Mat) = createHistogram(image, VERTICAL)

    private fun createHistogram(image: Mat, mode: WordsDetectionMode): FloatArray {
        return image.run {
            val numberOfObjectPixels = FloatArray(if (mode.isHorizontal()) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE) {
                    if (mode.isHorizontal()) {
                        numberOfObjectPixels[i / cols()]++
                    } else {
                        numberOfObjectPixels[i % cols()]++
                    }
                }
            }
            numberOfObjectPixels
        }
    }

    private fun updateGapTypeAveragesIfNecessary() {
        updateRowGapTypeAverageIfNecessary()
        updateWordGapTypeAverageIfNecessary()
    }

    private fun updateRowGapTypeAverageIfNecessary() {
        if (params.isRowAverageObjectSizeGapType()) {
            val heightAverage = descriptors.map { it.straightRectangleHeight }.average()
            params.setRowGapTypeValue(heightAverage.toFloat())
        }
    }

    private fun updateWordGapTypeAverageIfNecessary() {
        if (params.isWordAverageObjectSizeGapType()) {
            val widthAverage = descriptors.map { it.straightRectangleWidth }.average()
            params.setWordGapTypeValue(widthAverage.toFloat())
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) {
            val rows = blackWhiteImage.clone()
            drawer.drawHorizontalBoundaries(shortenRowsDividers, rows, blackWhiteImage.cols())
            storage.saveRowsBoundariesImage(rows)
            rows.release()
        }
    }

    private fun drawWordsBoundariesIfNecessary(
            image: Mat,
            currentRowDividerIndex: Int
    ) {
        drawVerticalBoundariesIfNecessary(shouldDrawWordsBoundaries(), image, currentRowDividerIndex,
                mergedColsDividers) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawWordsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(
            image: Mat,
            currentRowDividerIndex: Int
    ) {
        drawVerticalBoundariesIfNecessary(shouldCreateDebugInfo(), image, currentRowDividerIndex,
                shortenColsDividers) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawColumnsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    private fun drawVerticalBoundariesIfNecessary(
            condition: Boolean,
            image: Mat,
            currentRowDividerIndex: Int,
            dividers: List<Int>,
            drawAction: WordsDetectionDrawer.(Int, Int, List<Int>, Mat) -> Unit
    ) {
        if (condition) {
            drawAction(
                    drawer,
                    shortenRowsDividers[currentRowDividerIndex],
                    shortenRowsDividers[currentRowDividerIndex + 1],
                    dividers,
                    image
            )
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    private fun extractWordsOnRow(
            currentRowDividerIndex: Int,
            segment: MutableList<Point>
    ): Int {
        var numberOfWordsInSegment = 0
        for (i in 0 until shortenColsDividers.size - 1) {
            numberOfWordsInSegment +=
                    extractWordOnRowUsingColumnsRangeIfPossible(i, currentRowDividerIndex, segment)
        }
        return numberOfWordsInSegment
    }

    private fun extractWordOnRowUsingColumnsRangeIfPossible(
            currentShortenColsDivider: Int, currentRowDividerIndex: Int, segment: MutableList<Point>
    ): Int {
        val currentColDivider = shortenColsDividers[currentShortenColsDivider]

        if (doesColumnContainObjects(currentColDivider)) {
            val wordRect = determineWordBoundaries(
                    currentRowDividerIndex,
                    currentColDivider,
                    shortenColsDividers[currentShortenColsDivider + 1]
            )
            val wordObjects = mutableListOf<Descriptor>()
            val word = mutableListOf<Point>()

            descriptors.forEach { d ->
                if (isDescriptorCenterInsideRectangle(wordRect, d)) {
                    wordObjects.add(d)
                    word.addAll(d.matrix.toList())
                    descriptors.remove(d)
                }
            }

            if (addWordToResultIfPossible(wordObjects, word, segment)) return 1
        }
        return 0
    }

    private fun doesColumnContainObjects(currentColDivider: Int) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun determineWordBoundaries(
            currentRowDividerIndex: Int,
            currentColDivider: Int,
            nextColDivider: Int
    ): Rect {
        val currentRowDivider = shortenRowsDividers[currentRowDividerIndex]
        val nextRowDivider = shortenRowsDividers[currentRowDividerIndex + 1]
        val rowHeight = nextRowDivider - currentRowDivider
        var wordRectY = currentRowDivider + 1
        var wordRectHeight = rowHeight
        val maxExtraHeight = (rowHeight * BOUNDARY_TOLERANCE).toInt()
        if (currentRowDividerIndex - 1 >= 0) {
            val previousRowDivider = shortenRowsDividers[currentRowDividerIndex - 1]
            var topExtraHeight = ((currentRowDivider - previousRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (topExtraHeight > maxExtraHeight) topExtraHeight = maxExtraHeight
            wordRectY -= topExtraHeight
            wordRectHeight += topExtraHeight
        }
        if (currentRowDividerIndex + 2 in shortenRowsDividers.indices) {
            val nextNextRowDivider = shortenRowsDividers[currentRowDividerIndex + 2]
            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (bottomExtraHeight > maxExtraHeight) bottomExtraHeight = maxExtraHeight
            wordRectHeight += bottomExtraHeight
        }
        val width = nextColDivider - currentColDivider
        return Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)
    }

    private fun isDescriptorCenterInsideRectangle(rect: Rect, descriptor: Descriptor) =
            rect.contains(descriptor.center)

    private fun addWordToResultIfPossible(
            wordObjects: MutableList<Descriptor>,
            word: MutableList<Point>,
            segment: MutableList<Point>
    ): Boolean {
        if (wordObjects.isNotEmpty() && word.isNotEmpty()) {
            segment.addAll(word)
            val wordMatOfPoint = MatOfPoint()
            wordMatOfPoint.fromList(word)
            wordsWithObjects[Descriptor(wordMatOfPoint)] = wordObjects
            return true
        }
        return false
    }

    private fun addSegmentToResultIfPossible(
            segment: MutableList<Point>,
            numberOfWordsInSegment: Int
    ) {
        if (segment.isNotEmpty()) {
            val segmentMatOfPoint = MatOfPoint()
            segmentMatOfPoint.fromList(segment)
            segmentsWithNumberOfWords[Descriptor(segmentMatOfPoint)] = numberOfWordsInSegment
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords() && isFirstRun

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(shouldCreateDebugInfo(), rows)
    }

    private fun persistImageWithColoredWords() {
        storage.saveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(), blackWhiteImage.size(),
                wordsWithObjects.values.toList())
    }

    private fun tryToDetectWordsInGapsIfPossible() {
        if (shouldTryToDetectWordsInGaps()) {
            logger.logPreRecursionInfo(descriptors.size)

            doWithRecursionLock {
                val imageWithUnassignedObjects = prepareImageWithUnassignedObjects()
                val recursionResult = doWithFieldsRestore { detect() }

                if (recursionResult is Success) {
                    updateOriginalResult(recursionResult)
                    logger.logRecursionInfo(descriptors)
                    persistRecursiveImageWithColoredWords(imageWithUnassignedObjects)
                }
            }
        }
    }

    private fun shouldTryToDetectWordsInGaps() = descriptors.isNotEmpty() && isRecursionAllowed()

    private fun doWithRecursionLock(action: () -> Unit) {
        forbidRecursion()
        action()
        allowRecursion()
    }

    private fun prepareImageWithUnassignedObjects(): Mat {
        val blackWhiteImageWithUnassignedObjects = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val dataReadable = ByteArray(blackWhiteImageWithUnassignedObjects.rows() *
                blackWhiteImageWithUnassignedObjects.cols() *
                blackWhiteImageWithUnassignedObjects.channels())
        blackWhiteImageWithUnassignedObjects.get(0, 0, dataReadable)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % blackWhiteImageWithUnassignedObjects.cols() +
                        p.y.toInt() * blackWhiteImageWithUnassignedObjects.cols()
                dataReadable[index] = WHITE
            }
        }
        blackWhiteImageWithUnassignedObjects.put(0, 0, dataReadable)
        cache.cacheBinaryBlackWhiteImage(blackWhiteImageWithUnassignedObjects)
        cache.cacheDescriptors(descriptors)
        return blackWhiteImageWithUnassignedObjects
    }

    private inline fun <T> doWithFieldsRestore(action: () -> T): T {
        val blackWhiteImageCopy = blackWhiteImage
        val descriptorsCopy = descriptors.toMutableList()
        val shortenRowsDividersCopy = shortenRowsDividers.toList()
        val mergedRowsDividersCopy = mergedRowsDividers.toList()
        val shortenColsDividersCopy = shortenColsDividers.toList()
        val mergedColsDividersCopy = mergedColsDividers.toList()
        val wordsWithObjectsCopy = wordsWithObjects.toMutableMap()
        val segmentsWithNumberOfWordsCopy = segmentsWithNumberOfWords.toMutableMap()

        val result = action()

        blackWhiteImage = blackWhiteImageCopy
        descriptors = descriptorsCopy.toMutableList()
        shortenRowsDividers = shortenRowsDividersCopy.toList()
        mergedRowsDividers = mergedRowsDividersCopy.toList()
        shortenColsDividers = shortenColsDividersCopy.toList()
        mergedColsDividers = mergedColsDividersCopy.toList()
        wordsWithObjects = wordsWithObjectsCopy.toMutableMap()
        segmentsWithNumberOfWords = segmentsWithNumberOfWordsCopy.toMutableMap()

        return result
    }

    private fun updateOriginalResult(recursionResult: Success) {
        wordsWithObjects.putAll(recursionResult.wordsWithObjects)
        recursionResult.wordsWithObjects.values.forEach { descriptors.removeAll(it) }
        segmentsWithNumberOfWords.putAll(recursionResult.segmentsWithNumberOfWords)
    }

    private fun persistRecursiveImageWithColoredWords(blackWhiteImage: Mat) {
        storage.saveRecursiveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(),
                blackWhiteImage.size(), wordsWithObjects.values.toList())
    }

    private fun shouldCreateDebugInfo() = DEBUG && isFirstRun

    private fun isRecursionAllowed() = isFirstRun

    private fun forbidRecursion() {
        isFirstRun = false
    }

    private fun allowRecursion() {
        isFirstRun = true
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val segmentsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result()
    }
}