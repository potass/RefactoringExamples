package cz.potass.learn.refactoringexamples.smells.general

internal class ReplaceMagicNumbersWithNamedConstants {
    fun example1(seconds: Int): Int {
        return seconds / 86_400
    }

    fun example2() {
        repeat(39) {
            println(it)
        }
    }

    fun example3(result: Int) {
        when (result) {
            42 -> {}
            else -> {}
        }
    }

    fun example4(radius: Double): Double {
        return 3.141592653589793 * radius * radius
    }

    fun example5() {
        assertEquals(123, Employee.find("John Doe").id)
    }

    private companion object {
        const val SECONDS_PER_DAY = 86_400
        const val LINES_PER_PAGE = 39
        const val MEANING_OF_LIFE_UNIVERSE_AND_EVERYTHING = 42
        const val PI = 3.141592653589793
        const val TEST_EMPLOYEE_ID = 123
        const val TEST_EMPLOYEE_NAME = "John Doe"
    }

    fun refactoredExample1(seconds: Int): Int {
        return seconds / SECONDS_PER_DAY
    }

    fun refactoredExample2() {
        repeat(LINES_PER_PAGE) {
            println(it)
        }
    }

    fun refactoredExample3(result: Int) {
        when (result) {
            MEANING_OF_LIFE_UNIVERSE_AND_EVERYTHING -> {}
            else -> {}
        }
    }

    fun refactoredExample4(radius: Double): Double {
        return PI * radius * radius
    }

    fun refactoredExample5() {
        assertEquals(TEST_EMPLOYEE_ID, Employee.find(TEST_EMPLOYEE_NAME).id)
    }

    private fun assertEquals(i1: Int, i2: Int) {
        // Some code.
    }

    private data class Employee(val id: Int, val name: String) {
        companion object {
            fun find(name: String): Employee {
                // Some code.
                return Employee(0, "")
            }
        }
    }
}