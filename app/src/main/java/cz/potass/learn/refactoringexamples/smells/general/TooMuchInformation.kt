package cz.potass.learn.refactoringexamples.smells.general

import kotlin.Int.Companion.MIN_VALUE

internal class TooMuchInformation {
    class Circle(val radius: Double) {
        companion object {
            const val PI = 3.14
        }

        fun area(): Double {
            return PI * radius.toThePowerOf(2)
        }

        fun Double.toThePowerOf(power: Int): Double {
            return when (power) {
                in MIN_VALUE..-1 -> throw IllegalArgumentException()
                0 -> 1.0
                1 -> this
                else -> {
                    var result = this
                    repeat(power - 1) {
                        result *= result
                    }
                    result
                }
            }
        }

        fun perimeter(): Double {
            return 2 * PI * radius
        }
    }

    class RefactoredCircle(private val radius: Double) {
        private companion object {
            const val PI = 3.14
        }

        fun area(): Double {
            return PI * radius.toThePowerOf(2)
        }

        private fun Double.toThePowerOf(power: Int): Double {
            return when (power) {
                in MIN_VALUE..-1 -> throw IllegalArgumentException()
                0 -> 1.0
                1 -> this
                else -> {
                    var result = this
                    repeat(power - 1) {
                        result *= result
                    }
                    result
                }
            }
        }

        fun perimeter(): Double {
            return 2 * PI * radius
        }
    }
}