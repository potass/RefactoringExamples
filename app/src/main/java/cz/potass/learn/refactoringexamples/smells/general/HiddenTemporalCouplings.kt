package cz.potass.learn.refactoringexamples.smells.general

internal class HiddenTemporalCouplings {
    // Some instance variables used by all methods.
    private val a = 42
    private val b = "42"

    fun example() {
        a()
        b()
        c()
    }

    private fun a() {}
    private fun b() {}
    private fun c() {}

    fun refactoredExample() {
        val a = refactoredA()
        val b = refactoredB(a)
        refactoredC(b)
    }

    private fun refactoredA(): Int {
        return a * a
    }

    private fun refactoredB(a: Int): String {
        return a.toString()
    }

    private fun refactoredC(b: String) {}
}