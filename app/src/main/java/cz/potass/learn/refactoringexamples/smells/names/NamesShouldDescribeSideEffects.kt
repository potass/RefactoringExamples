package cz.potass.learn.refactoringexamples.smells.names

/** @see UnambiguousNames example */
internal class NamesShouldDescribeSideEffects {
    private var any: Any? = null

    fun getAny(): Any {
        if (any == null) {
            any = Any()
        }
        return any!!
    }

    fun createOrReturnAny(): Any {
        if (any == null) {
            any = Any()
        }
        return any!!
    }
}