package cz.potass.learn.refactoringexamples.showcase.step06

enum class WordsDetectionMode {
    HORIZONTAL,
    VERTICAL;

    fun isHorizontal() = this == HORIZONTAL
    fun isVertical() = this == VERTICAL
}