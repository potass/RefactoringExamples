package cz.potass.learn.refactoringexamples.showcase.step09

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionParams

internal class FirstRunWordsDetectionProcessor(params: WordsDetectionParams)
    : WordsDetectionProcessor(params) {
    override fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords()

    override fun shouldCreateDebugInfo() = DEBUG
}