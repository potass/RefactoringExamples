package cz.potass.learn.refactoringexamples.smells.functions

internal class FlagOrSelectorArguments {
    // SALARY EXAMPLE

    fun calculateSalary(includeOvertime: Boolean): Double {
        val straightPay = calculateStraightSalary()
        return if (includeOvertime) {
            straightPay + calculateOvertimeSalary()
        } else {
            straightPay
        }
    }

    private fun calculateStraightSalary(): Double {
        // Some more complex computation.
        return 42.0
    }

    private fun calculateOvertimeSalary(): Double {
        // Some more complex computation.
        return 4.2
    }

    fun refactoredCalculateOvertimeSalary(): Double {
        // Some more complex computation.
        return refactoredCalculateStraightSalary() + 4.2
    }

    fun refactoredCalculateStraightSalary(): Double {
        // Some more complex computation.
        return 42.0
    }

    // AREA EXAMPLE

    fun area(shape: FourSidedShape, isSquare: Boolean, isRectangle: Boolean): Double {
        return if (isSquare) {
            shape.a * shape.a
        } else if (isRectangle) {
            shape.a * shape.b
        } else if (isSquare && isRectangle) {
            // We are fucked as caller is an idiot.
            .0
        } else {
            // Random formula.
            shape.a * shape.b * shape.c * shape.d / Math.PI / 42
        }
    }

    data class FourSidedShape(
            val a: Double,
            val b: Double,
            val c: Double,
            val d: Double
    ) {
        constructor(a: Double, b: Double) : this(a, b, a, b)
        constructor(a: Double) : this(a, a, a, a)
    }

    fun refactoredArea(shape: RefactoredFourSidedShape): Double {
        return shape.area()
    }

    interface RefactoredFourSidedShape {
        fun area(): Double
    }

    data class Square(private val a: Double) : RefactoredFourSidedShape {
        override fun area() = a * a
    }

    data class Rectangle(private val a: Double, private val b: Double) : RefactoredFourSidedShape {
        override fun area() = a * b
    }

    data class Other(
            private val a: Double,
            private val b: Double,
            private val c: Double,
            private val d: Double
    ) : RefactoredFourSidedShape {
        override fun area() = a * b * c * d / Math.PI / 42
    }
}