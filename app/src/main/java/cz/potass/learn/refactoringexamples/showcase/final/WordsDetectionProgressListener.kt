package cz.potass.learn.refactoringexamples.showcase.final

internal interface WordsDetectionProgressListener {
    fun onStart()
    fun onWordsComponentsDetection()
    fun onWordsDetection()
}