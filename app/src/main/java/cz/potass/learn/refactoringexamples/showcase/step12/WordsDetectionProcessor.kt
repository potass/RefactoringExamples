package cz.potass.learn.refactoringexamples.showcase.step12

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionLogger
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionHistogramProcessor
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionLoader
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionParams
import cz.potass.learn.refactoringexamples.showcase.step12.WordsDetectionProcessor.Result.*

/**
 * 1. Refactor SecondRunWordsDetectionProcessor (extract methods).
 * 1. Refactor WordsDetectionDrawer (simplify method params).
 * 1. Refactor WordsDetectionStorage (define Params class).
 * 1. Extract WordsDetectionExtensions.
 * 1. Remove 2 params from RowWordsExtractor.Params.
 * 1. Do some minor changes (delete mergedColsDividers field, ...).
 */
internal abstract class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    protected val cache = Cache.get()
    protected val settings = Settings.get()
    protected val logger = WordsDetectionLogger()
    private val loader = WordsDetectionLoader(params)
    private val storage = WordsDetectionStorage(params)
    private val histogramProcessor = WordsDetectionHistogramProcessor()
    private val drawer = WordsDetectionDrawer(storage)

    private lateinit var blackWhiteImage: Mat
    protected lateinit var descriptors: MutableList<Descriptor> private set
    private lateinit var mergedRowsDividers: List<Int>
    private lateinit var shortenRowsDividers: List<Int>
    private val wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
            by lazy { LinkedHashMap<Descriptor, List<Descriptor>>() }
    private val rowsWithNumberOfWords: MutableMap<Descriptor, Int>
            by lazy { LinkedHashMap<Descriptor, Int>() }

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     */
    open fun detect(): Result {
        params.onStart()

        blackWhiteImage = loader.loadBlackWhiteImage()
        descriptors = loader.loadDescriptors()

        return detectWordsIfAnyObjectIsPresent()
    }

    private fun detectWordsIfAnyObjectIsPresent(): Result {
        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            detectWordsOnRowsIfPossible()?.run { return this }
        }
        return NoObjectsFound
    }

    private fun detectWordsOnRowsIfPossible(): Result? {
        val numberOfObjectPixelsOnRows = histogramProcessor.createHorizontalHistogram(blackWhiteImage)

        getRowsDividers(numberOfObjectPixelsOnRows)?.run {
            prepareForWordsDetectionOnEachRow(this)

            val rows = blackWhiteImage.clone()
            val bgrImageWithWordsBoundaries =
                    if (settings.shouldShowGapsBetweenWords()) loader.loadBgrImageWithWordsBoundaries()
                    else null
            if (detectWordsOnEachRow(rows, bgrImageWithWordsBoundaries)) return Cancelled
            persistWordsImages(bgrImageWithWordsBoundaries, rows)

            return Success(wordsWithObjects, descriptors, rowsWithNumberOfWords)
        } ?: logger.logNoRowDividers()
        return null
    }

    private fun prepareForWordsDetectionOnEachRow(rowsDividers: List<Int>) {
        updateGapTypeAveragesIfNecessary()
        mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
        shortenRowsDividers = shortenDividers(mergedRowsDividers)
        drawRowsBoundariesIfNecessary()
        params.onWordsDetection()
    }

    private fun detectWordsOnEachRow(rows: Mat, bgrImageWithWordsBoundaries: Mat?): Boolean {
        for (i in 0 until shortenRowsDividers.size - 1) {
            val currentRowDivider = shortenRowsDividers[i]
            if (doesRowContainObjects(currentRowDivider)) {
                if (params.isCancelled()) return true

                val nextRowDivider = shortenRowsDividers[i + 1]
                val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                val numberOfObjectPixelsOnCols = histogramProcessor.createVerticalHistogram(row)

                getColumnsDividers(numberOfObjectPixelsOnCols)?.run {
                    val mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())

                    extractWordsOnRowIfPossible(RowWordsExtractor.Params(
                            drawer, bgrImageWithWordsBoundaries, shouldShowGapsBetweenWords(),
                            shortenRowsDividers, mergedColsDividers, i, rows,
                            shouldCreateDebugInfo(), descriptors
                    ))
                }
            }
        }
        return false
    }

    private fun extractWordsOnRowIfPossible(params: RowWordsExtractor.Params) {
        with(params) {
            if (canExtractWordsOnRow())
                RowWordsExtractor(this).extract().let(::addRowWordsToResultIfPossible)
        }
    }

    private fun persistWordsImages(bgrImageWithWordsBoundaries: Mat?, rows: Mat) {
        bgrImageWithWordsBoundaries?.let(::persistBgrImageWithWordsBoundariesIfPossible)
        persistImageWithWordsBoundaries(rows)
        persistImageWithColoredWords(blackWhiteImage)
    }

    private fun updateGapTypeAveragesIfNecessary() {
        params.updateRowGapTypeAverageIfPossible {
            descriptors.map { it.straightRectangleHeight }.average().toFloat()
        }
        params.updateWordGapTypeAverageIfPossible {
            descriptors.map { it.straightRectangleWidth }.average().toFloat()
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) {
            drawer.drawRowsBoundaries(WordsDetectionDrawer.Params(
                    blackWhiteImage, shortenRowsDividers
            ))
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int) =
            mergedRowsDividers.doesSegmentContainObjects(currentRowDivider)

    private fun addRowWordsToResultIfPossible(rowWords: List<WordInfo>) {
        if (rowWords.isNotEmpty()) {
            rowWords.forEach {
                descriptors.removeAll(it.wordObjects)
                wordsWithObjects[Descriptor(it.createWordMatOfPoint())] = it.wordObjects
            }
            val rowPoints = rowWords.flatMap { it.word }
            val rowMatOfPoint = MatOfPoint().apply { fromList(rowPoints) }
            rowsWithNumberOfWords[Descriptor(rowMatOfPoint)] = rowWords.size
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(WordsDetectionStorage.Params(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries
            ))
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    protected abstract fun shouldShowGapsBetweenWords(): Boolean

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(
                WordsDetectionStorage.Params(shouldCreateDebugInfo(), rows)
        )
    }

    private fun persistImageWithColoredWords(blackWhiteImage: Mat) {
        persistImageWithColoredWords { arg1, arg2 ->
            saveImageWithColoredWordsIfPossible(
                    WordsDetectionStorage.Params(arg1, blackWhiteImage, arg2)
            )
        }
    }

    protected fun persistImageWithColoredWords(
            saveAction: WordsDetectionStorage.(Boolean, List<List<Descriptor>>) -> Unit
    ) {
        saveAction(storage, shouldCreateDebugInfo(), wordsWithObjects.values.toList())
    }

    protected abstract fun shouldCreateDebugInfo(): Boolean

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val rowsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result() {
            fun isAnyObjectUnprocessed() = descriptors.isNotEmpty()
        }
    }
}