package cz.potass.learn.refactoringexamples.smells.java

internal class ConstantsVersusEnums {
    fun salaryExample(hoursWorked: Int, hourlyRate: Double): Double {
        return hoursWorked * hourlyRate
    }

    fun refactoredSalaryExample(hoursWorked: Int, grade: HourlyPayGrade): Double {
        return hoursWorked * grade.rate
    }

    enum class HourlyPayGrade(val rate: Double) {
        JUNIOR(1.0),
        INTERMEDIATE(1.5),
        SENIOR(2.0)
    }
}