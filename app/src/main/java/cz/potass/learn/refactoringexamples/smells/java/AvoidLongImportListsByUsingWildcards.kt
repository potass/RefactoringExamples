package cz.potass.learn.refactoringexamples.smells.java

//import java.util.ArrayList
//import java.util.HashMap
//import java.util.HashSet

import java.util.*

internal class AvoidLongImportListsByUsingWildcards {
    private val list = ArrayList<Any>()
    private val set = HashSet<Any>()
    private val map = HashMap<Any, Any>()
}