package cz.potass.learn.refactoringexamples.showcase.final

internal enum class WordsDetectionMode {
    HORIZONTAL,
    VERTICAL;

    fun isHorizontal() = this == HORIZONTAL
    fun isVertical() = this == VERTICAL
}