package cz.potass.learn.refactoringexamples.smells.general

internal class FunctionNamesShouldSayWhatTheyDo {
    fun dateExample() {
        Date().add(10)
    }

    class Date {
        fun add(number: Int): Date {
            // Some code.
            return this
        }
    }

    fun refactoredDateExample() {
        RefactoredDate().addDays(10).addMonths(5).addYears(-1)
    }

    class RefactoredDate {
        fun addDays(number: Int): RefactoredDate {
            // Some code.
            return this
        }

        fun addMonths(number: Int): RefactoredDate {
            // Some code.
            return this
        }

        fun addYears(number: Int): RefactoredDate {
            // Some code.
            return this
        }
    }
}