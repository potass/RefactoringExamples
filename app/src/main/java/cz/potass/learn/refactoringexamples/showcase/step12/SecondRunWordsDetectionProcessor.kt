package cz.potass.learn.refactoringexamples.showcase.step12

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionParams
import cz.potass.learn.refactoringexamples.showcase.step12.WordsDetectionDrawer.Companion.WHITE_AS_BYTE
import cz.potass.learn.refactoringexamples.showcase.step12.WordsDetectionProcessor.Result.Success

internal class SecondRunWordsDetectionProcessor(params: WordsDetectionParams)
    : WordsDetectionProcessor(params) {
    private val imageWithUnassignedObjects: Mat

    init {
        val blackWhiteImage = checkNotNull(cache.getBinaryBlackWhiteImage())
        val descriptors = checkNotNull(cache.getDescriptors())
        imageWithUnassignedObjects = prepareImageWithUnassignedObjects(blackWhiteImage, descriptors)
    }

    private fun prepareImageWithUnassignedObjects(
            blackWhiteImage: Mat, descriptors: List<Descriptor>
    ): Mat {
        val bwImageWithUnassignedObjects =
                createImageWithUnassignedObjects(descriptors, blackWhiteImage)
        cache.cacheBinaryBlackWhiteImage(bwImageWithUnassignedObjects)
        return bwImageWithUnassignedObjects
    }

    private fun createImageWithUnassignedObjects(
            descriptors: List<Descriptor>, blackWhiteImage: Mat
    ): Mat {
        val image = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val imageData = initDataForImageWithUnassignedObjects(image)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % image.cols() + p.y.toInt() * image.cols()
                imageData[index] = WHITE_AS_BYTE
            }
        }
        image.put(0, 0, imageData)
        return image
    }

    private fun initDataForImageWithUnassignedObjects(image: Mat): ByteArray {
        val imageData = ByteArray(image.run { rows() * cols() * channels() })
        image.get(0, 0, imageData)
        return imageData
    }

    override fun detect(): Result {
        logger.logPreRecursionInfo(descriptors.size)
        val result = super.detect()
        if (result is Success) {
            logger.logRecursionInfo(descriptors)
            persistRecursiveImageWithColoredWords(imageWithUnassignedObjects)
        }
        return result
    }

    private fun persistRecursiveImageWithColoredWords(blackWhiteImage: Mat) {
        persistImageWithColoredWords { arg1, arg2 ->
            saveRecursiveImageWithColoredWordsIfPossible(
                    WordsDetectionStorage.Params(arg1, blackWhiteImage, arg2)
            )
        }
    }

    override fun shouldShowGapsBetweenWords() = false

    override fun shouldCreateDebugInfo() = false
}