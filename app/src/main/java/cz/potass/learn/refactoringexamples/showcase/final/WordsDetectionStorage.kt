package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.VERTICAL_WORDS_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME

internal class WordsDetectionStorage(private val params: WordsDetectionParams) {
    fun saveRowsBoundariesImage(rows: Mat) {
        val suffix =
                if (params.isHorizontal()) HORIZONTAL_ROWS_BOUNDARIES_IMAGE_NAME
                else VERTICAL_ROWS_BOUNDARIES_IMAGE_NAME
        save(rows, suffix)
    }

    fun saveBgrImageWithWordsBoundariesIfPossible(params: Params) {
        params.saveConditional { WHITE_BLACK_BGR_WITH_WORDS_BOUNDARIES_IMAGE_NAME }
    }

    fun saveImageWithWordsBoundariesIfPossible(params: Params) {
        params.saveConditional {
            if (this.params.isHorizontal()) HORIZONTAL_WORDS_BOUNDARIES_IMAGE_NAME
            else VERTICAL_WORDS_BOUNDARIES_IMAGE_NAME
        }
    }

    private fun Params.saveConditional(suffix: () -> String) {
        if (condition) save(image, suffix())
    }

    private fun save(imageMat: Mat, suffix: String) =
            conditionalSave(true, params.image, imageMat, suffix)

    fun saveImageWithColoredWordsIfPossible(params: Params) {
        params.saveColoredConditional {
            if (this.params.isHorizontal()) HORIZONTAL_WORDS_IMAGE_NAME
            else VERTICAL_WORDS_IMAGE_NAME
        }
    }

    fun saveRecursiveImageWithColoredWordsIfPossible(params: Params) {
        params.saveColoredConditional {
            if (this.params.isHorizontal()) RECURSIVE_HORIZONTAL_WORDS_IMAGE_NAME
            else RECURSIVE_VERTICAL_WORDS_IMAGE_NAME
        }
    }

    private fun Params.saveColoredConditional(suffix: () -> String) {
        if (condition) saveColored(size, objects, suffix())
    }

    private fun saveColored(size: Size, objects: List<List<Descriptor>>, suffix: String) {
        conditionalColorAndSave(true, params.image, size, objects, suffix)
    }

    data class Params(
            val condition: Boolean,
            val image: Mat,
            val objects: List<List<Descriptor>> = emptyList()
    ) {
        val size = image.size()
    }
}