package cz.potass.learn.refactoringexamples.smells.general

internal class IncorrectBehaviorAtTheBoundaries {
    private val list = listOf(42, 42, 42)

    fun indexExample(index: Int?): Int {
        return list[index!!]
    }

    fun refactoredIndexExample(index: Int?): Int {
        return if (index != null && index in list.indices) {
            list[index]
        } else {
            -1
        }
    }
}