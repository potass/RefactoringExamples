package cz.potass.learn.refactoringexamples.showcase.step06

import cz.potass.learn.refactoringexamples.BuildConfig.DEBUG
import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BINARY_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.BLACK_WHITE_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.ImageConstants.WHITE_BLACK_BGR_IMAGE_NAME
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.CV_LOAD_IMAGE_UNCHANGED
import cz.potass.learn.refactoringexamples.showcase.shared.Imgcodecs.imread
import cz.potass.learn.refactoringexamples.showcase.shared.Mat.Companion.zeros
import cz.potass.learn.refactoringexamples.showcase.shared.ObjectDetection.findObjects
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.HORIZONTAL
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionMode.VERTICAL
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionProcessor.Result.*
import java.util.*

/**
 *  1. Pass less parameters.
 *      1. Extract global fields (blackWhiteImage, descriptors, shortenRowsDividers).
 *  1. Extract other additional methods.
 *  1. Extract WordsDetectionLogger.
 *  1. Extract Params and Mode to separate classes to make them usable in other places.
 *  1. Extract WordsDetectionStorage and WordsDetectionDrawer.
 */
internal class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    private companion object {
        const val PATH_SEPARATOR = "/"
        const val WHITE = WordsDetectionDrawer.WHITE.toByte()
        const val BOUNDARY_TOLERANCE = 0.5
    }

    private val cache = Cache.get()
    private val settings = Settings.get()
    private var isFirstRun = true
    private val logger = WordsDetectionLogger()
    private val storage = WordsDetectionStorage(params)
    private val drawer = WordsDetectionDrawer()

    private lateinit var blackWhiteImage: Mat
    private lateinit var descriptors: MutableList<Descriptor>
    private lateinit var shortenRowsDividers: List<Int>
    private lateinit var mergedColsDividers: List<Int>
    private lateinit var shortenColsDividers: List<Int>
    private lateinit var wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
    private lateinit var segmentsWithNumberOfWords: MutableMap<Descriptor, Int>

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     * TODO create 3 classes, one for each logical procedure?
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    // TODO make smaller!
    fun detect(): Result {
        params.onStart()

        loadBlackWhiteImage()
        loadDescriptors()

        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            val numberOfObjectPixelsOnRows = createHorizontalHistogram()
            val rowsDividers = getRowsDividers(numberOfObjectPixelsOnRows)

            rowsDividers?.run {
                updateGapTypeAveragesIfNecessary()

                val mergedRowsDividers = mergeRowsDividers(this, params.rowGapType)
                shortenRowsDividers = shortenDividers(mergedRowsDividers)
                drawRowsBoundariesIfNecessary()

                params.onWordsDetection()

                val rows = blackWhiteImage.clone()
                val bgrImageWithWordsBoundaries =
                        if (settings.shouldShowGapsBetweenWords()) loadBgrImageWithWordsBoundaries()
                        else null

                wordsWithObjects = LinkedHashMap()
                segmentsWithNumberOfWords = LinkedHashMap()

                for (i in 0 until shortenRowsDividers.size - 1) {
                    val currentRowDivider = shortenRowsDividers[i]
                    if (doesRowContainObjects(currentRowDivider, mergedRowsDividers)) {
                        if (params.isCancelled()) return Cancelled

                        val nextRowDivider = shortenRowsDividers[i + 1]
                        val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                        val numberOfObjectPixelsOnCols = createVerticalHistogram(row)
                        val colsDividers = getColumnsDividers(numberOfObjectPixelsOnCols)

                        colsDividers?.run {
                            mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                            shortenColsDividers = shortenDividers(mergedColsDividers)

                            if (shortenColsDividers.isNotEmpty()) {
                                drawWordsBoundariesIfNecessary(bgrImageWithWordsBoundaries!!, i)
                                drawColumnsBoundariesIfNecessary(rows, i)

                                val segment = mutableListOf<Point>()
                                val numberOfWordsInSegment = extractWordsOnRow(i, segment)
                                addSegmentToResultIfPossible(segment, numberOfWordsInSegment)
                            }
                        }
                    }
                }

                persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries!!)
                persistImageWithWordsBoundaries(rows)
                persistImageWithColoredWords()

                tryToDetectWordsInGapsIfPossible()

                return Success(wordsWithObjects, descriptors, segmentsWithNumberOfWords)
            } ?: logger.logNoRowDividers()
        }

        return NoObjectsFound
    }

    // TODO extract methods to separate classes which possible regarding SRP!!!

    private fun loadBlackWhiteImage() {
        if (isFirstRun && cache.getBinaryBlackWhiteImage() == null) {
            blackWhiteImage = imread("${params.basePath}$PATH_SEPARATOR$BLACK_WHITE_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) blackWhiteImage = rotateBy90Multiple(blackWhiteImage, 1)
            cache.cacheBinaryBlackWhiteImage(blackWhiteImage)
        } else {
            blackWhiteImage = cache.getBinaryBlackWhiteImage()!!
        }
    }

    private fun loadDescriptors() {
        descriptors = if (isFirstRun && cache.getDescriptors() == null) {
            var binaryImage = imread("${params.basePath}$PATH_SEPARATOR$BINARY_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)
            if (params.isVertical()) binaryImage = rotateBy90Multiple(binaryImage, 1)
            findObjects(settings, binaryImage, settings.shouldMergeObjects(), params.isCancelled)
        } else {
            cache.getDescriptors()!!
        }
    }

    private fun loadBgrImageWithWordsBoundaries(): Mat {
        var result: Mat
        if (isFirstRun) {
            result = cache.getBgrImage()
                    ?.clone()
                    ?: imread("${params.basePath}$PATH_SEPARATOR$WHITE_BLACK_BGR_IMAGE_NAME",
                    CV_LOAD_IMAGE_UNCHANGED)

            if (params.isVertical()) result = rotateBy90Multiple(result, 1)
        } else {
            result = cache.getBgrImageWithWordsBoundaries()!!.clone()
        }
        return result
    }

    private fun createHorizontalHistogram() = createHistogram(blackWhiteImage, HORIZONTAL)

    private fun createVerticalHistogram(image: Mat) = createHistogram(image, VERTICAL)

    private fun createHistogram(image: Mat, mode: WordsDetectionMode): FloatArray {
        return image.run {
            val numberOfObjectPixels = FloatArray(if (mode.isHorizontal()) rows() else cols())
            val imageData = ByteArray(rows() * cols() * channels())
            get(0, 0, imageData)
            imageData.forEachIndexed { i, it ->
                if (it == WHITE) {
                    if (mode.isHorizontal()) {
                        numberOfObjectPixels[i / cols()]++
                    } else {
                        numberOfObjectPixels[i % cols()]++
                    }
                }
            }
            numberOfObjectPixels
        }
    }

    private fun updateGapTypeAveragesIfNecessary() {
        updateRowGapTypeAverageIfNecessary()
        updateWordGapTypeAverageIfNecessary()
    }

    private fun updateRowGapTypeAverageIfNecessary() {
        if (params.isRowAverageObjectSizeGapType()) {
            val heightAverage = descriptors.map { it.straightRectangleHeight }.average()
            params.setRowGapTypeValue(heightAverage.toFloat())
        }
    }

    private fun updateWordGapTypeAverageIfNecessary() {
        if (params.isWordAverageObjectSizeGapType()) {
            val widthAverage = descriptors.map { it.straightRectangleWidth }.average()
            params.setWordGapTypeValue(widthAverage.toFloat())
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) {
            val rows = blackWhiteImage.clone()
            drawer.drawHorizontalBoundaries(shortenRowsDividers, rows, blackWhiteImage.cols())
            storage.saveRowsBoundariesImage(rows)
            rows.release()
        }
    }

    private fun drawWordsBoundariesIfNecessary(
            image: Mat,
            currentRowDividerIndex: Int
    ) {
        drawVerticalBoundariesIfNecessary(shouldDrawWordsBoundaries(), image, currentRowDividerIndex,
                mergedColsDividers) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawWordsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(
            image: Mat,
            currentRowDividerIndex: Int
    ) {
        drawVerticalBoundariesIfNecessary(shouldCreateDebugInfo(), image, currentRowDividerIndex,
                shortenColsDividers) { arg1: Int, arg2: Int, arg3: List<Int>, arg4: Mat ->
            drawColumnsBoundaries(arg1, arg2, arg3, arg4)
        }
    }

    private fun drawVerticalBoundariesIfNecessary(
            condition: Boolean,
            image: Mat,
            currentRowDividerIndex: Int,
            dividers: List<Int>,
            drawAction: WordsDetectionDrawer.(Int, Int, List<Int>, Mat) -> Unit
    ) {
        if (condition) {
            drawAction(
                    drawer,
                    shortenRowsDividers[currentRowDividerIndex],
                    shortenRowsDividers[currentRowDividerIndex + 1],
                    dividers,
                    image
            )
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int, mergedRowsDividers: List<Int>) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    // TODO needs more refactoring!
    private fun extractWordsOnRow(
            currentRowDividerIndex: Int,
            segment: MutableList<Point>
    ): Int {
        var numberOfWordsInSegment = 0
        for (i in 0 until shortenColsDividers.size - 1) {
            val currentColDivider = shortenColsDividers[i]

            if (doesColumnContainObjects(currentColDivider)) {
                val wordRect = determineWordBoundaries(
                        currentRowDividerIndex,
                        currentColDivider,
                        shortenColsDividers[i + 1]
                )

                val wordObjects = mutableListOf<Descriptor>()
                val word = mutableListOf<Point>()

                val processedDescriptors = mutableListOf<Descriptor>()
                descriptors.forEach { d ->
                    if (isDescriptorCenterInsideRectangle(wordRect, d)) {
                        wordObjects.add(d)
                        word.addAll(d.matrix.toList())
                        processedDescriptors.add(d)
                    }
                }
                descriptors.removeAll(processedDescriptors)

                if (addWordToResultIfPossible(wordObjects, word, segment)) numberOfWordsInSegment++
            }
        }
        return numberOfWordsInSegment
    }

    private fun doesColumnContainObjects(currentColDivider: Int) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun determineWordBoundaries(
            currentRowDividerIndex: Int,
            currentColDivider: Int,
            nextColDivider: Int
    ): Rect {
        val currentRowDivider = shortenRowsDividers[currentRowDividerIndex]
        val nextRowDivider = shortenRowsDividers[currentRowDividerIndex + 1]
        val rowHeight = nextRowDivider - currentRowDivider
        var wordRectY = currentRowDivider + 1
        var wordRectHeight = rowHeight
        val maxExtraHeight = (rowHeight * BOUNDARY_TOLERANCE).toInt()
        if (currentRowDividerIndex - 1 >= 0) {
            val previousRowDivider = shortenRowsDividers[currentRowDividerIndex - 1]
            var topExtraHeight = ((currentRowDivider - previousRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (topExtraHeight > maxExtraHeight) topExtraHeight = maxExtraHeight
            wordRectY -= topExtraHeight
            wordRectHeight += topExtraHeight
        }
        if (currentRowDividerIndex + 2 in shortenRowsDividers.indices) {
            val nextNextRowDivider = shortenRowsDividers[currentRowDividerIndex + 2]
            var bottomExtraHeight = ((nextNextRowDivider - nextRowDivider) * BOUNDARY_TOLERANCE).toInt()
            if (bottomExtraHeight > maxExtraHeight) bottomExtraHeight = maxExtraHeight
            wordRectHeight += bottomExtraHeight
        }
        val width = nextColDivider - currentColDivider
        return Rect(currentColDivider + 1, wordRectY, width, wordRectHeight)
    }

    private fun isDescriptorCenterInsideRectangle(rect: Rect, descriptor: Descriptor) =
            rect.contains(descriptor.center)

    private fun addWordToResultIfPossible(
            wordObjects: MutableList<Descriptor>,
            word: MutableList<Point>,
            segment: MutableList<Point>
    ): Boolean {
        if (wordObjects.isNotEmpty() && word.isNotEmpty()) {
            segment.addAll(word)
            val wordMatOfPoint = MatOfPoint()
            wordMatOfPoint.fromList(word)
            wordsWithObjects[Descriptor(wordMatOfPoint)] = wordObjects
            return true
        }
        return false
    }

    private fun addSegmentToResultIfPossible(
            segment: MutableList<Point>,
            numberOfWordsInSegment: Int
    ) {
        if (segment.isNotEmpty()) {
            val segmentMatOfPoint = MatOfPoint()
            segmentMatOfPoint.fromList(segment)
            segmentsWithNumberOfWords[Descriptor(segmentMatOfPoint)] = numberOfWordsInSegment
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun shouldShowGapsBetweenWords() = settings.shouldShowGapsBetweenWords() && isFirstRun

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(shouldCreateDebugInfo(), rows)
    }

    private fun persistImageWithColoredWords() {
        storage.saveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(), blackWhiteImage.size(),
                wordsWithObjects.values.toList())
    }

    private fun tryToDetectWordsInGapsIfPossible() {
        if (shouldTryToDetectWordsInGaps()) {
            logger.logPreRecursionInfo(descriptors.size)

            doWithRecursionLock {
                val imageWithUnassignedObjects = prepareImageWithUnassignedObjects()
                val recursionResult = doWithFieldsRestore { detect() }

                if (recursionResult is Success) {
                    updateOriginalResult(recursionResult)
                    logger.logRecursionInfo(descriptors)
                    persistRecursiveImageWithColoredWords(imageWithUnassignedObjects)
                }
            }
        }
    }

    private fun shouldTryToDetectWordsInGaps() = descriptors.isNotEmpty() && isRecursionAllowed()

    private fun doWithRecursionLock(action: () -> Unit) {
        forbidRecursion()
        action()
        allowRecursion()
    }

    private fun prepareImageWithUnassignedObjects(): Mat {
        val blackWhiteImageWithUnassignedObjects = zeros(blackWhiteImage.size(), blackWhiteImage.type())
        val dataReadable = ByteArray(blackWhiteImageWithUnassignedObjects.rows() *
                blackWhiteImageWithUnassignedObjects.cols() *
                blackWhiteImageWithUnassignedObjects.channels())
        blackWhiteImageWithUnassignedObjects.get(0, 0, dataReadable)
        descriptors.forEach { d ->
            d.matrix.toList().forEach { p ->
                val index = p.x.toInt() % blackWhiteImageWithUnassignedObjects.cols() +
                        p.y.toInt() * blackWhiteImageWithUnassignedObjects.cols()
                dataReadable[index] = WHITE
            }
        }
        blackWhiteImageWithUnassignedObjects.put(0, 0, dataReadable)
        cache.cacheBinaryBlackWhiteImage(blackWhiteImageWithUnassignedObjects)
        cache.cacheDescriptors(descriptors)
        return blackWhiteImageWithUnassignedObjects
    }

    private inline fun <T> doWithFieldsRestore(action: () -> T): T {
        val blackWhiteImageCopy = blackWhiteImage
        val descriptorsCopy = descriptors.toMutableList()
        val shortenRowsDividersCopy = shortenRowsDividers.toList()
        val shortenColsDividersCopy = shortenColsDividers.toList()
        val mergedColsDividersCopy = mergedColsDividers.toList()
        val wordsWithObjectsCopy = wordsWithObjects.toMutableMap()
        val segmentsWithNumberOfWordsCopy = segmentsWithNumberOfWords.toMutableMap()

        val result = action()

        blackWhiteImage = blackWhiteImageCopy
        descriptors = descriptorsCopy.toMutableList()
        shortenRowsDividers = shortenRowsDividersCopy.toList()
        shortenColsDividers = shortenColsDividersCopy.toList()
        mergedColsDividers = mergedColsDividersCopy.toList()
        wordsWithObjects = wordsWithObjectsCopy.toMutableMap()
        segmentsWithNumberOfWords = segmentsWithNumberOfWordsCopy.toMutableMap()

        return result
    }

    private fun updateOriginalResult(recursionResult: Success) {
        wordsWithObjects.putAll(recursionResult.wordsWithObjects)
        recursionResult.wordsWithObjects.values.forEach { descriptors.removeAll(it) }
        segmentsWithNumberOfWords.putAll(recursionResult.segmentsWithNumberOfWords)
    }

    private fun persistRecursiveImageWithColoredWords(blackWhiteImage: Mat) {
        storage.saveRecursiveImageWithColoredWordsIfPossible(shouldCreateDebugInfo(),
                blackWhiteImage.size(), wordsWithObjects.values.toList())
    }

    private fun shouldCreateDebugInfo() = DEBUG && isFirstRun

    private fun isRecursionAllowed() = isFirstRun

    private fun forbidRecursion() {
        isFirstRun = false
    }

    private fun allowRecursion() {
        isFirstRun = true
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val segmentsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result()
    }
}