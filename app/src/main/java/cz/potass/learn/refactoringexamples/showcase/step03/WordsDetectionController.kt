package cz.potass.learn.refactoringexamples.showcase.step03

import cz.potass.learn.refactoringexamples.showcase.shared.BaseAsyncTask
import cz.potass.learn.refactoringexamples.showcase.shared.GapType
import cz.potass.learn.refactoringexamples.showcase.shared.Image
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener
import cz.potass.learn.refactoringexamples.showcase.step03.WordsDetectionProcessor.Mode
import cz.potass.learn.refactoringexamples.showcase.step03.WordsDetectionProcessor.Params

internal abstract class WordsDetectionController(private val task: BaseAsyncTask) :
        WordsDetectionProgressListener {
    protected abstract val onStartStringRes: Int
    protected abstract val onWordsComponentsDetectionStringRes: Int
    protected abstract val onWordsDetectionStringRes: Int
    protected abstract val mode: Mode

    fun detect(
            rowGapType: GapType,
            wordGapType: GapType,
            image: Image
    ) {
        val params = Params(
                this,
                rowGapType,
                wordGapType,
                image,
                mode
        ) { task.isCancelled() }

        // Skipping result handling.
        WordsDetectionProcessor(params).detect()
    }

    override fun onStart() {
        onStartStringRes.doProgress()
    }

    override fun onWordsComponentsDetection() {
        onWordsComponentsDetectionStringRes.doProgress()
    }

    override fun onWordsDetection() {
        onWordsDetectionStringRes.doProgress()
    }

    private fun Int.doProgress() {
        task.doProgress(this)
    }
}