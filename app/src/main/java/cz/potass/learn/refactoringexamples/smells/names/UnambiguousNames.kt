package cz.potass.learn.refactoringexamples.smells.names

internal class UnambiguousNames {
    private val refactorReferences = true
    private lateinit var pathToRename: Path
    private val newName = ""

    fun doRename(): String {
        if (refactorReferences) {
            renameReferences()
        }
        renamePage()

        pathToRename.removeNameFromEnd()
        pathToRename.addNameToEnd(newName)
        return PathParser.render(pathToRename)
    }

    fun renamePageAndOptionallyAllReferences(): String {
        if (refactorReferences) {
            renameReferences()
        }
        renamePage()

        pathToRename.removeNameFromEnd()
        pathToRename.addNameToEnd(newName)
        return PathParser.render(pathToRename)
    }

    private fun renameReferences() {
        // Some code.
    }

    private fun renamePage() {
        // Some code.
    }

    interface Path {
        fun removeNameFromEnd()
        fun addNameToEnd(newName: String)
    }

    class PathParser {
        companion object {
            fun render(path: Path): String {
                // Some code
                return ""
            }
        }
    }
}
