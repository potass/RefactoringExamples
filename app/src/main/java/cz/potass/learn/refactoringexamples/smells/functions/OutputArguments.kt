package cz.potass.learn.refactoringexamples.smells.functions

internal class OutputArguments {
    fun inputOutputDataExample(data: Data) {
        data.variable = 0
    }

    fun refactoredInputOutputDataExample(data: Data): Data {
        val outputData = Data(data.value)
        outputData.variable = 0
        return outputData
    }

    data class Data(val value: Int) {
        var variable = 42
    }

    fun inputModificationExample(list: MutableList<Int>): Int {
        list.removeAt(2)
        return list.sum()
    }

    fun refactoredInputModificationExample(list: MutableList<Int>): Int {
        val listCopy = ArrayList(list)
        listCopy.removeAt(2)
        return listCopy.sum()
    }
}