package cz.potass.learn.refactoringexamples.smells.comments

internal class InappropriateInformation {
    /** @author Jan Vyhnanek */
    fun authorExample() {}

    fun refactoredAuthorExample() {}

    fun lastModifiedDateExample() {
        // Last modified on 20.3.2018 16:41.
        val value = 42
    }

    fun refactoredLastModifiedDateExample() {
        val value = 42
    }
}