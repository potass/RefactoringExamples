package cz.potass.learn.refactoringexamples.smells.names;

import java.util.ArrayList;
import java.util.List;

public class UseLongNamesForLongScopesJava {
    private List<Listener> listeners = new ArrayList<>();

    public void shortScopeExample() {
        for (Listener l : listeners) {
            l.onSuccess();
        }
    }

    interface Listener {
        void onSuccess();
    }
}
