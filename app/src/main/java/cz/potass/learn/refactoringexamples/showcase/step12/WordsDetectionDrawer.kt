package cz.potass.learn.refactoringexamples.showcase.step12

import cz.potass.learn.refactoringexamples.showcase.shared.Imgproc.line
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.Point
import cz.potass.learn.refactoringexamples.showcase.shared.Scalar

internal class WordsDetectionDrawer(private val storage: WordsDetectionStorage) {
    companion object {
        private const val WHITE = 255
        const val WHITE_AS_BYTE = WHITE.toByte()
        private val BGR_WHITE = Scalar(WHITE)
        private val BGR_RED = Scalar(54, 67, 244)
    }

    fun drawRowsBoundaries(params: Params) {
        with(params) {
            val rows = image.clone()
            drawHorizontalBoundaries(shortenRowsDividers, rows, image.cols())
            storage.saveRowsBoundariesImage(rows)
            rows.release()
        }
    }

    private fun drawHorizontalBoundaries(dividers: List<Int>, rows: Mat, numberOfCols: Int) {
        dividers.forEach {
            drawLine(
                    rows,
                    Point(1, it),
                    Point(numberOfCols, it),
                    Scalar(WHITE)
            )
        }
    }

    fun drawWordsBoundaries(params: Params) = params.drawVerticalBoundaries(BGR_RED)

    fun drawColumnsBoundaries(params: Params) = params.drawVerticalBoundaries(BGR_WHITE)

    private fun Params.drawVerticalBoundaries(color: Scalar) {
        dividers.forEach {
            drawLine(
                    image,
                    Point(it, currentRowDivider + 1),
                    Point(it, nextRowDivider),
                    color
            )
        }
    }

    private fun drawLine(image: Mat, start: Point, end: Point, color: Scalar) =
            line(image, start, end, color, 1, 8, 0)

    data class Params(
            val image: Mat,
            val shortenRowsDividers: List<Int>,
            val currentRowDividerIndex: Int = -1,
            val dividers: List<Int> = emptyList()
    ) {
        val currentRowDivider = shortenRowsDividers[currentRowDividerIndex]
        val nextRowDivider = shortenRowsDividers[currentRowDividerIndex + 1]
    }
}