package cz.potass.learn.refactoringexamples.smells.comments

internal class ObsoleteComment {
    fun vatExample(amount: Double): Double {
        // Add 23% VAT.
        return amount * 1.21
    }

    fun refactoredVatExample(amount: Double): Double {
        // Add 21% VAT || \DELETE\
        return amount * 1.21
    }
}