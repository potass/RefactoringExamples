package cz.potass.learn.refactoringexamples.showcase.final

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.MatOfPoint
import cz.potass.learn.refactoringexamples.showcase.shared.Point

internal data class WordInfo(
        val wordObjects: MutableList<Descriptor>,
        val word: MutableList<Point>
) {
    fun isWordPresent() = wordObjects.isNotEmpty() && word.isNotEmpty()

    fun createWordMatOfPoint() = MatOfPoint().apply { fromList(word) }
}