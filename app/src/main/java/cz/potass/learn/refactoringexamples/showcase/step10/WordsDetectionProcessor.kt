package cz.potass.learn.refactoringexamples.showcase.step10

import cz.potass.learn.refactoringexamples.showcase.shared.*
import cz.potass.learn.refactoringexamples.showcase.step06.WordsDetectionLogger
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfoFactory
import cz.potass.learn.refactoringexamples.showcase.step10.WordsDetectionProcessor.Result.*

/**
 * 1. Do some minor refactoring (by lazy, ...).
 * 1. Extract WordsDetectionLoader.
 * 1. Extract WordsDetectionHistogramProcessor.
 * 1. Extract more logic to WordsDetectionParams.
 * 1. Extract more logic to WordsDetectionDrawer.
 */
internal abstract class WordsDetectionProcessor(private val params: WordsDetectionParams) {
    protected val cache = Cache.get()
    protected val settings = Settings.get()
    protected val logger = WordsDetectionLogger()
    private val loader = WordsDetectionLoader(params)
    private val storage = WordsDetectionStorage(params)
    private val histogramProcessor = WordsDetectionHistogramProcessor()
    private val drawer = WordsDetectionDrawer(storage)

    private lateinit var blackWhiteImage: Mat
    protected lateinit var descriptors: MutableList<Descriptor> private set
    private lateinit var mergedRowsDividers: List<Int>
    private lateinit var shortenRowsDividers: List<Int>
    private lateinit var mergedColsDividers: List<Int>
    private lateinit var shortenColsDividers: List<Int>
    private val wordsWithObjects: MutableMap<Descriptor, List<Descriptor>>
            by lazy { LinkedHashMap<Descriptor, List<Descriptor>>() }
    private val rowsWithNumberOfWords: MutableMap<Descriptor, Int>
            by lazy { LinkedHashMap<Descriptor, Int>() }

    /**
     * Detects words. This algorithm consists of 3 phases:
     *
     *  1. Defines rows in the image.
     *  1. Defines gaps between words on each row.
     *  1. Extracts words depending on found rows and gaps.
     *
     * Method is regularly checking whether task was not cancelled, if yes then it returns
     * `null`, and is also automatically notifying listeners about progress.
     * <br></br><br></br>
     * **Keep in mind that this is a long running task, so run it on the background thread.**
     */
    open fun detect(): Result {
        params.onStart()

        blackWhiteImage = loader.loadBlackWhiteImage()
        descriptors = loader.loadDescriptors()

        return detectWordsIfAnyObjectIsPresent()
    }

    private fun detectWordsIfAnyObjectIsPresent(): Result {
        if (descriptors.isNotEmpty()) {
            if (params.isCancelled()) return Cancelled
            params.onWordsComponentsDetection()

            detectWordsOnRowsIfPossible()?.run { return this }
        }
        return NoObjectsFound
    }

    private fun detectWordsOnRowsIfPossible(): Result? {
        val numberOfObjectPixelsOnRows = histogramProcessor.createHorizontalHistogram(blackWhiteImage)

        getRowsDividers(numberOfObjectPixelsOnRows)?.run {
            prepareForWordsDetectionOnEachRow(this)

            val rows = blackWhiteImage.clone()
            val bgrImageWithWordsBoundaries =
                    if (settings.shouldShowGapsBetweenWords()) loader.loadBgrImageWithWordsBoundaries()
                    else null
            if (detectWordsOnEachRow(rows, bgrImageWithWordsBoundaries)) return Cancelled
            persistWordsImages(bgrImageWithWordsBoundaries, rows)

            return Success(wordsWithObjects, descriptors, rowsWithNumberOfWords)
        } ?: logger.logNoRowDividers()
        return null
    }

    private fun prepareForWordsDetectionOnEachRow(rowsDividers: List<Int>) {
        updateGapTypeAveragesIfNecessary()
        mergedRowsDividers = mergeRowsDividers(rowsDividers, params.rowGapType)
        shortenRowsDividers = shortenDividers(mergedRowsDividers)
        drawRowsBoundariesIfNecessary()
        params.onWordsDetection()
    }

    private fun detectWordsOnEachRow(rows: Mat, bgrImageWithWordsBoundaries: Mat?): Boolean {
        for (i in 0 until shortenRowsDividers.size - 1) {
            val currentRowDivider = shortenRowsDividers[i]
            if (doesRowContainObjects(currentRowDivider)) {
                if (params.isCancelled()) return true

                val nextRowDivider = shortenRowsDividers[i + 1]
                val row = rows.rowRange(currentRowDivider, nextRowDivider + 1)
                val numberOfObjectPixelsOnCols = histogramProcessor.createVerticalHistogram(row)

                getColumnsDividers(numberOfObjectPixelsOnCols)?.run {
                    mergedColsDividers = mergeColumnsDividers(this, params.wordGapType, row.cols())
                    shortenColsDividers = shortenDividers(mergedColsDividers)

                    extractWordsOnRowIfPossible(WordsOnRowParams(
                            shortenColsDividers, bgrImageWithWordsBoundaries, i, rows))
                }
            }
        }
        return false
    }

    // TODO extractWordsOnRow -> WordsOnRowParams

    private fun extractWordsOnRowIfPossible(params: WordsOnRowParams) {
        with(params) {
            if (canExtractWordsOnRow()) {
                bgrImageWithWordsBoundaries
                        ?.run { drawWordsBoundariesIfNecessary(this, currentRowDividerIndex) }
                drawColumnsBoundariesIfNecessary(rows, currentRowDividerIndex)

                val rowWords = extractWordsOnRow(currentRowDividerIndex)
                addRowWordsToResultIfPossible(rowWords)
            }
        }
    }

    private fun persistWordsImages(bgrImageWithWordsBoundaries: Mat?, rows: Mat) {
        bgrImageWithWordsBoundaries?.let(::persistBgrImageWithWordsBoundariesIfPossible)
        persistImageWithWordsBoundaries(rows)
        persistImageWithColoredWords(blackWhiteImage.size())
    }

    private fun updateGapTypeAveragesIfNecessary() {
        params.updateRowGapTypeAverageIfPossible {
            descriptors.map { it.straightRectangleHeight }.average().toFloat()
        }
        params.updateWordGapTypeAverageIfPossible {
            descriptors.map { it.straightRectangleWidth }.average().toFloat()
        }
    }

    private fun drawRowsBoundariesIfNecessary() {
        if (shouldCreateDebugInfo()) drawer.drawRowsBoundaries(blackWhiteImage, shortenRowsDividers)
    }

    private fun drawWordsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        if (shouldDrawWordsBoundaries()) {
            drawer.drawWordsBoundaries(WordsDetectionDrawer.Params(
                    shortenRowsDividers,
                    image,
                    currentRowDividerIndex,
                    mergedColsDividers
            ))
        }
    }

    private fun shouldDrawWordsBoundaries() = shouldShowGapsBetweenWords()

    private fun drawColumnsBoundariesIfNecessary(image: Mat, currentRowDividerIndex: Int) {
        if (shouldCreateDebugInfo()) {
            drawer.drawColumnsBoundaries(WordsDetectionDrawer.Params(
                    shortenRowsDividers,
                    image,
                    currentRowDividerIndex,
                    shortenColsDividers
            ))
        }
    }

    private fun doesRowContainObjects(currentRowDivider: Int) =
            doesSegmentContainObjects(currentRowDivider, mergedRowsDividers)

    private fun extractWordsOnRow(currentRowDividerIndex: Int): List<WordInfo> {
        val rowWords = mutableListOf<WordInfo>()
        for (i in 0 until shortenColsDividers.size - 1) {
            extractWordOnRowUsingColumnsRangeIfPossible(RowParams(
                    descriptors, shortenRowsDividers, shortenColsDividers, i, currentRowDividerIndex
            ))?.let(rowWords::add)
        }
        return rowWords
    }

    // TODO extractWordOnRow -> RowParams

    private fun extractWordOnRowUsingColumnsRangeIfPossible(params: RowParams): WordInfo? {
        with(params) {
            if (doesColumnContainObjects(currentColDivider)) {
                val wordInfo = WordInfoFactory(WordInfoFactory.Params(
                        descriptors,
                        shortenRowsDividers,
                        currentRowDividerIndex,
                        currentColDivider,
                        nextColDivider
                )).create()

                descriptors.removeAll(wordInfo.wordObjects)

                if (addWordToResultIfPossible(wordInfo)) return wordInfo
            }
            return null
        }
    }

    private fun doesColumnContainObjects(currentColDivider: Int) =
            doesSegmentContainObjects(currentColDivider, mergedColsDividers)

    private fun doesSegmentContainObjects(currentDivider: Int, mergedDividers: List<Int>) =
            currentDivider + 1 !in mergedDividers

    private fun addWordToResultIfPossible(wordInfo: WordInfo): Boolean {
        with(wordInfo) {
            if (isWordPresent()) {
                wordsWithObjects[Descriptor(createWordMatOfPoint())] = wordObjects
                return true
            }
            return false
        }
    }

    private fun addRowWordsToResultIfPossible(rowWords: List<WordInfo>) {
        if (rowWords.isNotEmpty()) {
            val rowPoints = rowWords.flatMap { it.word }
            val rowMatOfPoint = MatOfPoint().apply { fromList(rowPoints) }
            rowsWithNumberOfWords[Descriptor(rowMatOfPoint)] = rowWords.size
        }
    }

    private fun persistBgrImageWithWordsBoundariesIfPossible(bgrImageWithWordsBoundaries: Mat) {
        if (shouldPersistBgrImageWithWordsBoundaries()) {
            cache.cacheBgrImageWithWordsBoundaries(bgrImageWithWordsBoundaries)
            storage.saveBgrImageWithWordsBoundariesIfPossible(
                    shouldCreateDebugInfo(), bgrImageWithWordsBoundaries)
        }
    }

    private fun shouldPersistBgrImageWithWordsBoundaries() = shouldShowGapsBetweenWords()

    protected abstract fun shouldShowGapsBetweenWords(): Boolean

    private fun persistImageWithWordsBoundaries(rows: Mat) {
        storage.saveImageWithWordsBoundariesIfPossible(shouldCreateDebugInfo(), rows)
    }

    private fun persistImageWithColoredWords(blackWhiteImageSize: Size) {
        persistImageWithColoredWords { arg1, arg2 ->
            saveImageWithColoredWordsIfPossible(arg1, blackWhiteImageSize, arg2)
        }
    }

    protected fun persistImageWithColoredWords(
            saveAction: WordsDetectionStorage.(Boolean, List<List<Descriptor>>) -> Unit
    ) {
        saveAction(storage, shouldCreateDebugInfo(), wordsWithObjects.values.toList())
    }

    protected abstract fun shouldCreateDebugInfo(): Boolean

    private data class WordsOnRowParams(
            val shortenColsDividers: List<Int>,
            val bgrImageWithWordsBoundaries: Mat?,
            val currentRowDividerIndex: Int,
            val rows: Mat
    ) {
        fun canExtractWordsOnRow() = shortenColsDividers.isNotEmpty()
    }

    private data class RowParams(
            val descriptors: MutableList<Descriptor>,
            val shortenRowsDividers: List<Int>,
            val shortenColsDividers: List<Int>,
            val currentShortenColsDivider: Int,
            val currentRowDividerIndex: Int
    ) {
        val currentColDivider = shortenColsDividers[currentShortenColsDivider]
        val nextColDivider = shortenColsDividers[currentShortenColsDivider + 1]
    }

    sealed class Result {
        object Cancelled : Result()
        object NoObjectsFound : Result()
        data class Success(
                val wordsWithObjects: Map<Descriptor, List<Descriptor>>,
                val descriptors: List<Descriptor>,
                val rowsWithNumberOfWords: Map<Descriptor, Int>
        ) : Result() {
            fun isAnyObjectUnprocessed() = descriptors.isNotEmpty()
        }
    }
}