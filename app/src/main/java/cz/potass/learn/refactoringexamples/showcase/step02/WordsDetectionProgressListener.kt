package cz.potass.learn.refactoringexamples.showcase.step02

interface WordsDetectionProgressListener {
    fun onStart()
    fun onWordsComponentsDetection()
    fun onWordsDetection()
}