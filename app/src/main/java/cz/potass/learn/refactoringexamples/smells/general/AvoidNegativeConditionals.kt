package cz.potass.learn.refactoringexamples.smells.general

internal class AvoidNegativeConditionals {
    fun example(result: String) {
        if (!result.isBlank()) {
            // Some code.
        }
    }

    fun refactoredExample(result: String) {
        if (result.isNotBlank()) {
            // Some code.
        }
    }
}