package cz.potass.learn.refactoringexamples.smells.functions

internal class TooManyArguments {
    fun example(arg1: Int, arg2: String, arg3: Float, arg4: Double) {}

    fun refactoredExample(params: Params) {}

    data class Params(
            val arg1: Int,
            val arg2: String,
            val arg3: Float,
            val arg4: Double
    )
}