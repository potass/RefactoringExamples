package cz.potass.learn.refactoringexamples.showcase.step12

import cz.potass.learn.refactoringexamples.showcase.shared.Descriptor
import cz.potass.learn.refactoringexamples.showcase.shared.Mat
import cz.potass.learn.refactoringexamples.showcase.shared.shortenDividers
import cz.potass.learn.refactoringexamples.showcase.step08.WordInfo
import cz.potass.learn.refactoringexamples.showcase.step11.WordInfoExtractor

internal class RowWordsExtractor(private val params: Params) {
    fun extract(): List<WordInfo> {
        with(params) {
            bgrImageWithWordsBoundaries?.run { drawWordsBoundariesIfNecessary() }
            drawColumnsBoundariesIfNecessary()
            return extractWordsOnRow()
        }
    }

    private fun Params.drawWordsBoundariesIfNecessary() {
        if (shouldDrawWordsBoundaries) {
            drawer.drawWordsBoundaries(WordsDetectionDrawer.Params(
                    bgrImageWithWordsBoundaries!!,
                    shortenRowsDividers,
                    currentRowDividerIndex,
                    mergedColsDividers
            ))
        }
    }

    private fun Params.drawColumnsBoundariesIfNecessary() {
        if (shouldDrawColumnsBoundaries) {
            drawer.drawColumnsBoundaries(WordsDetectionDrawer.Params(
                    rows,
                    shortenRowsDividers,
                    currentRowDividerIndex,
                    shortenColsDividers
            ))
        }
    }

    private fun Params.extractWordsOnRow(): List<WordInfo> {
        val rowWords = mutableListOf<WordInfo>()
        for (i in 0 until shortenColsDividers.size - 1) {
            extractWordOnRowIfPossible(WordInfoExtractor.Params(
                    descriptors, shortenRowsDividers, shortenColsDividers, i, currentRowDividerIndex
            ))?.let(rowWords::add)
        }
        return rowWords
    }

    private fun Params.extractWordOnRowIfPossible(params: WordInfoExtractor.Params): WordInfo? {
        with(params) {
            return if (doesColumnContainObjects(currentColDivider)) {
                WordInfoExtractor(this).extract().run {
                    if (isWordPresent()) this else null
                }
            } else null
        }
    }

    private fun Params.doesColumnContainObjects(currentColDivider: Int) =
            mergedColsDividers.doesSegmentContainObjects(currentColDivider)

    data class Params(
            val drawer: WordsDetectionDrawer,
            val bgrImageWithWordsBoundaries: Mat?,
            val shouldDrawWordsBoundaries: Boolean,
            val shortenRowsDividers: List<Int>,
            val mergedColsDividers: List<Int>,
            val currentRowDividerIndex: Int,
            val rows: Mat,
            val shouldDrawColumnsBoundaries: Boolean,
            val descriptors: List<Descriptor>
    ) {
        val shortenColsDividers = shortenDividers(mergedColsDividers)

        fun canExtractWordsOnRow() = shortenColsDividers.isNotEmpty()
    }
}