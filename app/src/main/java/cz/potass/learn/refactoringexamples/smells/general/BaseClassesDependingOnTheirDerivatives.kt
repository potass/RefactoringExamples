package cz.potass.learn.refactoringexamples.smells.general

internal class BaseClassesDependingOnTheirDerivatives {
    abstract class Base {
        companion object {
            fun create(): Base {
                return Derivative()
            }
        }
    }

    class Derivative : Base()

    abstract class RefactoredBase

    class RefactoredDerivative : RefactoredBase()

    abstract class BaseFactory {
        protected abstract fun create(): RefactoredBase
    }

    class DerivativeFactory : BaseFactory() {
        override fun create(): RefactoredBase {
            return RefactoredDerivative()
        }
    }
}