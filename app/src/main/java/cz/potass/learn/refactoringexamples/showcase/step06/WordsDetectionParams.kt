package cz.potass.learn.refactoringexamples.showcase.step06

import cz.potass.learn.refactoringexamples.showcase.shared.GapType
import cz.potass.learn.refactoringexamples.showcase.shared.Image
import cz.potass.learn.refactoringexamples.showcase.step02.WordsDetectionProgressListener

data class WordsDetectionParams(
        private val listener: WordsDetectionProgressListener,
        val rowGapType: GapType,
        val wordGapType: GapType,
        val image: Image,
        val mode: WordsDetectionMode,
        val isCancelled: () -> Boolean
) {
    val basePath = image.getBasePath()

    fun isVertical() = mode.isVertical()
    fun isHorizontal() = mode.isHorizontal()

    fun onStart() = listener.onStart()
    fun onWordsComponentsDetection() = listener.onWordsComponentsDetection()
    fun onWordsDetection() = listener.onWordsDetection()

    fun isRowAverageObjectSizeGapType() = rowGapType == GapType.AVERAGE_OBJECT_SIZE
    fun isWordAverageObjectSizeGapType() = wordGapType == GapType.AVERAGE_OBJECT_SIZE
    fun setRowGapTypeValue(value: Float) = rowGapType.setValue(value)
    fun setWordGapTypeValue(value: Float) = wordGapType.setValue(value)
}