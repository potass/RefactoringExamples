package cz.potass.learn.refactoringexamples.smells.general

internal class UseExplanatoryVariables {
    fun standardDeviationExample1(values: List<Int>): Double {
        return Math.sqrt(
                (values.sum().toDouble() / values.size)
                        .run { values.map { Math.pow(it - this, 2.0) }.sum() } / values.size
        )
    }

    fun standardDeviationExample2(values: List<Int>): Double {
        val result1 = values.sum().toDouble() / values.size
        val result2 = values.map { Math.pow(it - result1, 2.0) }.sum() / values.size
        return Math.sqrt(result2)
    }

    fun refactoredStandardDeviationExample(values: List<Int>): Double {
        val count = values.size
        val average = values.sum().toDouble() / count
        val variance = values.map { Math.pow(it - average, 2.0) }.sum() / count
        return Math.sqrt(variance)
    }
}